const path = require('path');

module.exports = {
    paths: function (paths, env) {        
        paths.appIndexJs = path.resolve(__dirname, 'frontend/index.js');
        paths.appSrc = path.resolve(__dirname, 'frontend');
        paths.appPublic = path.resolve(__dirname, './build_template');
        paths.appHtml = path.resolve(__dirname, './build_template/index.html');
        paths.appBuild = path.resolve(__dirname, './public/');
        return paths;
    },
}