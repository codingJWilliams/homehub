<?php

namespace App\Helpers;

use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class CustomSearchResult extends SearchResult {
    public $description;

    public function __construct(Searchable $searchable, string $title, string|null $description)
    {
        parent::__construct($searchable, $title, null);
        $this->description = $description;
    }

    function toArray() {
        
        return [
            "model" => last(explode("\\", get_class($this->searchable))),
            "title" => $this->title,
            "description" => $this->description,
            "id" => $this->searchable->id
        ];
    }
}