<?php

namespace App\Helpers;

class Pagination
{
    static function query($builder, $pagination)
    {
        if (!$pagination) $pagination = [
            "offset" => 0,
            "limit" => 100
        ];

        $offset = (int)$pagination['offset'];
        $limit = (int)$pagination['limit'];
        $items = $builder->clone()->offset($offset)->limit($limit)->get();
        return [
            "items" => $items,
            "pagination" => [
                "total" => $builder->count(),
                "offset" => $offset,
                "limit" => $limit,
                "returned" => count($items)
            ]
        ];
    }

    static function simple($array, $pagination)
    {
        if (!$pagination) $pagination = [
            "offset" => 0,
            "limit" => 100
        ];

        $offset = (int)$pagination['offset'];
        $limit = (int)$pagination['limit'];

        $items = array_slice($array, $offset, $limit);

        return [
            "items" => $items,
            "pagination" => [
                "total" => count($array),
                "offset" => $offset,
                "limit" => $limit,
                "returned" => count($items)
            ]
        ];
    }
}
