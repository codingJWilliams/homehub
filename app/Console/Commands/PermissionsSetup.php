<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\UserPermission;
use App\Models\UserRole;
use Illuminate\Console\Command;

class PermissionsSetup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:permissions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Configures an administrator role and promotes the app\'s first user';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $role = UserRole::query()->firstOrNew([ "name" => "Global Administrator" ]);
        $role->name = "Global Administrator";
        $role->save();
        $role->permissions()->sync(UserPermission::query()->pluck('id'));

        $user = User::find(1);
        $user->roles()->attach([ $role->id ]);
        
        $this->info("Completed.");
    }
}
