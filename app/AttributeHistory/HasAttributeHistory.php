<?php

namespace App\AttributeHistory;

use Illuminate\Support\Facades\Auth;

trait HasAttributeHistory
{
    function historyEntries() {
        return $this->morphMany(AttributeHistoryEntry::class, 'entity');
    }

    function historyEntriesFor($field) {
        return $this->historyEntries()->where("field", "=", $field);
    }

    function trackedValueAtTime($field, $time) {
        $entry = $this->historyEntriesFor($field)->where("created_at", "<", $time)->orderBy("created_at", "desc")->first();
        return $entry ? $entry->total : null;
    }

    static function bootHasAttributeHistory() {
        static::updating(function($model)
        {
            foreach($model->getDirty() as $key => $value)
            {
                if (!in_array($key, $model->trackHistoryFor)) continue;

                $original = $model->getOriginal($key);

                $change = ($original !== null) ? ($value - $original) : $value;
                
                $entry = new AttributeHistoryEntry([
                    "entity_type" => self::class,
                    "entity_id" => $model->id,
                    "field" => $key,
                    "change" => $change,
                    "total" => $value,
                    "description" => "Manual change by " . Auth::user()->email
                ]);
                $entry->save();
            }
        });
    }
}
