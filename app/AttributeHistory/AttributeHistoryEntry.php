<?php

namespace App\AttributeHistory;

use Illuminate\Database\Eloquent\Model;

class AttributeHistoryEntry extends Model
{
    protected $table = "attribute_history";
    protected $fillable = [
        'entity_type',
        'entity_id',
        'field',
        'change',
        'total',
        'description',
        'extra_field'
    ];

    /**
     * Relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function entity()
    {
        return $this->morphTo();
    }
}
