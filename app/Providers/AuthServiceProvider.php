<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;

use App\Models\User;
use Illuminate\Auth\Access\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        //
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        Gate::define('do', function (User $user, string $action, mixed $resource) {
            if (is_subclass_of($resource, Model::class)) {
                $resource = last(explode("\\", get_class($resource)));
            }

            $isAllowed = $user->hasPermission($action, $resource);

            if ($isAllowed) return Response::allow();
            else return Response::deny("You do not have the required permission '$resource.".ucfirst($action)."'");
        });
    }
}
