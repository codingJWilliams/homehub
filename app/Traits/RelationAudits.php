<?php

namespace App\Traits;

use App\Models\Container;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use OwenIt\Auditing\Events\AuditCustom;

trait RelationAudits
{
    function auditAttachDetach($evt, $relation, $values) {
        $this->auditEvent = $evt . "ed";
        $this->isCustomEvent = true;
        $this->auditCustomOld = [];
        $this->auditCustomNew = [$relation => $values];
        Event::dispatch(AuditCustom::class, [$this]);
    }
}
