<?php

namespace App\Traits;

use App\Models\Container;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait Many
{
    use RelationAudits;

    function attach($relation, $ids) {
        $rel = $this->$relation();
        $pivot_table = $rel->getTable();
        $pivot_local_key = $rel->getForeignPivotKeyName();
        $pivot_for_key = $rel->getRelatedPivotKeyName();

        foreach ($ids as $id) {
            DB::table($pivot_table)->insert([
                $pivot_local_key => $this->id,
                $pivot_for_key => $id
            ]);
        }

        $this->auditAttachDetach("attach", $relation, $ids);
        $this->refresh();
    }

    function detach($relation, $ids) {
        $rel = $this->$relation();
        $pivot_table = $rel->getTable();
        $pivot_local_key = $rel->getForeignPivotKeyName();
        $pivot_for_key = $rel->getRelatedPivotKeyName();

        foreach ($ids as $id) {
            Log::info("LOL, $pivot_table $pivot_local_key, $pivot_for_key, $id");
            $r = DB::table($pivot_table)->where($pivot_local_key, "=", $this->id)->where($pivot_for_key, "=", $id)->delete();
            Log::info("Res is " . print_r($r, true));
        }

        $this->auditAttachDetach("detach", $relation, $ids);
        $this->refresh();
    }
}
