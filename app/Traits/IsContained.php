<?php

namespace App\Traits;

use App\Models\Container;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait IsContained
{
    function parent(): BelongsTo {
        return $this->belongsTo(Container::class);
    }
}
