<?php

namespace App\Traits;

use Illuminate\Support\Facades\Validator;

trait ValidateFill {
    public function validateFill($params, $context = "admin") {
        $rules = self::$rules[$context] ?? self::$rules;
        $validator = Validator::make($params, $rules);
        if ($validator->fails()) return $validator->errors();
        else $this->fill($params);
    }
}