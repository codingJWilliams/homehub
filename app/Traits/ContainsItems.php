<?php

namespace App\Traits;

use App\Models\Container;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait ContainsItems
{
    function children(): HasMany {
        return $this->hasMany(Container::class);
    }
}
