<?php

namespace App\Traits;

use App\Models\Container;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use OwenIt\Auditing\Events\AuditCustom;

trait BidirectionalMany
{
    use RelationAudits;

    function biAttach($relation, $ids) {
        $rel = $this->$relation();
        $pivot_table = $rel->getTable();
        $pivot_local_key = $rel->getForeignPivotKeyName();
        $pivot_for_key = $rel->getRelatedPivotKeyName();

        foreach ($ids as $id) {
            DB::table($pivot_table)->insert([
                $pivot_local_key => $this->id,
                $pivot_for_key => $id
            ]);
            DB::table($pivot_table)->insert([
                $pivot_for_key => $this->id,
                $pivot_local_key => $id
            ]);
        }

        $this->auditAttachDetach("attach", $relation, $ids);
        $this->refresh();
    }

    function biDetach($relation, $ids) {
        $rel = $this->$relation();
        $pivot_table = $rel->getTable();
        $pivot_local_key = $rel->getForeignPivotKeyName();
        $pivot_for_key = $rel->getRelatedPivotKeyName();

        foreach ($ids as $id) {
            DB::table($pivot_table)->where($pivot_local_key, "=", $this->id)->where($pivot_for_key, "=", $id)->delete();
            DB::table($pivot_table)->where($pivot_local_key, "=", $id)->where($pivot_for_key, "=", $this->id)->delete();
        }

        $this->auditAttachDetach("detach", $relation, $ids);
        $this->refresh();
    }
}
