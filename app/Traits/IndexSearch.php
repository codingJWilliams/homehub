<?php

namespace App\Traits;

trait IndexSearch
{
    // protected static $indexSearchAttributes;
    static function search(string $term) {
        $q = self::query();
        
        foreach (self::$indexSearchAttributes as $i=>$attr) {
            $func = $i === 0 ? "where" : "orWhere";
            $q = $q->$func($attr, "LIKE", "%".$term."%");
        }

        return $q;
    }
}
