<?php

namespace App\Models;

use App\Helpers\CustomSearchResult;
use App\Traits\IndexSearch;
use App\Traits\IsContained;
use App\Traits\ValidateFill;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $name
 * @property string $manufacturer
 * @property string $link
 * @property string $notes
 */
class AssetModel extends Model implements Auditable, Searchable
{
    use HasFactory, IsContained, ValidateFill, IndexSearch, \OwenIt\Auditing\Auditable;

    protected $fillable = ["name", "manufacturer", "link", "notes"];
    protected $appends = ["display_name"];
    public static $globalSearchAttributes = ["name", "manufacturer"];
    protected static $indexSearchAttributes = ["name", "manufacturer"];

    protected static $rules = [
        "name" => "required"
    ];

    protected function getDisplayNameAttribute() {
        if (!$this->manufacturer) return $this->name;
        return $this->manufacturer . " - " . $this->name;
    }

    protected $attributes = [];

    function assets() {
        return $this->hasMany(Asset::class, "model_id");
    }

    function getSearchResult(): SearchResult
    {
        return new CustomSearchResult(
            $this,
            $this->display_name,
            null
        );
    }
}
