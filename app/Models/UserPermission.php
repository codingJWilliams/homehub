<?php

namespace App\Models;

use App\Helpers\CustomSearchResult;
use App\Traits\IndexSearch;
use App\Traits\IsContained;
use App\Traits\Many;
use App\Traits\ValidateFill;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $action
 * @property string $resource
 * @property string $description
 */
class UserPermission extends Model implements Auditable, Searchable
{
    use HasFactory, IsContained, ValidateFill, IndexSearch, Many, \OwenIt\Auditing\Auditable;

    protected $fillable = ["action", "resource", "description"];
    public static $globalSearchAttributes = ["action", "resource"];
    protected static $indexSearchAttributes = ["action", "resource"];
    public $rels = ["roles"];

    protected static $rules = [
        "action" => "required|min:1",
        "resource" => "required|min:1",
    ];

    function roles() {
        return $this->belongsToMany(UserRole::class, "role_permission_assignments", "permission_id", "role_id");
    }

    function getSearchResult(): SearchResult
    {
        return new CustomSearchResult(
            $this,
            ucfirst($this->action) . "." . $this->resource,
            null
        );
    }
}
