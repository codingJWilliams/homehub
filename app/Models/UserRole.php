<?php

namespace App\Models;

use App\Helpers\CustomSearchResult;
use App\Traits\IndexSearch;
use App\Traits\IsContained;
use App\Traits\Many;
use App\Traits\ValidateFill;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $name
 * @property string $description
 */
class UserRole extends Model implements Auditable, Searchable
{
    use HasFactory, IsContained, ValidateFill, IndexSearch, Many, \OwenIt\Auditing\Auditable;

    protected $fillable = ["name", "description"];
    public static $globalSearchAttributes = ["name", "description"];
    protected static $indexSearchAttributes = ["name"];
    public $rels = ["users", "permissions"];

    protected static $rules = [
        "name" => "required|min:1",
    ];

    function users() {
        return $this->belongsToMany(User::class, "user_role_assignments", "role_id", "user_id");
    }

    function permissions() {
        return $this->belongsToMany(UserPermission::class, "role_permission_assignments", "role_id", "permission_id");
    }

    function getSearchResult(): SearchResult
    {
        return new CustomSearchResult(
            $this,
            ucfirst($this->name),
            null
        );
    }
}
