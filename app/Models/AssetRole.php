<?php

namespace App\Models;

use App\Helpers\CustomSearchResult;
use App\Traits\IndexSearch;
use App\Traits\IsContained;
use App\Traits\Many;
use App\Traits\ValidateFill;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $name
 */
class AssetRole extends Model implements Auditable, Searchable
{
    use HasFactory, IsContained, ValidateFill, IndexSearch, Many, \OwenIt\Auditing\Auditable;

    protected $fillable = ["name"];
    public $rels = ["assets"];
    public static $globalSearchAttributes = ["name"];
    protected static $indexSearchAttributes = ["name"];

    protected static $rules = [
        "name" => "required"
    ];

    protected $attributes = [];

    function assets() {
        return $this->belongsToMany(Asset::class, "asset_role_links");
    }

    function getSearchResult(): SearchResult
    {
        return new CustomSearchResult(
            $this,
            $this->name,
            null
        );
    }
}
