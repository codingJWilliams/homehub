<?php

namespace App\Models;

use App\AttributeHistory\HasAttributeHistory;
use App\Helpers\CustomSearchResult;
use App\Traits\IndexSearch;
use App\Traits\IsContained;
use App\Traits\Many;
use App\Traits\ValidateFill;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $name
 * @property integer $size_kb
 * @property integer $parent_folder_id
 * @property integer $vault_id
 */
class VaultFolder extends Model implements Auditable, Searchable
{
    use HasFactory, ValidateFill, IndexSearch, Many, \OwenIt\Auditing\Auditable;

    protected $fillable = ["name", "size_kb", "parent_folder_id", "vault_id"];
    public static $globalSearchAttributes = ["name"];
    protected static $indexSearchAttributes = ["name"];

    protected static $rules = [
        "name" => "required|min:1"
    ];

    function files() {
        return $this->hasMany(VaultFile::class, "parent_folder_id");
    }

    function parent() {
        return $this->belongsTo(VaultFolder::class, "parent_folder_id");
    }

    function getBreadcrumbAttribute() {
        $crumb = [];

        $current = $this;
        while ($current) {
            $crumb[] = ["name" => $current->name, "id" => $current->id];
            $current = $current->parent;
        }

        return array_reverse($crumb);
    }

    function getSearchResult(): SearchResult
    {
        return new CustomSearchResult(
            $this,
            $this->name,
            null
        );
    }
}
