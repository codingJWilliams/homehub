<?php

namespace App\Models;

use OwenIt\Auditing\Models\Audit as SourceAudit;

class Audit extends SourceAudit
{
    protected $appends = ["user"];

    function getUserAttribute() {
        return $this->user()->first();
    }
}
