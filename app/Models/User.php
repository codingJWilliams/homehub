<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use App\Helpers\CustomSearchResult;
use App\Traits\Many;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Log;
use Laravel\Sanctum\HasApiTokens;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class User extends Authenticatable implements \PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject, Auditable, Searchable
{
    use HasApiTokens, HasFactory, Notifiable, Many, \Staudenmeir\EloquentHasManyDeep\HasRelationships, \OwenIt\Auditing\Auditable;

    public static $globalSearchAttributes = ["name", "email"];
    public $rels = ["roles"];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email'
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    function roles() {
        return $this->belongsToMany(UserRole::class, "user_role_assignments", "user_id", "role_id");
    }

    function permissions()
    {
        return $this->hasManyDeepFromRelations($this->roles(), (new UserRole())->permissions());
    }

    function hasPermission($action, $resource) {
        $count = $this->permissions()->where("action", $action)->where("resource", $resource)->count();
        return $count > 0;
    }

    function getSearchResult(): SearchResult
    {
        return new CustomSearchResult(
            $this,
            $this->name,
            $this->email
        );
    }
}
