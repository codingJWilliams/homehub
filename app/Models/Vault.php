<?php

namespace App\Models;

use App\AttributeHistory\HasAttributeHistory;
use App\Helpers\CustomSearchResult;
use App\Traits\IndexSearch;
use App\Traits\IsContained;
use App\Traits\Many;
use App\Traits\ValidateFill;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $name
 * @property string $password_hash
 * @property string $vault_key
 * @property string $salt
 */
class Vault extends Model implements Auditable, Searchable
{
    use HasFactory, IsContained, ValidateFill, IndexSearch, Many, \OwenIt\Auditing\Auditable;

    protected $fillable = ["name", "password_hash", "vault_key", "salt"];
    public static $globalSearchAttributes = ["name"];
    protected static $indexSearchAttributes = ["name"];

    protected static $rules = [
        "name" => "required|min:1"
    ];

    function getSearchResult(): SearchResult
    {
        return new CustomSearchResult(
            $this,
            $this->name,
            null
        );
    }

    function folders() {
        return $this->hasMany(VaultFolder::class);
    }

    function files() {
        return $this->hasMany(VaultFile::class);
    }
}
