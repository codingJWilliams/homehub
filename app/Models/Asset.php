<?php

namespace App\Models;

use App\Helpers\CustomSearchResult;
use App\Traits\BidirectionalMany;
use App\Traits\IndexSearch;
use App\Traits\IsContained;
use App\Traits\Many;
use App\Traits\ValidateFill;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $purchased_at
 * @property string $tag
 * @property integer $model_id
 * @property string $notes
 * @property string $serial_number
 * @property int $container_id
 * @property double $purchase_value
 */
class Asset extends Model implements Auditable, Searchable
{
    use HasFactory, IsContained, ValidateFill, \OwenIt\Auditing\Auditable, BidirectionalMany, Many, IndexSearch;

    protected $fillable = ["tag", "model_id", "notes", "status", "serial_number", "container_id", "purchased_at", "purchase_value"];
    protected $with = ["model"];
    public $biRels = ["linked"];
    public $rels = ["roles"];

    public static $globalSearchAttributes = ["name", "tag", "serial_number"];
    protected static $indexSearchAttributes = ["name", "tag", "serial_number"];
    protected static $rules = [
        "model_id" => "required",
        "status" => "required",
    ];

    protected $attributes = [
        "tag" => "",
        "status" => "active",
    ];

    protected $casts = [
        "purchased_at" => "datetime"
    ];

    function container() {
        return $this->hasOne(Container::class, "id", "container_id");
    }

    function model() {
        return $this->hasOne(AssetModel::class, "id", "model_id");
    }

    function linked() {
        return $this->belongsToMany(Asset::class, "asset_links", "to_asset_id", "from_asset_id");
    }

    function sisters() {
        return $this->hasMany(self::class, "model_id", "model_id")->where("id", "!=", $this->id);
    }

    function roles() {
        return $this->belongsToMany(AssetRole::class, "asset_role_links");
    }

    function getSearchResult(): SearchResult
    {
        return new CustomSearchResult(
            $this,
            $this->model->display_name,
            $this->tag ? $this->tag : "Untagged Asset",
        );
    }
}
