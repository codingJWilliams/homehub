<?php

namespace App\Models;

use App\Helpers\CustomSearchResult;
use App\Traits\IndexSearch;
use App\Traits\IsContained;
use App\Traits\Many;
use App\Traits\ValidateFill;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $last_modified_at
 * @property Carbon $received_at
 * @property string $name
 * @property string $file_key
 * @property integer $size_kb
 * @property integer $num_chunks
 * @property integer $parent_folder_id
 * @property integer $vault_id
 */
class VaultFile extends Model implements Auditable, Searchable
{
    use HasFactory, IsContained, ValidateFill, IndexSearch, Many, \OwenIt\Auditing\Auditable;

    protected $fillable = ["name", "file_key", "num_chunks", "size_kb", "parent_folder_id", "vault_id", "last_modified_at", "filename", "notes", "received_at"];
    public static $globalSearchAttributes = ["name"];
    protected static $indexSearchAttributes = ["name"];

    protected $casts = [
        "last_modified_at" => "datetime",
        "received_at" => "datetime"
    ];
    
    protected static function booted(): void
    {
        static::deleting(function (VaultFile $file) {
            $file->deleteData();
        });
    }

    protected static $rules = [
        "name" => "required|min:1",
        "filename" => "required",
        "file_key" => "required",
        "num_chunks" => "required|numeric",
        "size_kb" => "required|numeric",
        "last_modified_at" => "required"
    ];

    function getS3Path() {
        return "vault/$this->vault_id/$this->parent_folder_id/$this->id";
    }

    function getS3Name($chunk) {
        return "file-$this->id.$chunk.chunk";
    }

    function existsOnDisk() {
        try {
            return Storage::exists($this->getS3Path() . "/" .  $this->getS3Name(0)) && Storage::size($this->getS3Path() . "/" .  $this->getS3Name(0)) > 0;
        } catch (Exception $e) {
            return false;
        }
    }

    function deleteData() {
        $paths = [];
        for ($i = 0; $i < $this->num_chunks; $i++) $paths[] = $this->getS3Path() . "/" . $this->getS3Name($i);
        Storage::delete($paths);
    }

    function getSearchResult(): SearchResult
    {
        return new CustomSearchResult(
            $this,
            $this->name,
            null
        );
    }
}
