<?php

namespace App\Models;

use App\Helpers\CustomSearchResult;
use App\Traits\ContainsItems;
use App\Traits\IndexSearch;
use App\Traits\IsContained;
use App\Traits\ValidateFill;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $container_id
 * @property string $name
 * @property string $description
 */
class Container extends Model implements Auditable, Searchable
{
    use HasFactory, IsContained, ContainsItems, ValidateFill, IndexSearch, \OwenIt\Auditing\Auditable;

    protected $fillable = ["name", "type", "container_id", "description"];
    public static $globalSearchAttributes = ["name", "description"];
    protected static $indexSearchAttributes = ["name"];

    protected static $rules = [
        "name" => "required|min:1"
    ];

    protected $attributes = [
        "container_id" => 0,
        "description" => ""
    ];

    protected function assets() {
        return $this->hasMany(Asset::class);
    }

    function getChildAssetsAttribute() {
        return $this->assets()->count();
    }
    function getChildContainersAttribute() {
        return $this->children()->count();
    }

    function getSearchResult(): SearchResult
    {
        return new CustomSearchResult(
            $this,
            $this->name,
            $this->parent ? "Contained within '{$this->parent->name}'" : null
        );
    }
}
