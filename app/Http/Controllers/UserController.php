<?php

namespace App\Http\Controllers;

use App\Models\User;

class UserController extends CrudController
{
    protected $model = User::class;

    protected function getViewAttributes() {
        return ["id", "email", "name", "roles"];
    }

    protected function getIndexRelations() {
        return [];
    }
}
