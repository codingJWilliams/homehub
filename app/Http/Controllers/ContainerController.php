<?php

namespace App\Http\Controllers;

use App\Helpers\Pagination;
use App\Models\Asset;
use App\Models\Container;
use Illuminate\Support\Facades\Gate;

class ContainerController extends CrudController
{
    protected $model = Container::class;

    protected function getViewAttributes() {
        return ["name", "type", "container_id", "description", "children", "assets"];
    }

    /**
     * Display a listing of the resource.
     */
    public function tree()
    {
        Gate::authorize("do", ["read", $this->getModelName()]);

        $parent = request()->query("parent");
        if ($parent) $parent = (int)$parent;

        $containers = Container::query()->where("container_id", "=", $parent ?? 0)->get()->each->append("child_assets")->append("child_containers");

        $assetQuery = Asset::query();
        if ($parent) $assetQuery = $assetQuery->where("container_id", "=", (int)$parent);
        else $assetQuery = $assetQuery->whereNull("container_id");
        $assets = $assetQuery->get();

        // $assetQuery = Asset::query();
        // if ($parent) $assetQuery = $assetQuery->where("container_id", "=", (int)$parent);
        // else $assetQuery = $assetQuery->whereNull("container_id");
        // $assets = $assetQuery->get();

        return compact('containers', 'assets');
    }

    protected function getIndexRelations() {
        return [];
    }
}
