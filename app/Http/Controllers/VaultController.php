<?php

namespace App\Http\Controllers;

use App\Models\Vault;
use App\Models\VaultFile;
use App\Models\VaultFolder;
use Illuminate\Support\Arr;

class VaultController extends CrudController
{
    protected $model = Vault::class;

    protected function getViewAttributes() {
        return ["name", "password_hash", "vault_key", "salt"];
    }

    // protected function sortIndexQuery($query)
    // {
    //     return $query->orderBy("retired", "asc")->orderBy("name", "asc");
    // }

    function viewFolder(int $id) {
        $vault = Vault::query()->find($id);
        $folderId = request()->query("id");
        $folder = VaultFolder::query()->find($folderId);

        $subfolders = 
            $folder ?
            $vault->folders()->orderBy("name", "asc")->where("parent_folder_id", "=", $folder->id)->get() :
            $vault->folders()->orderBy("name", "asc")->whereNull("parent_folder_id")->get();
        
        $files = 
            $folder ?
            $vault->files()->orderBy("received_at", "desc")->orderBy("name", "asc")->where("parent_folder_id", "=", $folder->id)->get() :
            $vault->files()->orderBy("received_at", "desc")->orderBy("name", "asc")->whereNull("parent_folder_id")->get();

        $files = Arr::map($files->toArray(), fn($it) => array_merge($it, ["exists" => VaultFile::find($it['id'])->existsOnDisk()]));

        return [
            "folders" => $subfolders,
            "files" => $files,
            "breadcrumb" => $folder ? $folder->breadcrumb : [],
            "parent" => $folder ? ($folder->parent_folder_id ?? "default") : null
        ];
    }

    protected function getIndexRelations() {
        return [];
    }
}
