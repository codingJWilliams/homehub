<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{
    public function auth()
    {
        if (env("AUTH_BYPASS_USER")) return redirect('api/auth/callback');
        $redirect = Socialite::driver('azure')->stateless()->redirect();

        return $redirect;
        // $url = $redirect->getTargetUrl();

        // return [ "url" => $url ];
    }

    public function callback()
    {
        if ($bypass = env("AUTH_BYPASS_USER")) {
            $user = User::find($bypass);
            $token = auth("api")->login($user);
        } else {
            $providerUser = Socialite::driver('azure')->stateless()->user();

            /** @var User $user */
            $user = User::query()->firstOrNew(
                ['email' => $providerUser->getEmail()]
            );
            $user->name = $providerUser->getName();
            $user->save();

            $token = auth("api")->login($user);
        }
        return redirect(env("FRONTEND_URL").'/?token='.urlencode($token));
    }
}
