<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\AssetModel;
use App\Models\AssetRole;
use App\Models\Audit;
use App\Models\Container;
use App\Models\User;
use App\Models\UserPermission;
use App\Models\UserRole;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    function __construct()
    {
        $this->middleware("auth"); // Require auth
    }

    public function summary()
    {
        $modelCounts = [
            "Asset" => Asset::query()->count(),
            "AssetModel" => AssetModel::query()->count(),
            "AssetRole" => AssetRole::query()->count(),
            "Container" => Container::query()->count(),
            "User" => User::query()->count(),
            "UserRole" => UserRole::query()->count(),
            "UserPermission" => UserPermission::query()->count(),
            "Audit" => Audit::query()->count(),
        ];

        return $modelCounts;
    }

    public function context()
    {
        return [
            "permissions" => Auth::user()->permissions->map(function ($perm) {
                return ['action' => $perm['action'], 'resource' => $perm['resource']];
            })
        ];
    }
}
