<?php

namespace App\Http\Controllers;

use App\Models\UserPermission;

class UserPermissionController extends CrudController
{
    protected $model = UserPermission::class;

    protected function getViewAttributes() {
        return ["id", "action", "resource", "description", "roles"];
    }

    protected function getIndexRelations() {
        return [];
    }
}
