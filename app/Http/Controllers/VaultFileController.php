<?php

namespace App\Http\Controllers;

use App\Models\VaultFile;
use Illuminate\Support\Facades\Storage;

class VaultFileController extends CrudController
{
    protected $model = VaultFile::class;

    protected function getViewAttributes() {
        return ["name", "size_kb", "file_key", "parent_folder_id", "vault_id", "filename", "notes", "last_modified_at", "received_at"];
    }

    // protected function sortIndexQuery($query)
    // {
    //     return $query->orderBy("retired", "asc")->orderBy("name", "asc");
    // }
    protected function uploadChunk(int $id, int $chunk) {
        $file = $this->getModelById($id);

        $uploadedFile = request()->file("file");
        $path = $uploadedFile->storeAs($file->getS3Path(), $file->getS3Name($chunk));

        if (!$path) return response([
            "success" => false,
            "error" => $uploadedFile->getErrorMessage()
        ], 500);

        return [
            "success" => true,
            "path" => $path
        ];
    }

    protected function sortIndexQuery($query)
    {
        return $query->orderBy("received_at", "desc");
    }

    protected function getChunk(int $id, int $chunk) {
        $file = $this->getModelById($id);

        return Storage::download($file->getS3Path() . "/" .  $file->getS3Name($chunk));
    }

    protected function getIndexRelations() {
        return [];
    }
}
