<?php

namespace App\Http\Controllers;

use App\Helpers\Pagination;
use App\Models\Recipe;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Inertia\Inertia;

class CrudController extends Controller
{
    protected $model = Model::class;

    function __construct()
    {
        $this->middleware("auth"); // Require auth
    }

    protected function getModelName()
    {
        return last(explode("\\", $this->model));
    }

    protected function getViewAttributes()
    {
        return ["name"];
    }

    protected function getIndexRelations()
    {
        return [];
    }

    protected function sortIndexQuery($query)
    {
        return $query;
    }

    protected function getModelById(string $id)
    {
        if ($id === "create") return new $this->model;
        else return $this->model::findOrFail($id);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        Gate::authorize("do", ["read", $this->getModelName()]);

        try {
            if (!request()->input("query")) $query = $this->model::query();
            else $query = $this->model::search(request()->input("query"));
        } catch (\Exception $e) {
            $query = $this->model::query();
        }

        if (request()->input("exclude")) {
            $excludeIds = explode(',', request()->input("exclude"));
            $query = $query->whereNotIn("id", $excludeIds);
        }

        foreach ($this->getIndexRelations() as $rel) {
            $query = $query->with($rel);
        }

        $query = $this->sortIndexQuery($query);

        return Pagination::query($query, request()->input("pagination") ?? []);
    }

    /**
     * Display the specified resource.
     */
    public function view(string $id)
    {
        Gate::authorize("do", ["read", $this->getModelName()]);
        $model = $this->getModelById($id);
        return $model->only(array_merge(["id", "created_at", "updated_at"], $this->getViewAttributes()));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        Gate::authorize("do", ["edit", $this->getModelName()]);
        $model = $this->getModelById($id);

        if ($error = $model->validateFill($request->input()))
            return response()->json(["errors" => $error], 400);

        $model->save();

        return ["id" => $model->id];
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id)
    {
        Gate::authorize("do", ["delete", $this->getModelName()]);
        $model = $this->model::query()->findOrFail($id);
        $model->delete();
        return ["success" => true];
    }

    public function audits(int $id)
    {
        Gate::authorize("do", ["read", "Audit"]);
        $model = $this->getModelById($id);
        return Pagination::query($model->audits()->orderBy("created_at", "desc"), request()->input("pagination"));
    }

    public function relation(int $id, string $relation)
    {
        Gate::authorize("do", ["edit", $this->getModelName()]);

        $model = $this->getModelById($id);
        $isBiRel = isset($model->biRels) && in_array($relation, $model->biRels);
        $isRel = isset($model->rels) && in_array($relation, $model->rels);
        if (!$isBiRel && !$isRel)
            return response()->json(["message" => "Unable to edit relation '$relation'"], 400);

        $body = request()->input();

        $attachFunc = $isBiRel ? "biAttach" : "attach";
        $detachFunc = $isBiRel ? "biDetach" : "detach";

        if (array_key_exists("\$attach", $body)) $model->$attachFunc($relation, $body['$attach']);
        if (array_key_exists("\$detach", $body)) $model->$detachFunc($relation, $body['$detach']);

        $model->load($relation);

        return $model->only($this->getViewAttributes());
    }
}
