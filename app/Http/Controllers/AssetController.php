<?php

namespace App\Http\Controllers;

use App\Models\Asset;

class AssetController extends CrudController
{
    protected $model = Asset::class;

    protected function getViewAttributes() {
        return ["purchased_at", "tag", "status", "model", "model_id", "notes", "serial_number", "container_id", "purchase_value", "linked", "roles", "sisters"];
    }

    protected function byTag(string $tag) {
        $asset = Asset::query()->where([ "tag" => $tag ])->firstOrFail();
        return $this->view($asset->id);
    }

    protected function getIndexRelations() {
        return ["model"];
    }
}
