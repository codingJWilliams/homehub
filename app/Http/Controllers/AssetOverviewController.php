<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\TuckItem;
use App\Models\TuckMoneyCount;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class AssetOverviewController extends Controller
{
    function __construct()
    {
        $this->middleware("auth"); // Require auth
    }

    public static function buildTimeSeries($from, $to) {

        $totalValue = (double)Asset::query()->where("created_at", "<", $to)->sum("purchase_value");

        return [
            "purchase_value" => $totalValue
        ];
    }

    public static function buildAnalytics($from = null, $to = null) {
        $from = $from ?? now()->subDays(7);
        $to = $to ?? now();

        $i = $from->clone();
        $timeSeries = [];
        while ($i < $to) {
            $fromPeriod = $i->clone();
            $toPeriod = $i->addDays(1);
            $timeSeries[] = self::buildTimeSeries($fromPeriod, $toPeriod);
        }

        return ["time_series" => $timeSeries];
    }

    public function overview()
    {
        Gate::authorize("do", ["read", "Asset"]);

        $from = request()->query("from") ? Carbon::parse(request()->query("from")) : null;
        $to = request()->query("to") ? Carbon::parse(request()->query("to")) : null;
        $to->setHour(23);
        $to->setMinute(59);
        return self::buildAnalytics($from, $to);
    }
}
