<?php

namespace App\Http\Controllers;

use App\Helpers\Pagination;
use App\Models\Asset;
use App\Models\AssetModel;
use App\Models\AssetRole;
use App\Models\Audit;
use App\Models\Container;
use App\Models\User;
use App\Models\UserPermission;
use App\Models\UserRole;
use Spatie\Searchable\Search;

class SearchController extends Controller
{
    protected $searchModels = [
        Asset::class,
        AssetModel::class,
        AssetRole::class,
        Container::class,
        User::class,
        UserPermission::class,
        UserRole::class
    ];

    function __construct()
    {
        $this->middleware("auth");
    }

    public function search()
    {
        $query = request()->query("query");
        $pagination = request()->input("pagination");
        
        if (!$query) return Pagination::simple([], $pagination);

        $search = (new Search());
        foreach ($this->searchModels as $model) {
            $search = $search->registerModel($model, $model::$globalSearchAttributes);
        }
        $results = $search->search($query);

        return Pagination::simple(
            $results
                ->map(function ($result) {
                    $res = $result->toArray();
                    $res["compositeId"] = $res["model"] . "." . $res["id"];
                    return $res;
                })
                ->groupBy(function (array $model) {
                    return $model['model'];
                })
                ->toArray(),
            $pagination
        );
    }
}
