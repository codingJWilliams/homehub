<?php

namespace App\Http\Controllers;

use App\Models\UserRole;

class UserRoleController extends CrudController
{
    protected $model = UserRole::class;

    protected function getViewAttributes() {
        return ["id", "name", "description", "users", "permissions"];
    }

    protected function getIndexRelations() {
        return [];
    }
}
