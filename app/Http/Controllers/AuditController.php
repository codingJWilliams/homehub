<?php

namespace App\Http\Controllers;

use App\Models\Audit;

class AuditController extends CrudController
{
    protected $model = Audit::class;

    protected function getViewAttributes()
    {
        return ["auditable_id", "auditable_type", "event", "ip_address", "old_values", "new_values", "tags", "url", "user_agent", "user_id", "user_type", "user"];
    }

    protected function sortIndexQuery($query)
    {
        return $query->orderBy("created_at", "desc");
    }
    
    protected function getIndexRelations()
    {
        return [];
    }
}
