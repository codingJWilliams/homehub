<?php

namespace App\Http\Controllers;

use App\Models\AssetModel;

class AssetModelController extends CrudController
{
    protected $model = AssetModel::class;

    protected function getViewAttributes() {
        return ["name", "manufacturer", "link", "notes", "assets"];
    }

    protected function getIndexRelations() {
        return [];
    }
}
