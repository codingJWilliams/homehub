<?php

namespace App\Http\Controllers;

use App\Models\VaultFolder;

class VaultFolderController extends CrudController
{
    protected $model = VaultFolder::class;

    protected function getViewAttributes() {
        return ["name", "size_kb", "parent_folder_id", "vault_id"];
    }

    // protected function sortIndexQuery($query)
    // {
    //     return $query->orderBy("retired", "asc")->orderBy("name", "asc");
    // }

    protected function getIndexRelations() {
        return [];
    }
}
