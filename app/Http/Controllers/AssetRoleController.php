<?php

namespace App\Http\Controllers;

use App\Models\AssetRole;

class AssetRoleController extends CrudController
{
    protected $model = AssetRole::class;

    protected function getViewAttributes() {
        return ["name", "assets"];
    }

    protected function getIndexRelations() {
        return [];
    }
}
