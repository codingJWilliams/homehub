<?php

use Illuminate\Http\Request;
use App\Http\Controllers\AssetController;
use App\Http\Controllers\AssetModelController;
use App\Http\Controllers\AssetOverviewController;
use App\Http\Controllers\AssetRoleController;
use App\Http\Controllers\AuditController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ContainerController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserPermissionController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\UserRoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VaultController;
use App\Http\Controllers\VaultFileController;
use App\Http\Controllers\VaultFolderController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

function resourceController($route, $class)
{
    Route::get("$route/", [$class, "index"]);
    Route::get("$route/{id}", [$class, "view"]);
    Route::get("$route/{id}/audits", [$class, "audits"]);
    Route::put("$route/{id}/relation/{relation}", [$class, "relation"]);
    Route::put("$route/{id}", [$class, "update"]);
    Route::delete("$route/{id}", [$class, "destroy"]);
}

Route::get('/auth', [AuthController::class, "auth"]);
Route::get('/auth/callback', [AuthController::class, "callback"]);
Route::get('/summary', [DashboardController::class, "summary"]);
Route::get('/context', [DashboardController::class, "context"]);
Route::get('/search', [SearchController::class, "search"]);
Route::get("/container/tree", [ContainerController::class, "tree"]);
resourceController('/container', ContainerController::class);
Route::get("/asset/tag/{tag}", [AssetController::class, "byTag"]);
Route::get("/asset/overview", [AssetOverviewController::class, "overview"]);
resourceController('/asset/model', AssetModelController::class);
resourceController('/asset/role', AssetRoleController::class);
resourceController('/asset', AssetController::class);
resourceController('/audit', AuditController::class);
resourceController('/user/role', UserRoleController::class);
resourceController('/user/permission', UserPermissionController::class);
resourceController('/user', UserController::class);

// Vault
Route::post('/vault/file/{id}/chunk/{chunk}', [VaultFileController::class, "uploadChunk"]);
Route::get('/vault/file/{id}/chunk/{chunk}', [VaultFileController::class, "getChunk"]);
Route::get('/vault/{id}/viewFolder', [VaultController::class, "viewFolder"]);
resourceController('/vault/file', VaultFileController::class);
resourceController('/vault/folder', VaultFolderController::class);
resourceController('/vault', VaultController::class);
