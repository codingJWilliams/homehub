import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import fs from 'fs/promises';
import react from '@vitejs/plugin-react'

export default defineConfig({
    plugins: [
        laravel({
            input: ['resources/css/app.css', 'resources/js/app.jsx'],
            refresh: true,
        }),
        react()
    ],
    // esbuild: {
    //     loader: "jsx",
    //     include: /src\/.*\.jsx?$/,
    //     // loader: "tsx",
    //     // include: /src\/.*\.[tj]sx?$/,
    //     exclude: [],
    // },
    // optimizeDeps: {
    //     esbuildOptions: {
    //         plugins: [
    //             {
    //                 name: "load-js-files-as-jsx",
    //                 setup(build) {
    //                     build.onLoad({ filter: /src\/.*\.js$/ }, async (args) => ({
    //                         loader: "jsx",
    //                         contents: await fs.readFile(args.path, "utf8"),
    //                     }));
    //                 },
    //             },
    //         ],
    //     },
    // },
});
