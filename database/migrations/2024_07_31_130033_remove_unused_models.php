<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::drop("tuck_deals");
        Schema::drop("tuck_deal_items_a");
        Schema::drop("tuck_deal_items_b");
        Schema::drop("tuck_items");
        Schema::drop("tuck_money_counts");
        Schema::drop("tuck_item_categories");
        Schema::drop("snacks");
        Schema::drop("tupperware");
        Schema::drop("recipes");

        foreach (["Recipe", "Snack", "Tupperware", "TuckMoneyCount", "TuckItem", "TuckItemCategory", "TuckDeal", "TuckAnalytics"] as $resource) {
            foreach (["read", "edit", "delete"] as $action) {
                DB::table("user_permissions")->where([
                    "action" => $action,
                    "resource" => $resource
                ])->delete();
            }
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
