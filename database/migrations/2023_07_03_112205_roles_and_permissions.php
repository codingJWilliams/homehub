<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create("user_roles", function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string("name", 128);
            $table->text("description");
        });

        Schema::create("user_role_assignments", function (Blueprint $table) {
            $table->integer("user_id");
            $table->integer("role_id");
        });

        Schema::create("user_permissions", function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string("action", 128);
            $table->string("resource", 128);
            $table->text("description");
        });

        Schema::create("role_permission_assignments", function (Blueprint $table) {
            $table->integer("role_id");
            $table->integer("permission_id");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
