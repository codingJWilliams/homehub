<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        foreach (["Asset", "AssetModel", "AssetRole", "Audit", "Container", "Recipe", "Snack", "Tupperware", "User", "UserPermission", "UserRole"] as $resource) {
            foreach (["read", "edit", "delete"] as $action) {
                DB::table("user_permissions")->insert([
                    "created_at" => now(),
                    "updated_at" => now(),
                    "action" => $action,
                    "resource" => $resource
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
    }
};
