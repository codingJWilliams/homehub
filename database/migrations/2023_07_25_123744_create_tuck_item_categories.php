<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tuck_item_categories', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string("name", 128);
            $table->boolean("fridge");
        });

        Schema::table('tuck_items', function (Blueprint $table) {
            $table->integer("tuck_item_category_id")->nullable();
        });

        foreach (["read", "edit", "delete"] as $action) {
            DB::table("user_permissions")->insert([
                "created_at" => now(),
                "updated_at" => now(),
                "action" => $action,
                "resource" => "TuckItemCategory"
            ]);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tuck_item_categories');
    }
};
