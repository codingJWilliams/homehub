<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateAttributeHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("attribute_history", function (Blueprint $table) {
            $table->timestamps();
            $table->id();
            $table->morphs('entity');
            $table->string("field", 128);
            $table->integer('change');
            $table->integer('total');
            $table->text('description')->nullable();
            $table->text('extra_field')->nullable();
        });

        foreach (DB::table("stock_mutations")->get() as $mutation) {
            DB::table("attribute_history")->insert([
                "created_at" => $mutation->created_at,
                "updated_at" => $mutation->updated_at,
                "entity_id" => $mutation->stockable_id,
                "entity_type" => $mutation->stockable_type,
                "field" => "stock",
                "change" => $mutation->amount,
                "total" => DB::table("stock_mutations")->where("created_at", "<=", $mutation->created_at)->where("stockable_id", "=", $mutation->stockable_id)->sum("amount"),
                "description" => $mutation->description
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('stock.table'));
    }
}
