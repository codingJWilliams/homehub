<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vaults', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string("name", 128);
            $table->text("password_hash");
            $table->text("vault_key");
        });

        Schema::create('vault_folders', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->text("name");
            $table->bigInteger("size_kb");
            $table->integer("parent_folder_id");
            $table->integer("vault_id");
        });
        
        Schema::create('vault_files', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->text("file_key");
            $table->text("name");
            $table->bigInteger("size_kb");
            $table->integer("num_chunks");
            $table->integer("parent_folder_id");
            $table->integer("vault_id");
        });

        foreach (["Vault", "VaultFolder", "VaultFile"] as $entity) {
            foreach (["read", "edit", "delete"] as $action) {
                DB::table("user_permissions")->insert([
                    "created_at" => now(),
                    "updated_at" => now(),
                    "action" => $action,
                    "resource" => $entity
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
