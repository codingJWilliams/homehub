<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tuck_items', function ( $table) {
            $table->id();
            $table->timestamps();
            $table->string("name", 256);
            $table->string("bookers_id", 256)->nullable();
            $table->decimal("purchase_price");
            $table->decimal("sale_price");
        });

        foreach (["read", "edit", "delete"] as $action) {
            DB::table("user_permissions")->insert([
                "created_at" => now(),
                "updated_at" => now(),
                "action" => $action,
                "resource" => "TuckItem"
            ]);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
