<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('asset_roles', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string("name", 128);
        });

        Schema::create('asset_role_links', function (Blueprint $table) {
            $table->integer("asset_id");
            $table->integer("asset_role_id");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
