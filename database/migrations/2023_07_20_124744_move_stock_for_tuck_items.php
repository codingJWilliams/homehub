<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table("tuck_items", function (Blueprint $table) {
            $table->integer("stock_count");
        });

        foreach (DB::table("tuck_items")->get() as $item) {
            DB::table("tuck_items")->where("id", "=", $item->id)->update([
                "stock_count" => DB::table("attribute_history")->where("entity_id", "=", $item->id)->orderBy("created_at", "desc")->first()->total
            ]);
        }

        DB::table("attribute_history")->update([ "field" => "stock_count" ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
