<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::table("attribute_history")->where("description", "Automatic backfill")->delete();

        foreach (DB::table("tuck_items")->get() as $item) {
            foreach (DB::table("attribute_history")->where("entity_id", "=", $item->id)->where("entity_type", "=", "App\\Models\\TuckItem")->where("field", "=", "stock_count")->where("created_at", "<", "2023-07-20T15:28:08.000000Z")->get() as $stock) {
                if ($stock->change > 0) {
                    DB::table("attribute_history")->insert([
                        "created_at" => $stock->created_at,
                        "updated_at" => $stock->updated_at,
                        "entity_id" => $item->id,
                        "entity_type" => "App\\Models\\TuckItem",
                        "field" => "stored_stock",
                        "change" => 0 - $stock->change,
                        "total" => 6969,
                        "description" => "Automatic backfill"
                    ]);
                }
            }
        }

        foreach (DB::table("attribute_history")->where("created_at", "<", "2023-07-20T15:28:08.000000Z")->where("total", "=", 6969)->orderByDesc("created_at")->get() as $stock) {
            $history_after =
                DB::table("attribute_history")
                ->where("entity_id", "=", $stock->entity_id)
                ->where("entity_type", "=", $stock->entity_type)
                ->where("field", "=", $stock->field)
                ->where("created_at", ">", $stock->created_at)
                ->orderBy("created_at", "asc")
                ->first();

            if ($history_after) {
                $expected_total = $history_after->total - $stock->change;
                DB::table("attribute_history")->where("id", "=", $stock->id)->update(["total" => $expected_total]);
            }
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
