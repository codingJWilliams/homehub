<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('asset_links', function (Blueprint $table) {
            $table->unique(["from_asset_id", "to_asset_id"]);
        });

        Schema::table('asset_role_links', function (Blueprint $table) {
            $table->unique(["asset_id", "asset_role_id"]);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
