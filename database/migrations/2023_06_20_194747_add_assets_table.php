<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create("assets", function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string("tag", 18)->unique()->nullable();
            $table->integer("model_id");
            $table->string("name", 128)->nullable();
            $table->text("notes")->nullable();
            $table->text("serial_number")->nullable();
            $table->integer("container_id");
            $table->decimal("purchase_value");
            $table->timestamp("purchased_at");
        });

        Schema::create("asset_models", function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string("name", 64)->nullable();
            $table->string("manufacturer", 64)->nullable();

            $table->integer("model_id");
            $table->text("notes")->nullable();
            $table->text("serial_number")->nullable();
            $table->integer("container_id");
            $table->decimal("purchase_value");
            $table->timestamp("purchased_at");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
