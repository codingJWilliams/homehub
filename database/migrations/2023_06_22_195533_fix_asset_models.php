<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::dropIfExists("asset_models");
        Schema::create("asset_models", function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string("name", 64)->nullable();
            $table->string("manufacturer", 64)->nullable();
            $table->text("link")->nullable();
            $table->text("notes")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
