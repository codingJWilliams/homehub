<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tupperware', function (Blueprint $table) {
            $table->integer("recipe_id")->nullable();
            $table->decimal("portion_cost")->nullable();
            $table->timestamp("cooked_at")->nullable();
            $table->integer("container_id")->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tupperware', function (Blueprint $table) {
            //
        });
    }
};
