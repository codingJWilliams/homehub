<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tuck_deals', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string("name", 128);
            $table->double("sale_price");
            $table->boolean("active");
        });

        Schema::create('tuck_deal_items_a', function (Blueprint $table) {
            $table->id();
            $table->integer("tuck_deal_id");
            $table->integer("tuck_item_id");
        });

        Schema::create('tuck_deal_items_b', function (Blueprint $table) {
            $table->id();
            $table->integer("tuck_deal_id");
            $table->integer("tuck_item_id");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
