<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tuck_money_counts', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->timestamp("counted_at");
            $table->decimal("external_change");
            $table->decimal("amount");
        });

        foreach (["read", "edit", "delete"] as $action) {
            DB::table("user_permissions")->insert([
                "created_at" => now(),
                "updated_at" => now(),
                "action" => $action,
                "resource" => "TuckMoneyCount"
            ]);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tuck_money_count');
    }
};
