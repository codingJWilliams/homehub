<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        foreach (["read", "edit", "delete"] as $action) {
            DB::table("user_permissions")->insert([
                "created_at" => now(),
                "updated_at" => now(),
                "action" => $action,
                "resource" => "TuckDeal"
            ]);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
