<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        foreach (DB::table("tuck_items")->get() as $item) {
            DB::table("attribute_history")->insert([
                "created_at" => now(),
                "updated_at" => now(),
                "entity_id" => $item->id,
                "entity_type" => "App\\Models\\TuckItem",
                "field" => "stored_stock",
                "change" => $item->stored_stock,
                "total" => $item->stored_stock,
                "description" => "Initial Value"
            ]);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
