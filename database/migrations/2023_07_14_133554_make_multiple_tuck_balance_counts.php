<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tuck_money_counts', function (Blueprint $table) {
            $table->decimal("float_amount");
        });

        DB::raw("UPDATE tuck_money_counts SET float_amount=amount");

        Schema::table('tuck_money_counts', function (Blueprint $table) {
            $table->dropColumn('amount');
            $table->decimal('lockbox_amount');
            $table->decimal('bank_amount');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
