resource "random_password" "jwt_key" {
  length  = 128
  special = false
}
resource "random_password" "app_key" {
  length  = 128
  special = false
}

resource "vercel_project" "homehub" {
  count                      = var.deploy_to_vercel ? 1 : 0
  name                       = "homehub"
  serverless_function_region = var.vercel_region
  team_id                    = null

  vercel_authentication = {
    protect_production = false
  }

  git_repository = {
    type              = "gitlab"
    repo              = "codingJWilliams/homehub"
    production_branch = "master"
  }

  environment = [
    {
      key    = "DB_HOST"
      value  = "aws.connect.psdb.cloud"
      target = ["production", "preview", "development"]
    },
    {
      key    = "APP_ENV"
      value  = "production"
      target = ["production"]
    },
    {
      key    = "APP_KEY"
      value  = random_password.app_key.result
      target = ["production"]
    },
    {
      key    = "DB_PORT"
      value  = "3306"
      target = ["production", "preview", "development"]
    },
    {
      key    = "DB_CONNECTION"
      value  = "mysql"
      target = ["production", "preview", "development"]
    },
    {
      key    = "MYSQL_ATTR_SSL_CA"
      value  = "/etc/pki/tls/certs/ca-bundle.crt"
      target = ["production", "preview", "development"]
    },
    {
      key    = "DB_USERNAME"
      value  = var.planetscale_username
      target = ["production"]
    },
    {
      key    = "DB_PASSWORD"
      value  = var.planetscale_password
      target = ["production"]
    },
    {
      key    = "DB_DATABASE"
      value  = var.planetscale_database_name
      target = ["production"]
    },
    {
      key    = "JWT_SECRET"
      value  = random_password.jwt_key.result
      target = ["production"]
    },
    {
      key    = "JWT_ALGO"
      value  = "HS256"
      target = ["production"]
    },
    {
      key    = "AZURE_TENANT_ID"
      value  = var.azure_tenant_id
      target = ["production"]
    },
    {
      key    = "AZURE_REDIRECT_URI"
      value  = "https://${var.vercel_domain}/api/auth/callback"
      target = ["production"]
    },
    {
      key    = "AZURE_CLIENT_ID"
      value  = azuread_service_principal.main.application_id
      target = ["production"]
    },
    {
      key    = "AZURE_CLIENT_SECRET"
      value  = azuread_application_password.app.value
      target = ["production"]
    },
    {
      key    = "AWS_ACCESS_KEY_ID"
      value  = var.s3_access_key_id,
      target = ["production"]
    },
    {
      key    = "AWS_BUCKET"
      value  = var.s3_bucket,
      target = ["production"]
    },
    {
      key    = "AWS_DEFAULT_REGION"
      value  = var.s3_region,
      target = ["production"]
    },
    {
      key    = "AWS_SECRET_ACCESS_KEY"
      value  = var.s3_secret_access_key,
      target = ["production"]
    },
    {
      key    = "AWS_ENDPOINT"
      value  = var.s3_endpoint,
      target = ["production"]
    },
    {
      key    = "AWS_USE_PATH_STYLE_ENDPOINT"
      value  = "true",
      target = ["production"]
    }
  ]

  lifecycle {
    ignore_changes = [
      "team_id"
    ]
  }
}

resource "vercel_project_domain" "homehub" {
  count      = var.deploy_to_vercel ? 1 : 0
  project_id = vercel_project.homehub[0].id
  domain     = var.vercel_domain
  team_id    = null
  lifecycle {
    ignore_changes = [
      "team_id"
    ]
  }
}
