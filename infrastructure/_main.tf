provider "aws" {
  region  = var.region
  profile = var.profile

  default_tags {
    tags = {
      service = module.this.id
    }
  }
}

provider "azurerm" {
  features {}
}

provider "vercel" {
  # Reads from the VERCEL_API_TOKEN environment variable
  team = var.vercel_team
}

provider "azuread" {
  tenant_id = var.azure_tenant_id
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }

    vercel = {
      source  = "vercel/vercel"
      version = "~> 0.4"
    }

    azuread = {
      source  = "hashicorp/azuread"
      version = "~> 2.15.0"
    }
  }

  backend "s3" {
    key     = "homehub/terraform.tfstate"
    encrypt = false
  }
}

module "this" {
  source = "cloudposse/label/null"

  namespace   = "g8"
  environment = var.environment
  stage       = "homehub"

  label_order = ["namespace", "stage", "environment", "name"]
}
