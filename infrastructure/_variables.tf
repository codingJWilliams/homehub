variable "profile" {
  description = "AWS profile"
  type        = string
}

variable "region" {
  description = "AWS region to deploy to"
  type        = string
}

variable "account_id" {
  description = "AWS account ID"
  type        = string
}

variable "environment" {
  description = "Environment"
  type        = string
}

variable "application_name" {
  type = string
  default = "HomeHub"
}

variable "azure_tenant_id" {
  description = "Tenant ID to enable authentication for"
  type        = string
}


variable "vercel_team" {
  description = "Slug of team in Vercel (example: g8)"
  type        = string
  default     = null
}

variable "vercel_region" {
  description = "Vercel region to deploy to"
  type        = string
  default     = "lhr1"
}

variable "vercel_domain" {
  description = "Vercel domain to deploy to"
  type        = string
}

variable "planetscale_database_name" {
  type = string
}

variable "planetscale_username" {
  type = string
}

variable "planetscale_password" {
  type = string
}

variable "deploy_to_vercel" {
  type = bool
  default = true
}

variable "protocol" {
  type = string
  default = "https"
}

variable "s3_access_key_id" {
  type = string
}
variable "s3_bucket" {
  type = string
}
variable "s3_secret_access_key" {
  type = string
}
variable "s3_endpoint" {
  type = string
}
variable "s3_region" {
  type = string
}