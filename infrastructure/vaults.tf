resource "aws_s3_bucket" "vaults" {
  bucket = "${module.this.id}-vaults"

  lifecycle_rule {
    abort_incomplete_multipart_upload_days = 0
    enabled                                = true
    id                                     = "delete_old"

    noncurrent_version_expiration {
      days = 7
    }
  }
}

resource "aws_s3_bucket_versioning" "vaults" {
  bucket = aws_s3_bucket.vaults.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_public_access_block" "vaults" {
  bucket = aws_s3_bucket.vaults.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

data "aws_iam_policy_document" "vaults" {
  statement {
    actions = [
      "s3:ListBucket",
    ]

    resources = [
      "arn:aws:s3:::${aws_s3_bucket.vaults.bucket}",
    ]
  }
  statement {
    actions = [
      "s3:GetObject",
      "s3:PutObject",
      "s3:DeleteObject",
    ]

    resources = [
      "arn:aws:s3:::${aws_s3_bucket.vaults.bucket}/*",
    ]
  }
}

resource "aws_iam_user" "vaults" {
  name = "${module.this.id}-vaults"
}

resource "aws_iam_access_key" "vaults" {
  user = aws_iam_user.vaults.name
}

resource "aws_iam_user_policy" "vaults" {
  name   = "${module.this.id}-vaults"
  user   = aws_iam_user.vaults.name
  policy = data.aws_iam_policy_document.vaults.json
}

output "AWS_ACCESS_KEY_ID" {
  value = aws_iam_access_key.vaults.id
}

output "AWS_SECRET_ACCESS_KEY" {
  value     = aws_iam_access_key.vaults.secret
  sensitive = true
}

output "AWS_DEFAULT_REGION" {
  value = var.region
}

output "AWS_BUCKET" {
  value = aws_s3_bucket.vaults.bucket
}
