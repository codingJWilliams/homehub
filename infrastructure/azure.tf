resource "azuread_application" "app" {
  display_name = "${var.application_name} [${module.this.environment}]"
  owners       = [data.azuread_client_config.current.object_id]

  api {
    mapped_claims_enabled = true
  }

  web {
    homepage_url = "${var.protocol}://${var.vercel_domain}"
    redirect_uris = ["${var.protocol}://${var.vercel_domain}/api/auth/callback"]

    implicit_grant {
      access_token_issuance_enabled = true
      id_token_issuance_enabled     = true
    }
  }
}

resource "azuread_application_password" "app" {
  application_object_id = azuread_application.app.object_id
}

data "azuread_application_published_app_ids" "well_known" {}

resource "azuread_service_principal" "msgraph" {
  application_id = data.azuread_application_published_app_ids.well_known.result.MicrosoftGraph
  use_existing   = true
}

resource "azuread_service_principal_delegated_permission_grant" "adminconsent" {
  service_principal_object_id          = azuread_service_principal.main.object_id
  resource_service_principal_object_id = azuread_service_principal.msgraph.object_id
  claim_values                         = ["openid", "User.Read"]
}

data "azuread_client_config" "current" {}

data "azurerm_subscription" "primary" {
}

resource "azuread_service_principal" "main" {
  application_id               = azuread_application.app.application_id
  app_role_assignment_required = false
  owners                       = [data.azuread_client_config.current.object_id]

  login_url                     = "${var.protocol}://${var.vercel_domain}"
  preferred_single_sign_on_mode = "oidc"


  feature_tags {
    enterprise = true
  }
}