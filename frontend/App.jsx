import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

import * as cry from "./Helpers/crypto/key-derivation";
import * as ff from "./Helpers/crypto/files";


import './app.css';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import LayoutProvider from './Common/Layout/LayoutProvider';
import Home from "./Pages/Home";
import * as Audit from "./Pages/Audit";
import * as Asset from "./Pages/Asset";
import * as AssetModel from "./Pages/AssetModel";
import * as AssetRole from "./Pages/AssetRole";
import * as Container from "./Pages/Container";
import * as User from "./Pages/User";
import * as UserRole from "./Pages/UserRole";
import * as UserPermission from "./Pages/UserPermission";
import * as Vault from "./Pages/Vault";
import * as VaultFolder from "./Pages/VaultFolder";
import useStickyState from './Helpers/useStickyState';
import { useUrlParameter } from './Helpers/hooks';
import { useEffect } from 'react';
import AppFullpageMessage from './Components/AppFullpageMessage';
import AssetMassUpdate from './Pages/AssetMassUpdate';
import AssetOverview from './Pages/AssetOverview';
window.cry = cry;
window.ff = ff;
const crudComponents = (base, resource) => (
  [
    {
      path: base,
      element: <resource.List />
    },
    {
      path: `${base}/:id`,
      element: <resource.View />
    }
  ]
);

const router = createBrowserRouter([
  {
    element: <LayoutProvider />,
    children:
      crudComponents("/asset/model", AssetModel)
        .concat(crudComponents("/asset/role", AssetRole))
        .concat(crudComponents("/asset", Asset))
        .concat(crudComponents("/audit", Audit))
        .concat(crudComponents("/user/role", UserRole))
        .concat(crudComponents("/user/permission", UserPermission))
        .concat(crudComponents("/user", User))
        .concat(crudComponents("/container", Container))
        .concat(crudComponents("/vault/folder", VaultFolder))
        // .concat(crudComponents("/vault/file", VaultFile))
        .concat(crudComponents("/vault", Vault))
        .concat([
          {
            path: "/",
            element: <Home />
          },
          {
            path: "/asset/overview",
            element: <AssetOverview />
          },
          {
            path: "/asset/massupdate",
            element: <AssetMassUpdate />
          }
        ])
  },
]);

function App() {
  const [token, setToken] = useStickyState("token");
  const [returnToken, setReturnToken] = useUrlParameter("token", "");

  useEffect(() => {
    if (!token && returnToken) {
      setToken(returnToken);
      setReturnToken("");
    } else if (!token) {
      window.location.href = `${process.env.REACT_APP_API ? process.env.REACT_APP_API : ''}/api/auth`;
    }
  }, [ token, returnToken ]);

  if (!token) return <AppFullpageMessage message="Authenticating..." />;
  return <RouterProvider router={router} />;
}

export default App;