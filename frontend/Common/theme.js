import { createTheme, darken } from "@mui/material";

export const theme = createTheme({
    palette: {
        mode: 'dark',
        primary: {
            main: '#ffa31a',
        },
        secondary: {
            main: '#8a5100',
        },
        warning: {
            main: '#fd6d1e',
        },
        background: {
            offset: '#1c1c1c',
            paper: "#121212"
        },
        text: {
            primary: `rgb(248, 249, 250)`
        }
    },
    typography: {
        fontFamily: 'system-ui, -apple-system, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", "Liberation Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"',
        fontWeightRegular: 400,
        subtitle2: {
            color: darken("#ffffff", 0.55),
            fontSize: "14px"
        }
    },
    components: {
        MuiTab: {
            styleOverrides: {
                root: ({theme}) => ({
                    textTransform: "none",
                    minWidth: 0,
                    minHeight: 0,
                    padding: `${theme.spacing(1.2)} ${theme.spacing(1.5)}`,
                })
            }
        },
        MuiTabs: {
            styleOverrides: {
                root:{
                    minHeight: 0,
                }
            }
        }
    }
});

window.theme = theme;