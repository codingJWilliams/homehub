import { Box, Breadcrumbs, Tabs, Typography } from "@mui/material";

export default function TabbedPage({ breadcrumb, title, subtitle, titleButtons, tabs, onTabChange, tabValue, tabContent }) {
    return (
        <>
            <Box
                sx={{
                    mx: -2,
                    // [theme.breakpoints.up("sm")]: {
                    //     mx: -3
                    // },
                    mt: -4,
                    mb: 2,
                    p: 3,
                    px: 2,
                    pb: 0,
                    bgcolor: "background.offset"
                }}
            >
                <Breadcrumbs>
                    {breadcrumb}
                </Breadcrumbs>
                <Box sx={{ my: 1, display: "flex", justifyContent: "space-between", alignItems: "center", flexWrap: "wrap" }}>
                    <Box>
                        <Typography variant="h4">{title}</Typography>
                        <Typography variant="subtitle2">{subtitle}</Typography>
                    </Box>
                    <Box>{titleButtons}</Box>
                </Box>
                <Tabs value={tabValue} onChange={(evt, value) => onTabChange(value)} children={tabs} />
            </Box>
            {tabContent}
        </>
    )
};