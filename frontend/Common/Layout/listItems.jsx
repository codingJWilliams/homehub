import * as React from 'react';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import PlaceIcon from '@mui/icons-material/Place';
import FormatListNumberedIcon from '@mui/icons-material/FormatListNumbered';
import TakeoutDiningIcon from '@mui/icons-material/TakeoutDining';
import HomeIcon from '@mui/icons-material/Home';
import { ListSubheader, Divider } from '@mui/material';
import { Category, Construction, DataArray, Fastfood, Groups, History, Icecream, Lock, Money, Numbers, Person, PersonAdd, PieChart, Smartphone, Tag } from '@mui/icons-material';

export const mainList = {
  General: [
    {
      page: "Home",
      icon: HomeIcon,
      url: "/",
      dashboardHidden: true
    },
    {
      page: "Places",
      icon: PlaceIcon,
      url: "/container",
      model: "Container",
    },
    {
      page: "Vaults",
      model: "Vault",
      icon: Lock,
      url: "/vault"
    },
  ],
  "Asset Management": [
    {
      page: "Assets",
      icon: Smartphone,
      model: "Asset",
      url: "/asset"
    },
    {
      page: "Models",
      icon: Category,
      model: "AssetModel",
      url: "/asset/model"
    },
    {
      page: "Roles",
      model: "AssetRole",
      icon: Construction,
      url: "/asset/role"
    },
    {
      page: "Overview",
      icon: DataArray,
      model: "AssetOverview",
      permission: { action: "read", resource: "Asset" },
      url: "/asset/overview"
    },
    {
      page: "Mass Update",
      icon: Numbers,
      url: "/asset/massupdate"
    }
  ],
  "System": [
    {
      page: "Users",
      icon: Person,
      model: "User",
      url: "/user"
    },
    {
      page: "Roles",
      icon: Groups,
      model: "UserRole",
      url: "/user/role"
    },
    {
      page: "Permissions",
      icon: PersonAdd,
      model: "UserPermission",
      url: "/user/permission"
    },
    {
      page: "Audit Log",
      icon: History,
      model: "Audit",
      url: "/audit"
    },
  ]
};

export function getPageByUrl() {
  for (const submenu of Object.keys(mainList)) {
    const found = mainList[submenu].find(entry => window.location.pathname.startsWith(entry.url));
    if (found) return found;
  }
}

export function buildMainList(navigate, permissions) {
  const hasPermission = ({ action, resource }) => permissions.some(perm => perm.action === action && perm.resource === resource);
  const permissionFilter = sidebarEntry => {
    if (sidebarEntry.permission) return hasPermission(sidebarEntry.permission);
    else if (sidebarEntry.model) return hasPermission({ action: "read", resource: sidebarEntry.model });
    return true;
  }

  const list = {};
  for (const groupName of Object.keys(mainList)) {
    const allowedGroupItems = mainList[groupName].filter(permissionFilter);
    if (allowedGroupItems.length) list[groupName] = allowedGroupItems;
  }

  return (
    <React.Fragment>
      {Object.keys(list).map(key => (
        <React.Fragment key={key}>
          <ListSubheader component="div">
            {key}
          </ListSubheader>
          {list[key].filter(permissionFilter).map(entry => (
            <ListItemButton key={entry.url} onClick={() => navigate(entry.url)}>
              <ListItemIcon sx={{ minWidth: "40px" }} >
                <entry.icon />
              </ListItemIcon>
              <ListItemText primary={entry.page} />
            </ListItemButton>
          ))}
          <Divider sx={{ my: 1 }} />
        </React.Fragment>
      ))}
    </React.Fragment>
  );
}