import { AppBar, Box, Container, CssBaseline, Divider, Drawer, IconButton, List, Paper, TextField, ThemeProvider, Toolbar, styled } from "@mui/material";
import { theme } from "../theme";
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import { useContext, useState } from "react";
import { buildMainList } from "./listItems";
import { Outlet, useNavigate } from "react-router-dom";
import NavbarSearchInput from "../../Components/NavbarSearchInput";
import { ApplicationContext } from "../../Context/ApplicationContext";

const drawerWidth = 240;

const CustomAppBar = styled(AppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}));

const CustomDrawer = styled(Drawer, { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        '& .MuiDrawer-paper': {
            position: 'relative',
            whiteSpace: 'nowrap',
            width: drawerWidth,
            transition: theme.transitions.create('width', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen,
            }),
            boxSizing: 'border-box',
            ...(!open && {
                overflowX: 'hidden',
                transition: theme.transitions.create('width', {
                    easing: theme.transitions.easing.sharp,
                    duration: theme.transitions.duration.leavingScreen,
                }),
                width: 0,
                // width: theme.spacing(7),
                // [theme.breakpoints.up('sm')]: {
                //     width: theme.spacing(9),
                // },
            }),
        },
    }),
);

export default function Layout() {
    const [open, setOpen] = useState(window.innerWidth > theme.breakpoints.values.sm);
    const navigate = useNavigate();
    const applicationContext = useContext(ApplicationContext);
    const [mainList] = useState(buildMainList(navigate, applicationContext.permissions));
    const toggleDrawer = () => {
        setOpen(!open);
    };

    return (
        <Box sx={{ display: 'flex', height: "100vh" }}>
            <CssBaseline />
            <CustomAppBar position="absolute" open={open}>
                <Toolbar
                    sx={{
                        pr: '24px', // keep right padding when drawer closed
                        bgcolor: "primary.main"
                    }}
                >
                    <IconButton
                        edge="start"
                        color="inherit"
                        aria-label="open drawer"
                        onClick={toggleDrawer}
                        sx={{
                            marginRight: '36px',
                            ...(open && { display: 'none' }),
                        }}
                    >
                        <MenuIcon />
                        {/* Menu */}
                    </IconButton>
                    <NavbarSearchInput onClick={() => {
                        if (window.openCommandBar) window.openCommandBar();
                    }} />
                </Toolbar>
            </CustomAppBar>
            <CustomDrawer variant="permanent" open={open}>
                <Toolbar
                    sx={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'flex-end',
                        px: [1]
                    }}
                >
                    <IconButton onClick={toggleDrawer}>
                        <ChevronLeftIcon />
                    </IconButton>
                </Toolbar>
                <Divider />
                <List component="nav">
                    {mainList}
                </List>
            </CustomDrawer>
            <Box
                component="main"
                sx={{
                    backgroundColor: (theme) =>
                        theme.palette.mode === 'light'
                            ? theme.palette.grey[100]
                            : undefined,
                    flexGrow: 1,
                    overflow: 'auto',
                }}
            >
                <Toolbar />
                <Box sx={{ mt: 4, mb: 4, px: 2 }}>
                    <Outlet />
                </Box>
            </Box>
        </Box>
    )
}