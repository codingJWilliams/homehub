import { LocalizationProvider } from "@mui/x-date-pickers";
import { ApplicationContext } from "../../Context/ApplicationContext";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { ThemeProvider } from "@mui/material";
import { SnackbarProvider } from "notistack";
import { theme } from "../theme";
import { useApi } from "../../Helpers/api";
import AppFullpageMessage from "../../Components/AppFullpageMessage";
import Layout from ".";
import 'dayjs/locale/en-gb';
import { CommandBarHandler } from "../../Components/CommandBar/CommandBarHandler";

export default function Providers({ children }) {
    const { data, isLoading, error } = useApi("/context/");

    let component = <Layout />;
    if (isLoading)
        return (
            <AppFullpageMessage message="Loading..." />
        );
    if (error)
        return (
            <AppFullpageMessage message="Error initialising app" />
        );

    return (
        <ApplicationContext.Provider value={data}>
            <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="en-gb">
                <ThemeProvider theme={theme}>
                    <SnackbarProvider />
                    <CommandBarHandler />
                    {component}
                </ThemeProvider>
            </LocalizationProvider>
        </ApplicationContext.Provider>
    );
}