import { Box, Paper, TablePagination, Typography } from "@mui/material";
import NoResults from "../../Components/NoResults";
import SearchInput from "../../Components/SearchInput";
import ResultsTable from "./ResultsTable";
import ErrorMessage from "../../Components/ErrorMessage";

export default function PaginatedIndex({ resource, componentProps, indexResponse, limit, setLimit, setOffset, query, setQuery, noSearch, forceFocus, limitedOptions = false }) {
    const { data, isLoading, error } = indexResponse;

    const search = (setQuery && !noSearch) ? <SearchInput forceFocus={forceFocus} value={query} onSearch={setQuery} /> : null;

    if (error)
        return (
            <>
                {search}
                <ErrorMessage exception={error} />
            </>
        )

    if (!isLoading && (!data.items || !data.items.length))
        return (
            <>
                {search}
                <NoResults />
            </>
        );

    return (
        <>
            {search}
            <ResultsTable
                items={isLoading ? [] : data.items}
                resource={resource}
                isLoading={isLoading}
                {...componentProps}
            />
            {data ?
                <TablePagination
                    rowsPerPageOptions={limitedOptions ? [10, 15, 20, 50] : [10, 25, 50, 250]}
                    component="div"
                    count={data.pagination.total}
                    page={Math.floor(data.pagination.offset / data.pagination.limit)}
                    onPageChange={(_, newPage) => { setOffset(newPage * limit) }}
                    rowsPerPage={limit}
                    onRowsPerPageChange={(_, el) => { setLimit(el.props.value); }}
                /> : null}
        </>
    )
}