import { useState } from "react";
import { enqueueSnackbar } from "notistack";
import Content from "./ViewPage/Content";
import { Box, Modal, Typography } from "@mui/material";
import { updateResource } from "../../../Helpers/api";
import ButtonAsync from "../../../Components/ButtonAsync";
import { theme } from "../../theme";
import { ValidationError } from "../../../Helpers/errors";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    bgcolor: 'background.paper',
    border: '2px solid white',
    boxShadow: 24,
    minWidth: '50%',
    p: 4,
    [theme.breakpoints.down('sm')]: {
        minWidth: '90%'
    }
};

export default function CreateModal({ resource, resourceName, open, form, onSave }) {
    const [clientData, setClientData] = useState({});
    const [validationErrors, setValidationErrors] = useState({});

    const performCreate = async () => {
        try {
            const { id } = await updateResource(`${resource}/create`, clientData);

            enqueueSnackbar(`Created successfully`, { "variant": "success" });
            setValidationErrors({});
            if (id) onSave(id);
        } catch (error) {
            if (error instanceof ValidationError) {
                setValidationErrors(error.problems);
                enqueueSnackbar(`There were some problems with the form`, { variant: "error" })
            } else {
                setValidationErrors({});
                enqueueSnackbar(error.message, { variant: "error" });
            }
        }
    };

    const updateAttributeCb = (attribute, value) => {
        setClientData({
            ...clientData,
            [attribute]: value
        });
    };

    return (
        <Modal
            open={open}
            onClose={() => onSave()}
        >
            <Box sx={style}>
                <Typography variant="h5">Create an {resourceName}</Typography>
                <Content
                    tab={{
                        type: "form",
                        fields: form
                    }}
                    data={clientData}
                    updateAttributeCb={updateAttributeCb}
                    validationErrors={validationErrors}
                />
                <ButtonAsync ajax={performCreate} color="primary">Create</ButtonAsync>
            </Box>
        </Modal>
    );
}