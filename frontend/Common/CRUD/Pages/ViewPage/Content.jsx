import AuditsTab from "../../Tabs/AuditsTab";
import FormTab from "../../Tabs/FormTab";
import ManyRelationTab from "../../Tabs/ManyRelationTab";
import RelationTab from "../../Tabs/RelationTab";
import Table from "../../Tabs/RelationTab/Table";

export default function Content({ tab, data, updateAttributeCb, resource, validationErrors, isCreate }) {
    if (tab.type === "form")
        return (
            <FormTab
                fields={tab.fields}
                data={data}
                updateAttributeCb={updateAttributeCb}
                validationErrors={validationErrors}
            />
        );
    if (isCreate) return null;
    if (tab.type === "relation")
        return (
            <RelationTab
                displayComponent={Table}
                items={data[tab.key]}
                relation={tab.key}
                resource={resource}
                updateAttributeCb={updateAttributeCb}
                displayProps={{
                    columns: tab.columns,
                    resource: tab.resource
                }}
            />
        );
    if (tab.type === "many_relation")
        return (
            <ManyRelationTab
                items={data[tab.key]}
                relation={tab.key}
                resource={resource}
                updateAttributeCb={updateAttributeCb}
                columns={tab.columns}
                tab={tab}
            />
        );
    if (tab.type === "audits")
        return (
            <AuditsTab
                resource={resource}
            />
        )
    if (tab.type === "custom")
        return (
            <tab.component
                resource={resource}
                data={data}
            />
        )
    if (tab.type === "audits")
        return (
            <AuditsTab
                resource={resource}
            />
        )
}