import { useApi } from "../../../../Helpers/api";
import LoadingWrapper from "../../../../Components/LoadingWrapper";
import Page from "./Page";
import FakeTabbedPage from "../../../../Components/SkeletonTabbedPage";

export default function ViewPage({ resource, resourceName, resourceNameKey = "name", tabs, readonly }) {
    const { data, isLoading, error } = useApi(resource);
    const isCreate = resource.endsWith("/create");

    return (
        <LoadingWrapper isLoading={isLoading} error={error} fakeComponent={<FakeTabbedPage />}>
            <Page {...{ resource, resourceName, resourceNameKey, tabs, data, isCreate, readonly }} />
        </LoadingWrapper>
    );
}