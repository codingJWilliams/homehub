import { useEffect, useState } from "react";
import { LoadingButton } from "@mui/lab";
import { deleteResource, updateResource } from "../../../../Helpers/api";
import TabbedPage from "../../../Layout/TabbedPage";
import { enqueueSnackbar } from "notistack";
import ButtonAsync from "../../../../Components/ButtonAsync";
import Content from "./Content";
import BadgedTab from "../../../../Components/BadgedTab";
import { Tab } from "@mui/material";
import { useNavigate } from "react-router-dom";
import formatDate, { formatDateRelative } from "../../../../Helpers/formatDate";
import { ValidationError } from "../../../../Helpers/errors";

export default function Page({ resource, resourceName, resourceNameKey = "name", isCreate, tabs, data, readonly }) {
    const [clientData, setClientData] = useState(data);
    const [isDeleting, setIsDeleting] = useState(false);
    const [validationErrors, setValidationErrors] = useState({});
    const [tabIndex, setTabIndex] = useState(0);
    const navigate = useNavigate();
    
    useEffect(() => {
        setClientData(data);
    }, [resource, data?.id]);

    const baseResource = `${resource.split("/").slice(0, -1).join("/")}`;

    const performUpdate = async () => {
        try {
            const { id } = await updateResource(resource, clientData);

            setValidationErrors({});
            if (isCreate && id) navigate(`${resource.replace("create", id)}`);
            enqueueSnackbar(`Saved ${resourceName.toLowerCase()} successfully`, { "variant": "success" });
        } catch (error) {
            if (error instanceof ValidationError) {
                setValidationErrors(error.problems);
                enqueueSnackbar(`There were some problems with the form`, { variant: "error" })
            } else {
                setValidationErrors({});
                enqueueSnackbar(error.message, { variant: "error" });
            }
        }
    };

    const performDelete = async () => {
        setIsDeleting(true);
        const { success } = await deleteResource(resource);
        if (success) {
            navigate(baseResource);
        }
        setIsDeleting(false);
    };

    const tab = tabs[tabIndex];

    const updateAttributeCb = (attribute, value) => {
        if (readonly) return;
        if (typeof attribute === 'object') {
            setClientData({
                ...clientData,
                ...attribute
            });
        } else {
            setClientData({
                ...clientData,
                [attribute]: value
            });
        }
    };

    const getTabLabel = (tab, data, idx) => {
        if (tab.type === "relation" || tab.type === "many_relation")
            return (
                <BadgedTab
                    label={tab.label}
                    key={idx}
                    badge={data[tab.key] ? data[tab.key].length : null}
                />
            );

        return <Tab
            icon={tab.icon}
            iconPosition="start"
            key={idx}
            label={tab.label}
            value={idx}
        />;
    };

    const titleButtons = readonly ? [] : [
        <ButtonAsync
            key="save"
            variant="contained"
            ajax={performUpdate}
        >
            Save
        </ButtonAsync>,
        <LoadingButton key="delete" sx={{ mx: 1 }} variant="outlined" color="error" loading={isDeleting} onClick={performDelete}>Delete</LoadingButton>
    ];

    return (
        <TabbedPage
            title={isCreate ? `Create ${resourceName}` : (clientData[resourceNameKey] ?? resourceName)}
            subtitle={isCreate ? null : `Created ${formatDate(clientData.created_at)} · Updated ${formatDateRelative(clientData.updated_at)}`}
            titleButtons={titleButtons}
            tabs={tabs.filter(tabVal => tabVal.type === "form" || !isCreate).map((tabVal, idx) => getTabLabel(tabVal, clientData, idx))}
            onTabChange={setTabIndex}
            tabValue={tabIndex}
            tabContent={
                <Content
                    tab={tab}
                    data={clientData}
                    updateAttributeCb={updateAttributeCb}
                    validationErrors={validationErrors}
                    resource={resource}
                    isCreate={isCreate}
                />
            }
        />
    );
}