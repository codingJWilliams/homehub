import { Button, Tab } from "@mui/material";
import PaginatedIndex from "../PaginatedIndex";
import { Add } from "@mui/icons-material";
import TabbedPage from "../../Layout/TabbedPage";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useApi } from "../../../Helpers/api";
import BadgedTab from "../../../Components/BadgedTab";
import { useDebouncedFunction, useUrlParameter } from "../../../Helpers/hooks";
import useStickyState from "../../../Helpers/useStickyState";

export default function IndexPage({ resource, resourceName, componentProps, noSearch }) {
    const [tab, setTab] = useState("results");
    const [stringOffset, setOffset] = useUrlParameter("offset", "0");
    const offset = parseInt(stringOffset);
    const [limit, setLimit] = useStickyState("pagination_limit", 10);
    const currentParams = new URLSearchParams(window.location.search);
    const [search, setSearch] = useState(currentParams.get("search") || "");
    const [query, setQuery] = useUrlParameter("search", "");
    const debouncedSetSearch = useDebouncedFunction(setSearch, 300);
    const navigate = useNavigate();

    const paginationParams = new URLSearchParams();
    paginationParams.append("pagination[offset]", offset);
    paginationParams.append("pagination[limit]", limit);
    if (!noSearch) paginationParams.append("query", search);
    const indexResponse = useApi(`${resource}?${paginationParams}`, { headers: { Accept: "application/json" } });

    let content;

    if (tab === "results") {
        content = (
            <PaginatedIndex 
                {...{ resource, componentProps, indexResponse, limit, setLimit, setOffset, noSearch, query }} 
                setQuery={q => {
                    setQuery(q);
                    debouncedSetSearch(q);
                }}
            />
        );
    }

    return (
        <TabbedPage
            title={resourceName}
            titleButtons={<>
                <Button onClick={() => navigate(`${resource}create`)} variant="outlined" label="Add" startIcon={<Add />}>Add</Button>
            </>}
            tabs={[
                <BadgedTab key="results" badge={(indexResponse.data && !indexResponse.error) ? indexResponse.data.pagination.total : 0} label="Results" value="results" />,
            ]}
            onTabChange={setTab}
            tabValue={tab}
            tabContent={content}
        />
    );
};