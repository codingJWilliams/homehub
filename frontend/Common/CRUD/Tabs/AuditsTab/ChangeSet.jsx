import { Chip } from "@mui/material";

export default function ChangeSet({ changes }) {
    return (
        <>
            {Object.keys(changes).map(key => (
                <Chip key={key} label={`${key}: ${changes[key]}`} />
            ))}
        </>
    );
}