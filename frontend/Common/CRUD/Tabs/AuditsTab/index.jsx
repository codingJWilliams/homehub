import PaginatedIndex from "../../PaginatedIndex";
import { useState } from "react";
import { useApi } from "../../../../Helpers/api";
import ChangeSet from "./ChangeSet";
import ucfirst from "../../../../Helpers/ucfirst";
import useStickyState from "../../../../Helpers/useStickyState";
import { AuditLogTable } from "../../../../Pages/Audit/List";

export default function AuditsTab({ resource }) {
    const [offset, setOffset] = useState(0);
    const [limit, setLimit] = useStickyState("pagination_limit", 10);

    const paginationParams = new URLSearchParams();
    paginationParams.append("pagination[offset]", offset);
    paginationParams.append("pagination[limit]", limit);
    const indexResponse = useApi(`${resource}/audits?${paginationParams}`, { headers: { Accept: "application/json" } });

    const columns = [...AuditLogTable].filter(c => c.field !== "object");

    return (
        <PaginatedIndex
            {...{ indexResponse, limit, setLimit, setOffset }}
            resource={`${resource}/audits`}
            componentProps={{
                openLink: row => `/audit/${row.id}`,
                columns
            }}
        />
    );
}