import { updateResource } from "../../../../Helpers/api";
import NoResults from "../../../../Components/NoResults";
import { enqueueSnackbar } from "notistack";
import Table from "./Table";
import ButtonAsync from "../../../../Components/ButtonAsync";
import { Box, Button } from "@mui/material";
import { Add, LinkOff } from "@mui/icons-material";
import { useState } from "react";
import AjaxPickerModal from "../../../../Components/AjaxPickerModal";
import LoadingWrapper from "../../../../Components/LoadingWrapper";

export default function ManyRelationTab({ relation, updateAttributeCb, resource, items, columns, tab }) {
    const [selections, setSelections] = useState([]);
    const [modalOpen, setModalOpen] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    const relationPath = `${resource}/relation/${relation}`;

    const onDetach = async (ids) => {
        try {
            await updateResource(relationPath, {
                "$detach": ids
            });

            updateAttributeCb(relation, items.filter(it => !ids.includes(it.id)));
            enqueueSnackbar(`Detached successfully`, { variant: "success" });
            setSelections([]);
        } catch (error) {
            enqueueSnackbar(error.message, { variant: "error" });
        }
    }

    const onAttach = async ids => {
        setIsLoading(true);
        try {
            const response = await updateResource(relationPath, {
                "$attach": ids
            });

            updateAttributeCb(relation, response[relation]);
            enqueueSnackbar(`Attached successfully`, { variant: "success" });
        } catch (error) {
            enqueueSnackbar(error.message, { variant: "error" });
        }
        setIsLoading(false);
    }

    const buttons = (
        <Box sx={{ display: "flex", gap: "10px" }}>
            <Button color="primary" variant="outlined" onClick={() => setModalOpen(true)} startIcon={<Add />}>{tab.attachLabel ?? "Attach"}</Button>
            <ButtonAsync disabled={!selections.length} color="error" variant="outlined" ajax={() => onDetach(selections)} startIcon={<LinkOff />}>{tab.detachLabel ?? "Detach"}</ButtonAsync>
        </Box>
    );

    const modal = modalOpen ?
        <AjaxPickerModal
            isOpen={modalOpen}
            selectionCb={ids => {
                if (ids) onAttach(ids)
                setModalOpen(false);
            }}
            resource={tab.attachResource}
            title={tab.pickerTitle}
            initialValue={undefined}
            excludeIds={items.map(it => it.id)}
            multiple
            clearable={false}
            columns={tab.columns}
        /> : null;

    if (!items.length) return (
        <>
            {modal}
            {buttons}
            <Box sx={{ mt: 1 }} />
            <NoResults />
        </>
    );

    return (
        <>
            {modal}
            {buttons}
            <Box sx={{ mt: 1 }} />
            <LoadingWrapper isLoading={isLoading}>
                <Table resource={tab.resource} {...{ columns, onDetach, selections, setSelections, items }} />
            </LoadingWrapper>
        </>
    );
}