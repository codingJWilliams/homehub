import { Button, ButtonGroup } from "@mui/material";
import ResultsTable from "../../ResultsTable";
import { useNavigate } from "react-router-dom";
import { useState } from "react";

export default function Table({ items, columns, selections, setSelections }) {
    const navigate = useNavigate();

    const tableCols = [
        ...columns,
        // {
        //     field: 'actions',
        //     headerName: 'Actions',
        //     flex: 3,
        //     sortable: false,
        //     renderCell: params => <>
        //         <ButtonGroup variant="outlined">
        //             <Button size="small" onClick={() => navigate(`${resource}/${params.row?.id}`)} color="info">View</Button>
        //             <Button size="small" onClick={() => onDetach(params.row?.id)}>Detach</Button>
        //         </ButtonGroup>
        //     </>
        // }
    ];

    return (
        <ResultsTable
            items={items}
            columns={tableCols} 
            onRowSelectionModelChange={setSelections}
            rowSelectionModel={selections}
        />
    );
}