import { Box, Typography } from "@mui/material";
import { PaperCard, PaperCardBody, PaperCardHeader } from "../../../Components/PaperCard";

export default function FormTab({ fields, data, updateAttributeCb, validationErrors }) {
    const formFields = fields.map((field, i) => (
        <Box key={field.key} sx={{ pb: i === (fields.length-1) ? 0 : 2 }}>
            <field.component field={field} data={data} errors={validationErrors[field.key]} updateAttributeCb={updateAttributeCb} />
        </Box>
    ));

    return (
        <PaperCard>
            <PaperCardBody>
                {formFields}
            </PaperCardBody>
        </PaperCard>
    )
}