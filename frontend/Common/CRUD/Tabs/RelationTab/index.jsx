import { updateResource } from "../../../../Helpers/api";
import NoResults from "../../../../Components/NoResults";
import { enqueueSnackbar } from "notistack";

export default function RelationTab({ displayComponent, displayProps, relation, updateAttributeCb, resource, items }) {

    const Component = displayComponent;
    const relationPath = `${resource}/relation/${relation}`;

    const onDetach = async (id) => {
        try {
            await updateResource(relationPath, {
                "$detach": [id]
            });

            updateAttributeCb(relation, items.filter(it => it.id !== id));
            enqueueSnackbar(`Detached successfully`, { variant: "success" });
        } catch (error) {
            enqueueSnackbar(error.message, { variant: "error" });
        }
    }

    if (!items.length) return <NoResults />;

    return <Component {...displayProps} onDetach={onDetach} items={items} />;
}