import { Button, ButtonGroup } from "@mui/material";
import ResultsTable from "../../ResultsTable";
import { useNavigate } from "react-router-dom";

export default function Table({ items, columns, resource, onDetach }) {
    const navigate = useNavigate();

    const tableCols = [
        ...columns,
        {
            field: 'actions',
            headerName: '',
            flex: 0,
            sortable: false,
            renderCell: params => <>
                <Button size="small" onClick={() => navigate(`${resource}/${params.row?.id}`)}>Open</Button>
            </>
        }
    ];

    return <ResultsTable items={items} columns={tableCols} />
}