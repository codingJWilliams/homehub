import { useState } from "react";
import { InputAdornment, TextField } from "@mui/material";

export default function Number({ field, data, updateAttributeCb, errors }) {
    const currentValue = data[field.key] ?? "";
    const [value, setValue] = useState(currentValue);

    if (field.currency) {
        field.step = 0.01;
        field.adornment = "£";
    }

    const InputProps = field.adornment ? {
        startAdornment: <InputAdornment position="start">{field.adornment}</InputAdornment>,
    } : {};

    const inputProps = field.step ? {
        step: field.step
    } : {};
    
    return (
        <TextField 
            id={field.key} 
            label={field.label}
            value={value}
            error={!!(errors && errors.length)}
            helperText={errors ? errors.join(", ") : null}
            sx={{ width: "100%" }}
            type="number"
            inputProps={inputProps}
            InputProps={InputProps}
            onChange={event => {
                setValue(event.target.value);
                updateAttributeCb(field.key, event.target.value);
            }}
        />
    );
}