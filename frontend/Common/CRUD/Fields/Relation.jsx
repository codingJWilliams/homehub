import { useState } from "react";
import { IconButton, InputAdornment, TextField } from "@mui/material";
import { Edit, Link } from "@mui/icons-material";
import { useApi } from "../../../Helpers/api";
import AjaxPickerModal from "../../../Components/AjaxPickerModal";
import { useNavigate } from "react-router-dom";

export default function Relation({ field, data, updateAttributeCb, errors }) {
    const currentValue = data[field.key] ?? "";
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const navigate = useNavigate();

    const { relationDisplayProperty, relationPath } = field;
    const { data: displayData, isLoading, error, refresh } = useApi(`${relationPath}/${data[field.key]}`, { headers: { Accept: "application/json" } });

    let displayValue = "Error processing data";

    if (!data[field.key]) displayValue = "";
    else if (isLoading) displayValue = "Loading...";
    else if (displayData[relationDisplayProperty]) displayValue = displayData[relationDisplayProperty];

    return (
        <>
            {modalIsOpen ?
                <AjaxPickerModal
                    isOpen={true}
                    selectionCb={(id) => {
                        if (id !== undefined) {
                            updateAttributeCb(field.key, id);
                        }
                        setModalIsOpen(false);
                    }}
                    resource={relationPath}
                    title={field.pickerTitle}
                    initialValue={currentValue}
                    creationForm={field.creationForm}
                    columns={field.columns}
                /> : null}
            <TextField
                id={field.key}
                label={field.label}
                value={displayValue}
                error={errors && errors.length}
                helperText={errors ? errors.join(", ") : null}
                disabled
                sx={{
                    width: "100%",
                    "& .MuiInputBase-root input": {
                        WebkitTextFillColor: "white !important"
                    }
                }}
                InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                            <IconButton sx={{ mr: 0.3 }} edge="end" onClick={() => navigate(`${relationPath}/${currentValue}`)}> <Link /> </IconButton>
                            <IconButton edge="end" onClick={() => setModalIsOpen(true)}> <Edit /> </IconButton>
                        </InputAdornment>
                    )
                }}
            />
        </>
    );
}