import { useState } from "react";
import { TextField } from "@mui/material";

export default function Text({ field, data, updateAttributeCb, errors }) {
    const currentValue = data[field.key] ?? "";
    const [value, setValue] = useState(currentValue);
    
    return (
        <TextField 
            id={field.key} 
            label={field.label}
            value={value}
            error={!!(errors && errors.length)}
            helperText={errors ? errors.join(", ") : null}
            sx={{ width: "100%" }}
            onChange={event => {
                setValue(event.target.value);
                updateAttributeCb(field.key, event.target.value);
            }}
        />
    );
}