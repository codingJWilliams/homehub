import { useState } from "react";
import { IconButton, InputAdornment, TextField } from "@mui/material";
import { QrCode } from "@mui/icons-material";
import BarcodeScannerModal from "../../../Components/BarcodeScannerModal";

export default function Barcode({ field, data, updateAttributeCb, errors }) {
    const currentValue = data[field.key] ?? "";
    const [value, setValue] = useState(currentValue);
    const [modalIsOpen, setModalIsOpen] = useState(false);
    
    return (
        <>
        <BarcodeScannerModal
            isOpen={modalIsOpen}
            selectionCb={(result) => {
                setModalIsOpen(false);
                setValue(result ?? "");
                updateAttributeCb(field.key, result);
            }}
        />
        <TextField
            id={field.key} 
            label={field.label}
            value={value}
            error={!!(errors && errors.length)}
            helperText={errors ? errors.join(", ") : null}
            sx={{ width: "100%" }}
            InputProps={{
                endAdornment: (
                    <InputAdornment position="end">
                        <IconButton edge="end" onClick={() => setModalIsOpen(true)}><QrCode /></IconButton>
                    </InputAdornment>
                )
            }}
            onChange={event => {
                setValue(event.target.value);
                updateAttributeCb(field.key, event.target.value);
            }}
        />
        </>
    );
}