import { useState } from "react";
import { FormControlLabel, FormGroup, Switch, Typography } from "@mui/material";

export default function Boolean({ field, data, updateAttributeCb, errors }) {
    const currentValue = data[field.key] ?? "";
    const [value, setValue] = useState(currentValue);

    return (
        <>
            <FormGroup sx={{ ml: 1 }}> 
                <FormControlLabel
                    control={
                        <Switch
                            checked={value}
                            onChange={evt => {
                                setValue(evt.target.checked);
                                updateAttributeCb(field.key, evt.target.checked);
                            }}
                            color={(errors && errors.length) ? "error" : undefined}
                            inputProps={{ 'aria-label': 'controlled' }}
                        />
                    }
                    label={field.label}
                />
            </FormGroup>
            {
                (!errors || !errors.length) ||
                <Typography variant="subtitle2" color="error">{errors ? errors.join(", ") : null}</Typography>
            }
        </>
    );
}