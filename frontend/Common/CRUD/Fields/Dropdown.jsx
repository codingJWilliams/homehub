import { useState } from "react";
import { MenuItem, TextField } from "@mui/material";

export default function Dropdown({ field, data, updateAttributeCb, errors }) {
    const currentValue = data[field.key] ?? "";
    const [value, setValue] = useState(currentValue);
    
    return (
        <TextField 
            id={field.key} 
            label={field.label}
            value={value}
            error={!!(errors && errors.length)}
            helperText={errors ? errors.join(", ") : null}
            select
            sx={{ width: "100%" }}
            onChange={event => {
                setValue(event.target.value);
                updateAttributeCb(field.key, event.target.value);
            }}
        >
            {field.options.map(option => (
                <MenuItem key={option.value} value={option.value}>{option.label}</MenuItem>
            ))}
        </TextField>
    );
}