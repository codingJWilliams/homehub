import { useState } from "react";
import { Box, TextField } from "@mui/material";
import ButtonAsync from "../../../Components/ButtonAsync";
import { aesEncrypt, bufferToHex, generateRandomAesKey, getKeysFromPassword } from "../../../Helpers/crypto/key-derivation";

export default function VaultKeys({ field, data, updateAttributeCb, errors }) {
    const [value, setValue] = useState("");

    return (
        <Box sx={{ width: "100%", display: "flex" }}>
            <TextField 
                id={field.key} 
                label="Encryption password"
                value={value}
                error={!!(errors && errors.length)}
                helperText={errors ? errors.join(", ") : null}
                sx={{ width: "100%" }}
                onChange={event => {
                    setValue(event.target.value);
                }}
            />
            <ButtonAsync ajax={async () => {
                const salt = Math.random().toString();
                const keys = await getKeysFromPassword(value, salt);
                const vaultKey = await generateRandomAesKey();
                const encryptedVaultKey = bufferToHex(await aesEncrypt(await crypto.subtle.exportKey("raw", vaultKey), keys.masterKey));
                console.log("doing it for", value, "is", bufferToHex(keys.masterPasswordHash));

                updateAttributeCb({
                    password_hash: bufferToHex(keys.masterPasswordHash),
                    vault_key: encryptedVaultKey,
                    salt
                });
            }}>Encrypt</ButtonAsync>
        </Box>
    );
}