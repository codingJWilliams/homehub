import { useState } from "react";
import { DatePicker, DateTimePicker } from "@mui/x-date-pickers";
import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";
import timezone from "dayjs/plugin/timezone";
import { Button, InputAdornment } from "@mui/material";

dayjs.extend(utc);
dayjs.extend(timezone);

export default function Date({ field, data, updateAttributeCb, errors }) {
    const currentValue = data[field.key] ?? "";
    const [value, setValue] = useState(currentValue);

    const Component = field.time ? DateTimePicker : DatePicker;

    window.dayjs = dayjs;

    return (
        <Component
            id={field.key}
            label={field.label}
            value={value ? dayjs.utc(value).tz("Europe/London") : null}
            slotProps={{
                textField: {
                    error: !!(errors && errors.length),
                    helperText: errors ? errors.join(", ") : null,
                    InputProps: {
                        startAdornment: (
                            <InputAdornment position="start">
                                <Button size="small" variant="outlined" color="inherit" onClick={() => {
                                    try {
                                        const d = dayjs();
                                        updateAttributeCb(field.key, d.toISOString());
                                        setValue(d);
                                    } catch (e) { }
                                }}>Now</Button>
                            </InputAdornment>
                        )
                    }
                },
            }}
            sx={{ width: "100%" }}
            onChange={(newValue) => {
                setValue(newValue);
                updateAttributeCb(field.key, newValue ? newValue.toISOString() : null);
            }}
        />
    );
}