import { Lock } from "@mui/icons-material";
import { Box, Button, FormControlLabel, FormGroup, Switch, TextField, Typography } from "@mui/material";
import { useState } from "react";
import { checkMasterPassword } from "../../../Helpers/crypto/key-derivation";
import ButtonAsync from "../../../Components/ButtonAsync";

export default function PasswordEntry({ setStoredPassword, setSessionPassword, verificationHash, salt }) {
    const [userPassword, setUserPassword] = useState("");
    const [remember, setRemember] = useState(false);

    return (
        <Box display="flex" flexDirection="column" alignItems="center" my={2}>
            <Typography variant="h2"></Typography>
            <Typography variant="h4" display="flex" alignItems={"center"} gap="10px">
                <Lock fontSize="inherit" /> This vault is locked
                </Typography>
            <Typography>Please enter the password to decrypt the vault. This will not be sent to the server.</Typography>

            <Box sx={{ my: 8 }}>
                <Box display="flex" flexDirection="row" alignItems="center">
                    <TextField label="Decryption password" value={userPassword} onChange={p => setUserPassword(p.target.value)} type="password" sx={{ width: "50vw" }} />
                    <ButtonAsync
                        sx={{ ml: 2 }} 
                        variant="contained"
                        ajax={async () => {
                            const passwordIsCorrect = await checkMasterPassword(userPassword, salt, verificationHash);
                            if (passwordIsCorrect) {
                                setSessionPassword(userPassword);
                                if (remember) setStoredPassword(userPassword);
                            } else {
                                alert("bad password :(");
                            }
                        }}
                    >
                        Open
                    </ButtonAsync>
                </Box>
                <FormGroup>
                    <FormControlLabel
                        control={
                            <Switch
                                checked={remember}
                                onChange={evt => {
                                    setRemember(evt.target.checked)
                                    // setValue(evt.target.checked);
                                    // updateAttributeCb(field.key, evt.target.checked);
                                }}
                            />
                        }
                        label={"Remember me"}
                    />
                </FormGroup>
            </Box>


            <Typography variant="subtitle2">Note that if you choose to remember the password, it will be stored in your browser's "local storage".</Typography>
        </Box>
    )
}