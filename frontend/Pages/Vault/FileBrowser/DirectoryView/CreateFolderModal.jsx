import { useEffect, useRef, useState } from "react";
import ModalBox from "../../../../Components/ModalBox";
import { Button, TextField, Typography } from "@mui/material";
import ButtonAsync from "../../../../Components/ButtonAsync";
import { aesEncryptText } from "../../../../Helpers/crypto/key-derivation";
import { updateResource } from "../../../../Helpers/api";

export default function CreateFolderModal({ parent_folder_id, vault_id, vaultKey, open, onClose, refreshView }) {
    const [name, setName] = useState("");
    const inputRef = useRef();

    useEffect(() => {
        if (open && inputRef.current) {
            console.log("ref ", inputRef.current);
            inputRef.current.focus();
        }
    }, [open, inputRef, inputRef.current])

    return (
        <ModalBox isOpen={open} onClose={onClose}>
            <Typography variant="h5">Create a folder</Typography>

            <TextField
                label="Name"
                value={name}
                sx={{ width: "100%", mt: 3 }}
                inputRef={inputRef}
                onChange={event => {
                    setName(event.target.value);
                }}
            />

            <ButtonAsync variant="contained" sx={{ mt: 2 }} ajax={async () => {
                const encryptedName = await aesEncryptText(name, vaultKey);
                await updateResource("/vault/folder/create", {
                    name: encryptedName,
                    parent_folder_id,
                    vault_id,
                    size_kb: 0
                });
                onClose();
                refreshView();
            }}>
                Create
            </ButtonAsync>
        </ModalBox>
    )
}