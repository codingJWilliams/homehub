import { useEffect, useState } from "react";
import LoadingWrapper from "../../../../Components/LoadingWrapper";
import Page from "../../../../Common/CRUD/Pages/ViewPage/Page";
import Directory from "./Directory";
import { useApi } from "../../../../Helpers/api";
import { aesDecrypt, aesDecryptText, bufferToString, hexToBuffer } from "../../../../Helpers/crypto/key-derivation";
import { processFileDownload } from "../../../../Helpers/crypto/files";
import TaskManager from "./TaskManager";
import PreviewModal from "./Preview/PreviewModal";

const sortByName = items => items.sort((a, b) => {
    const nameA = a.name.toUpperCase();
    const nameB = b.name.toUpperCase();
    if (nameA < nameB)
        return -1;
    if (nameA > nameB)
        return 1;
    return 0;
});

const decryptNames = async (items, vaultKey) => {
    const decrypted = [];
    for (const item of items) {
        decrypted.push({
            ...item,
            name: await aesDecryptText(item.name, vaultKey),
            filename: item.filename ? await aesDecryptText(item.filename, vaultKey) : null
        });
    }
    return decrypted;
};

export default function DirectoryView({ vault, vaultKey }) {
    const [currentFolderId, setCurrentFolderId] = useState("default");
    const [isPreviewingFile, setIsPreviewingFile] = useState(false);
    const [previewFile, setPreviewFile] = useState(null);
    const [tasks, setTasks] = useState([]);
    const { data, isLoading, error, refresh } = useApi(`/vault/${vault.id}/viewFolder?id=${currentFolderId}`, { headers: { Accept: "application/json" } });

    const [decryptedFiles, setDecryptedFiles] = useState(null);
    const [decryptedFolders, setDecryptedFolders] = useState(null);
    const [decryptingLoading, setDecryptingLoading] = useState(true);

    useEffect(() => {
        setDecryptingLoading(true);
        if (data && data.folders !== null && data.files !== null && data.files !== undefined && data.folders !== undefined) {
            (async function () {
                setDecryptedFiles(await decryptNames(data.files, vaultKey));
                setDecryptedFolders(await decryptNames(data.folders, vaultKey));
                setDecryptingLoading(false);
            })()
        } else {
            setDecryptingLoading(true);
        }
    }, [data ? data.folders : null, data ? data.files : null]);


    const addTask = task => setTasks([...tasks, task]);
    const updateTask = (id, task) => setTasks(currentTasks => currentTasks.map(t => (t.id === id) ? task : t));
    const removeTask = (id) => setTasks(currentTasks => currentTasks.filter(t => t.id !== id));
    const taskManager = { addTask, updateTask, removeTask, refresh };

    const preview = (file) => { setPreviewFile(file); setIsPreviewingFile(true); }


    return (
        <LoadingWrapper isLoading={isLoading || decryptingLoading} error={error}>
            <LoadingWrapper isLoading={decryptingLoading}>
                <TaskManager tasks={tasks} />
                {isPreviewingFile ? <PreviewModal
                    open={isPreviewingFile}
                    onClose={() => setIsPreviewingFile(false)}
                    file={previewFile}
                    vault={vault}
                    taskManager={taskManager}
                    vaultKey={vaultKey}
                    currentFolderId={currentFolderId}
                /> : null}
                <Directory
                    folders={decryptedFolders ? decryptedFolders : []}
                    files={decryptedFiles ? decryptedFiles : []}
                    breadcrumb={data ? data.breadcrumb : []}
                    vault={vault}
                    vaultKey={vaultKey}
                    taskManager={taskManager}
                    currentFolderId={currentFolderId === "default" ? null : parseInt(currentFolderId)}
                    navigateToCb={folder_id => {
                        setCurrentFolderId(folder_id);
                    }}
                    previewCb={preview}
                    downloadFileCb={async file => {
                        const baseTask = {
                            id: (Math.random() * 50).toString(),
                            type: "download",
                            name: file.name,
                        };
                        taskManager.addTask({
                            ...baseTask,
                            percent: 0,
                            description: "Starting download"
                        });
                        await processFileDownload(file, vaultKey, (description, percent) => {
                            taskManager.updateTask(baseTask.id, {
                                ...baseTask,
                                percent: parseInt(percent),
                                description
                            });
                        });
                        taskManager.updateTask(baseTask.id, {
                            ...baseTask,
                            percent: 100,
                            description: "Completed"
                        });
                        setTimeout(() => {
                            taskManager.removeTask(baseTask.id);
                        }, 2000);
                    }}
                    refreshView={() => refresh()}
                />
            </LoadingWrapper>
        </LoadingWrapper>
    );
}