import { useEffect, useRef, useState } from "react";
import ModalBox from "../../../../Components/ModalBox";
import { Box, Breadcrumbs, Button, TextField, Typography } from "@mui/material";
import ButtonAsync from "../../../../Components/ButtonAsync";
import { aesDecryptText, aesEncryptText } from "../../../../Helpers/crypto/key-derivation";
import { updateResource } from "../../../../Helpers/api";
import { Link } from "react-router-dom";
import DecryptingText from "../../../../Components/DecryptingText";
import StyleResetLink from "../../../../Components/StyleResetLink";

export default function Breadcrumb({ breadcrumb, vaultKey, navigateCallback }) {
    const effectiveData = [{ name: "Vault", id: "default", type: "fake" }].concat(breadcrumb).map(function (it) {
        if (it.type === "fake") return {...it, effectiveName: it.name};
        return {...it, effectiveName: <DecryptingText hex={it.name} vaultKey={vaultKey} />}
    });

    return (
        <Breadcrumbs aria-label="breadcrumb">
            {effectiveData.map((d, idx) => (idx === effectiveData.length - 1) ? (
                <Typography color="text.primary">
                    {d.effectiveName}
                </Typography>
            ) : (
                <StyleResetLink
                    underline="hover"
                    color="#7b7b7b"
                    style={{
                        textDecoration: "none"
                    }}
                    href="#"
                    onClick={() => navigateCallback(d.id)}
                >
                    {d.effectiveName}
                </StyleResetLink>
            ))}
        </Breadcrumbs>
    )
}