import { Description, Error, FileOpen, Folder, FolderZip, Photo, VideoFile } from "@mui/icons-material";

export default function MimetypeIcon({ file }) {
    if (file.type === "folder") return <Folder sx={{ mr: 1 }} />;
    
    if (!file.exists) return <Error sx={{ mr: 1, color: 'red' }} />;

    if ([
        ".jpg", ".png", ".jpeg"
    ].some(end => file.filename.endsWith(end))) return <Photo sx={{ mr: 1 }} />;

    if ([
        ".pdf", ".doc", ".docx", ".xls", ".xlsx", ".pptx"
    ].some(end => file.filename.endsWith(end))) return <Description sx={{ mr: 1 }} />;

    if ([
        ".mp4", ".avi", ".mpeg"
    ].some(end => file.filename.endsWith(end))) return <VideoFile sx={{ mr: 1 }} />;

    if ([
        ".zip", ".tar.gz", ".rar"
    ].some(end => file.filename.endsWith(end))) return <FolderZip sx={{ mr: 1 }} />;


    return <FileOpen sx={{ mr: 1 }} />;
}