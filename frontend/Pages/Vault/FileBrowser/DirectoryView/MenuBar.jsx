import { Box, Button } from "@mui/material";
import ButtonFile from "../../../../Components/ButtonFile";
import { processFile } from "../../../../Helpers/crypto/files";
import { aesEncryptText } from "../../../../Helpers/crypto/key-derivation";
import { Add, UploadFile } from "@mui/icons-material";
import CreateFolderModal from "./CreateFolderModal";
import { useEffect, useState } from "react";
import ButtonDropdown from "../../../../Components/ButtonDropdown";
import UploadFileModal from "./UploadFileModal";

export default function MenuBar({ vault, vaultKey, currentFolderId, refreshView, taskManager }) {
    const [createFolderOpen, setCreateFolderOpen] = useState(false);
    const [uploadFileOpen, setUploadFileOpen] = useState(false);
    const [currentFileObject, setCurrentFileObject] = useState(null);

    useEffect(() => {
        const pListener = function (e) {
            e.preventDefault();
        };

        window.addEventListener("dragover", pListener, false);
        window.addEventListener("drop", pListener, false);

        const dropListener = function (evt) {
            if (evt.dataTransfer.files.length) {
                setCurrentFileObject(evt.dataTransfer.files[0]);
                setUploadFileOpen(true);
            }
        };

        window.addEventListener("drop", dropListener, false);

        return () => {
            window.removeEventListener("dragover", pListener);
            window.removeEventListener("drop", pListener);
            window.removeEventListener("drop", dropListener);
        }
    })

    const handleNewClick = (index) => {
        if (index === 0) {
            // New Folder
            setCreateFolderOpen(true);
        }
    }

    return (
        <>
            <Box mt={1}>
                <ButtonDropdown
                    color="success"
                    labelText="New"
                    onClick={handleNewClick}
                    options={[
                        { name: "New Folder", type: "button" },
                        { name: "Upload File", type: "file" }
                    ]}
                    sx={{}}
                    onFileSelect={async f => {
                        setCurrentFileObject(f);
                        setUploadFileOpen(true);

                    }}
                />
            </Box>
            {createFolderOpen ?
                <CreateFolderModal
                    open={createFolderOpen}
                    onClose={() => setCreateFolderOpen(false)}
                    vault_id={vault.id}
                    vaultKey={vaultKey}
                    parent_folder_id={currentFolderId}
                    refreshView={refreshView}
                /> :
                null}
            {uploadFileOpen ?
                <UploadFileModal
                    open={uploadFileOpen}
                    onClose={() => setUploadFileOpen(false)}
                    browserFile={currentFileObject}
                    {...{ taskManager, vault, vaultKey, currentFolderId }}
                /> :
                null}
        </>
    );
}