import { useEffect, useRef, useState } from "react";
import ModalBox from "../../../../Components/ModalBox";
import { Button, TextField, Typography } from "@mui/material";
import ButtonAsync from "../../../../Components/ButtonAsync";
import { aesEncryptText } from "../../../../Helpers/crypto/key-derivation";
import { deleteResource, updateResource } from "../../../../Helpers/api";
import Text from "../../../../Common/CRUD/Fields/Text";
import { processFile } from "../../../../Helpers/crypto/files";

export default function DeleteFileModal({ open, onClose, file, refresh }) {
    return (
        <ModalBox isOpen={open} onClose={onClose}>
            <Typography variant="h5" mb={2}>Are you sure you want to delete '{file.name}'?</Typography>
            <Typography variant="subtitle1" mb={2}>This file will be permanently deleted.</Typography>


            <ButtonAsync variant="contained" color="error"  sx={{ mt: 2 }} ajax={async () => {
                await deleteResource(`/vault/file/${file.id}`, {});
                refresh();
                onClose();
            }}>
                Delete
            </ButtonAsync>
            <Button variant="outlined" color="inherit" sx={{ mt: 2, ml: 1 }} onClick={async () => {
                onClose();
            }}>
                Cancel
            </Button>
        </ModalBox>
    )
}