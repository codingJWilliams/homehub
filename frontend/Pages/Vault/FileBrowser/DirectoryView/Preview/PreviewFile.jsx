export default function PreviewFile({ file, href }) {
    console.log(file.filename)

    if ([
        ".jpg", ".png", ".jpeg"
    ].some(end => file.filename.endsWith(end))) return (
        <img style={{ width: "100%" }} src={href} />
    );
    //       

    if ([
        ".pdf"
    ].some(end => file.filename.endsWith(end))) return (
        <embed style={{ width: "100%", height: "80vh" }} src={href} type="application/pdf" />
    );
}