import { useEffect, useRef, useState } from "react";
import ModalBox from "../../../../../Components/ModalBox";
import { Box, Button, IconButton, TextField, Typography, lighten } from "@mui/material";
import ButtonAsync from "../../../../../Components/ButtonAsync";
import { aesEncryptText } from "../../../../../Helpers/crypto/key-derivation";
import { updateResource } from "../../../../../Helpers/api";
import Text from "../../../../../Common/CRUD/Fields/Text";
import TextArea from "../../../../../Common/CRUD/Fields/TextArea";
import Date from "../../../../../Common/CRUD/Fields/Date";
import { processFile, processFileDownload, saveBlob } from "../../../../../Helpers/crypto/files";
import { enqueueSnackbar } from "notistack";
import { Close, Download, Edit, Image } from "@mui/icons-material";
import { theme } from "../../../../../Common/theme";
import MimetypeIcon from "../MimetypeIcon";
import FullwidthCircular from "../../../../../Components/FullwidthCircular";
import TitledModalBox from "../../../../../Components/TitledModalBox";
import PreviewFile from "./PreviewFile";

export default function PreviewModal({ open, onClose, file, taskManager, vault, vaultKey, currentFolderId }) {
    const [isLoaded, setIsLoaded] = useState(false);
    const [loadedBlobUrl, setLoadedBlobUrl] = useState(null);
    const [loadedBlob, setLoadedBlob] = useState(null);

    useEffect(() => {
        setIsLoaded(false);
        (async function () {
            const baseTask = {
                id: (Math.random() * 50).toString(),
                type: "download",
                name: "Previewing " + file.name,
            };
            taskManager.addTask({
                ...baseTask,
                percent: 0,
                description: "Starting download"
            });
            const blob = await processFileDownload(file, vaultKey, (description, percent) => {
                taskManager.updateTask(baseTask.id, {
                    ...baseTask,
                    percent: parseInt(percent),
                    description
                });
            }, true);
            taskManager.updateTask(baseTask.id, {
                ...baseTask,
                percent: 100,
                description: "Completed"
            });
            setTimeout(() => {
                taskManager.removeTask(baseTask.id);
            }, 2000);

            const url = URL.createObjectURL(blob);
            setLoadedBlobUrl(url);
            setLoadedBlob(blob);
            setIsLoaded(true);
        })()
    }, [file.id]);

    return (
        <TitledModalBox
            isOpen={open}
            onClose={onClose}
            title={
                <Box sx={{ display: "flex", alignItems: "center", justifyContent: "space-between" }}>
                    <Typography variant="h5" display="flex" alignItems="center">
                        <MimetypeIcon file={file} /> {file.name}
                    </Typography>
                    <Box>
                        <IconButton disabled={!isLoaded}><Edit /></IconButton>
                        <IconButton onClick={() => saveBlob(file.filename, loadedBlob)} disabled={!isLoaded}><Download /></IconButton>
                        <IconButton onClick={onClose}><Close /></IconButton>
                    </Box>
                </Box>
            }
        >
            {isLoaded ?
                <PreviewFile file={file} href={loadedBlobUrl} /> :
                <FullwidthCircular sx={{ height: "50vh" }} />
            }
        </TitledModalBox>
    )

    // return (
    //     <ModalBox isOpen={open} onClose={onClose}>
    //         <Box sx={{ m: -2, mb: 2, p: 1, borderBottom: `1px solid ${lighten(theme.palette.background.paper, 0.15)}` }}>
    //             <Box sx={{ display: "flex", alignItems: "center", justifyContent: "space-between" }}>
    //                 <Typography variant="h5" display="flex" alignItems="center">
    //                     <MimetypeIcon file={file} /> {file.name}
    //                 </Typography>
    //                 <Box>
    //                     <IconButton disabled={!isLoaded}><Edit /></IconButton>
    //                     <IconButton onClick={() => saveBlob(file.filename, loadedBlob)} disabled={!isLoaded}><Download /></IconButton>
    //                     <IconButton onClick={onClose}><Close /></IconButton>
    //                 </Box>
    //             </Box>
    //         </Box>
    //         {isLoaded ?
    //             <img style={{ width: "100%" }} src={loadedBlobUrl} /> :
    //             <FullwidthCircular sx={{ height: "50vh" }} />
    //         }
    //     </ModalBox>
    // )
}