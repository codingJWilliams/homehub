import { FileDownload, UploadFile } from "@mui/icons-material";
import { Box, LinearProgress, Paper, Typography } from "@mui/material";

function LinearProgressWithLabel(props) {
    return (
        <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Box sx={{ width: '100%', minWidth: "14vw", mr: 1 }}>
                <LinearProgress variant="determinate" {...props} />
            </Box>
            <Box sx={{ minWidth: 35 }}>
                <Typography variant="body2" color="text.secondary">{`${Math.round(
                    props.value,
                )}%`}</Typography>
            </Box>
        </Box>
    );
}

export default function TaskManager({ tasks }) {
    return (
        <Box sx={{
            position: "absolute",
            bottom: 0,
            right: 30,
            zIndex: 6900
        }}>
            <Paper>
                {tasks.map((t, idx) => (
                    <Box key={idx} sx={{ display: "flex", justifyContent: "stretch", alignItems: "center", p: 1, borderTop: "1px solid black" }}>
                        <Typography display={"flex"} alignItems={"center"} variant="h3">
                            {t.type === "upload" ?
                                <UploadFile fontSize="inherit" /> :
                                <FileDownload fontSize="inherit" />
                            }
                        </Typography>
                        <Box>
                            <Typography variant="subtitle1" lineHeight={1.3}>{t.name}</Typography>
                            <Typography variant="subtitle2" lineHeight={1.2}>{t.description}</Typography>
                            <LinearProgressWithLabel value={t.percent} />
                        </Box>
                    </Box>
                ))}
            </Paper>
        </Box>
    );
}