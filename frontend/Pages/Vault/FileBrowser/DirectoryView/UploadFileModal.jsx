import { useEffect, useRef, useState } from "react";
import ModalBox from "../../../../Components/ModalBox";
import { Box, Button, TextField, Typography } from "@mui/material";
import ButtonAsync from "../../../../Components/ButtonAsync";
import { aesEncryptText } from "../../../../Helpers/crypto/key-derivation";
import { updateResource } from "../../../../Helpers/api";
import Text from "../../../../Common/CRUD/Fields/Text";
import TextArea from "../../../../Common/CRUD/Fields/TextArea";
import Date from "../../../../Common/CRUD/Fields/Date";
import { processFile } from "../../../../Helpers/crypto/files";
import { enqueueSnackbar } from "notistack";

export default function UploadFileModal({ open, onClose, browserFile, taskManager, vault, vaultKey, currentFolderId }) {
    const browserFileParts = (browserFile.name ?? '').split(".");
    let name = browserFileParts.filter((v, i) => i !== browserFileParts.length - 1).join(".");

    let receivedAtDate = new window.Date();
    if (/\d\d\d\d-\d\d-\d\d/.test(name.split(" ")[0])) {
        const [year, month, day] = name.split(" ")[0].split("-");
        receivedAtDate.setDate(parseInt(day));
        receivedAtDate.setMonth(parseInt(month) - 1);
        receivedAtDate.setYear(parseInt(year));
        receivedAtDate.setHours(0);
        receivedAtDate.setMinutes(0);
        name = name.split(" ").filter((v, i) => i !== 0).join(" ");
    } else {
        receivedAtDate.setTime(browserFile.lastModified);
    }

    const [formData, setFormData] = useState({
        name,
        received_at: receivedAtDate.toISOString()
    });

    const updateAttributeCb = (key, val) => setFormData({ ...formData, [key]: val });

    return (
        <ModalBox isOpen={open} onClose={onClose}>
            <Typography variant="h5" mb={2}>Upload a file</Typography>

            <Box sx={{ display: "flex", gap: "20px", flexDirection: "column" }}>
                <Text field={{ label: "Name", key: "name" }} data={formData} updateAttributeCb={updateAttributeCb} />
                <Date field={{ label: "Date", key: "received_at" }} data={formData} updateAttributeCb={updateAttributeCb} />
                <TextArea field={{ label: "Notes", key: "notes" }} data={formData} updateAttributeCb={updateAttributeCb} />
            </Box>

            <Button variant="contained" sx={{ mt: 2 }} onClick={async () => {
                onClose();
                const fileName = formData.name;
                const baseTask = {
                    id: (Math.random() * 50).toString(),
                    type: "upload",
                    name: fileName,
                };
                try {
                    taskManager.addTask({
                        ...baseTask,
                        percent: 0,
                        description: "Starting upload"
                    });
                    await processFile(browserFile, {
                        name: await aesEncryptText(fileName, vaultKey),
                        parent_folder_id: currentFolderId,
                        vault_id: vault.id,
                        notes: formData.notes,
                        received_at: formData.received_at
                    }, vaultKey, (description, percent) => {
                        taskManager.updateTask(baseTask.id, {
                            ...baseTask,
                            percent: parseInt(percent),
                            description
                        });
                    });
                    taskManager.updateTask(baseTask.id, {
                        ...baseTask,
                        percent: 100,
                        description: "Completed"
                    });
                    taskManager.refresh();
                    setTimeout(() => {
                        taskManager.removeTask(baseTask.id);
                    }, 2000);
                } catch (e) {
                    enqueueSnackbar("Error whilst uploading file", { variant: "error" });
                    taskManager.removeTask(baseTask.id);
                }
            }}>
                Upload
            </Button>
        </ModalBox>
    )
}