import { Box, Button, Chip, IconButton } from "@mui/material";
import ResultsTable from "../../../../Common/CRUD/ResultsTable";
import { Delete, Download } from "@mui/icons-material";
import { useState } from "react";
import Breadcrumb from "./Breadcrumb";
import MenuBar from "./MenuBar";
import { PaperCard, PaperCardBody } from "../../../../Components/PaperCard";
import MimetypeIcon from "./MimetypeIcon";
import StyleResetLink from "../../../../Components/StyleResetLink";
import DeleteFileModal from "./DeleteFileModal";
import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";
import timezone from "dayjs/plugin/timezone";

dayjs.extend(utc);
dayjs.extend(timezone);

const canPreview = filename => [".pdf", ".png", ".jpg", ".jpeg"].some(p => filename.endsWith(p));

export default function Directory({ folders, files, breadcrumb, vault, currentFolderId, vaultKey, navigateToCb, downloadFileCb, refreshView, taskManager, previewCb }) {
    const [deleteFileOpen, setDeleteFileOpen] = useState(false);
    const [deleteFile, setDeleteFile] = useState(false);
    //downloadFileCb(params.row)
    const [columns] = useState([
        {
            field: 'name',
            headerName: 'Name',
            flex: 5,
            sortable: false,
            renderCell: params =>
                params.row.type === "folder" ?
                    <>
                        <MimetypeIcon file={params.row} />
                        <StyleResetLink color="white" href="#" onClick={() => navigateToCb(params.row.id)}>
                            {params.row.name}
                        </StyleResetLink>
                    </> :
                    <>
                        <Chip label={<span style={{ fontSize: "0.9rem" }}>{dayjs.utc(params.row.received_at).tz("Europe/London").format('DD MMM YYYY')}</span>} style={{ marginRight: 5, width: "115px" }} />
                        <MimetypeIcon file={params.row} />
                        <StyleResetLink color="white" href="#" onClick={() => canPreview(params.row.filename) ? previewCb(params.row) : downloadFileCb(params.row)}>
                            {params.row.name}
                        </StyleResetLink>
                    </>
        },
        {
            field: 'action',
            headerName: 'Actions',
            flex: 0,
            sortable: false,
            renderCell: params => <Box sx={{ display: "flex", justifyContent: "flex-end", width: "100%" }}>
                {
                    params.row.type === "folder" ?
                        (
                            <IconButton aria-label="delete">
                                <Delete />
                            </IconButton>
                        ) :
                        (
                            <>
                                <IconButton onClick={() => downloadFileCb(params.row)}>
                                    <Download />
                                </IconButton>
                                <IconButton aria-label="delete" onClick={() => {
                                    setDeleteFile(params.row);
                                    setDeleteFileOpen(true);
                                }}>
                                    <Delete />
                                </IconButton>
                            </>
                        )
                }
            </Box>

        },
    ]);

    return (
        <>
            <PaperCard sx={{ mb: 1 }}>
                <PaperCardBody sx={{ p: 1 }}>
                    <Breadcrumb breadcrumb={breadcrumb} vaultKey={vaultKey} navigateCallback={navigateToCb} />
                    <MenuBar {...{ vault, vaultKey, currentFolderId, refreshView, taskManager }} />
                </PaperCardBody>
            </PaperCard>
            <ResultsTable
                items={folders.map(f => ({ ...f, type: "folder" })).concat(files.map(f => ({ ...f, type: "file" })))}
                smartLoading={false}
                columns={columns}
            />
            <DeleteFileModal open={deleteFileOpen} onClose={() => setDeleteFileOpen(false)} file={deleteFile} refresh={refreshView} />
        </>
    )
}