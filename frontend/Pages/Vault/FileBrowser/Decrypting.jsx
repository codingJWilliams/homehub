import { Lock } from "@mui/icons-material";
import { Box, Button, CircularProgress, FormControlLabel, FormGroup, Switch, TextField, Typography } from "@mui/material";
import { useState } from "react";
import { checkMasterPassword } from "../../../Helpers/crypto/key-derivation";
import ButtonAsync from "../../../Components/ButtonAsync";

export default function Decrypting({  }) {
    return (
        <Box display="flex" flexDirection="column" alignItems="center" my={2} py={5}>
            <CircularProgress />
            <Typography variant="h6">Decrypting</Typography>
        </Box>
    )
}