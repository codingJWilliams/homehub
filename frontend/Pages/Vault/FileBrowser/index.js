import { useEffect, useState } from "react";
import useStickyState from "../../../Helpers/useStickyState";
import { PaperCard, PaperCardBody } from "../../../Components/PaperCard";
import PasswordEntry from "./PasswordEntry";
import Decrypting from "./Decrypting";
import DirectoryView from "./DirectoryView";
import { aesDecrypt, aesEncrypt, bufferToAesKey, bufferToHex, generateRandomAesKey, getKeysFromPassword, hexToBuffer } from "../../../Helpers/crypto/key-derivation";

function str2ab(str) {
    var buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
    var bufView = new Uint16Array(buf);
    for (var i = 0, strLen = str.length; i < strLen; i++) {
      bufView[i] = str.charCodeAt(i);
    }
    return buf;
  }
  

export default function FileBrowser({ data }) {
    const [sessionPassword, setSessionPassword] = useState(null);
    const [storedPassword, setStoredPassword] = useStickyState(`vault_${data.id}_password`, "");
    const [vaultKeyBuffer, setVaultKeyBuffer] = useState(null);
    const [vaultKey, setVaultKey] = useState(null);
    const effectivePassword = sessionPassword || storedPassword;

    const calculateKey = async (pw) => {
        const keys = await getKeysFromPassword(pw, data.salt);
        const masterKey = await bufferToAesKey(keys.masterKey);
        console.log(bufferToHex(await aesEncrypt(await crypto.subtle.exportKey("raw", await generateRandomAesKey()), masterKey)));
        const vaultKey = await aesDecrypt(hexToBuffer(data.vault_key), masterKey);
        setVaultKeyBuffer(true);
        setVaultKey(vaultKey);

        window.k = vaultKey;

        // console.log(bufferToHex(await aesEncrypt(str2ab("test encrypted :)"), vaultKey)));
    };

    useEffect(() => {
        if (effectivePassword) calculateKey(effectivePassword);
    }, [effectivePassword]);


    if (!effectivePassword) return (
        <PaperCard>
            <PaperCardBody>
                <PasswordEntry {...{setStoredPassword, setSessionPassword}} salt={data.salt} verificationHash={data.password_hash} />
            </PaperCardBody>
        </PaperCard>
    );

    if (!vaultKey) return (
        <PaperCard>
            <PaperCardBody>
                <Decrypting />
            </PaperCardBody>
        </PaperCard>
    );

    return <DirectoryView vault={data} vaultKey={vaultKey} />;
}