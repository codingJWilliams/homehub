import IndexPage from "../../Common/CRUD/Pages/IndexPage";

export const VaultTable = [
    { field: 'name', headerName: 'Name', flex: 2, sortable: false }
];

export default function List() {
    return (
        <IndexPage
            resource={`/vault/`}
            resourceName="Vaults"
            componentProps={{
                openLink: row => `/vault/${row.id}`,
                columns: VaultTable
            }}
        />
    );
}