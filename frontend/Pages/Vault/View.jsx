import Text from "../../Common/CRUD/Fields/Text";
import ViewPage from "../../Common/CRUD/Pages/ViewPage";
import { Edit, Folder } from "@mui/icons-material";
import { useParams } from "react-router-dom";
import { AuditTabDefinition } from "../../Common/CRUD/Tabs/AuditsTab/AuditTabDefinition";
import FileBrowser from "./FileBrowser";
import VaultKeys from "../../Common/CRUD/Fields/VaultKeys";

export const VaultForm = [
    { key: "name", component: Text, label: "Name" },
    { key: "password_hash", component: VaultKeys, label: "Encryption keys" },
];

export default function View() {
    const { id } = useParams();

    return (
        <ViewPage
            resource={`/vault/${id}`}
            resourceName="Vault"
            tabs={[
                {
                    type: "form",
                    label: "Properties",
                    icon: <Edit fontSize="small" />,
                    fields: VaultForm
                },
                {
                    type: "custom",
                    label: "File browser",
                    icon: <Folder fontSize="small" />,
                    component: FileBrowser
                },
                AuditTabDefinition
            ]}
        />
    );
}