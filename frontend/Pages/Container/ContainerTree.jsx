import { DataGrid } from "@mui/x-data-grid";
import { theme } from "../../Common/theme";
import { Button, darken } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { PaperCard, PaperCardBody } from "../../Components/PaperCard";
import RenderTree from "./RenderTree";
import ErrorMessage from "../../Components/ErrorMessage";
import NoResults from "../../Components/NoResults";
import FullwidthCircular from "../../Components/FullwidthCircular";

// export default function ContainerTree({ items, columns, openLink, compact, onRowSelectionModelChange = () => { }, rowSelectionModel, getRowClassName, smartLoading = true }) {
export default function ContainerTree({ resource, componentProps, indexResponse }) {
    const { data, isLoading, error } = indexResponse;

    if (error)
        return (
            <>
                <ErrorMessage exception={error} />
            </>
        )

    if (isLoading) 
        return (
            <FullwidthCircular />
        );


    return (
        <PaperCard>
            <PaperCardBody>
                <RenderTree data={data} />
            </PaperCardBody>
        </PaperCard>
    );

    return JSON.stringify(data);
    // const navigate = useNavigate();
    // const cols = [...columns];

    // if (openLink)
    //     cols.push({
    //         field: 'actions',
    //         headerName: '',
    //         flex: 0,
    //         sortable: false,
    //         renderCell: params => <Button onClick={() => navigate(openLink(params.row))}>Open</Button>
    //     });

    // return (
    //     <DataGrid
    //         rows={items}
    //         columns={cols}
    //         pagination={null}
    //         autoHeight
    //         hideFooterPagination={true}
    //         disableColumnFilter
    //         loading={smartLoading ? items.length <= 0 : false}
    //         disableVirtualization
    //         density={compact ? "compact" : "standard"}
    //         disableRowSelectionOnClick
    //         rowSelectionModel={rowSelectionModel}
    //         getRowClassName={getRowClassName || (() => {})}
    //         checkboxSelection={rowSelectionModel !== undefined}
    //         onRowSelectionModelChange={onRowSelectionModelChange}
    //         keepNonExistentRowsSelected
    //         disableColumnMenu
    //         hideFooter
    //         sx={{
    //             bgcolor: "background.offset",
    //             borderColor: darken(theme.palette.divider, 0.45),
    //             "& .MuiDataGrid-row .MuiDataGrid-cell": {
    //                 borderColor: darken(theme.palette.divider, 0.45)
    //             },
    //             "& .MuiDataGrid-columnHeaders": {
    //                 borderColor: darken(theme.palette.divider, 0.45)
    //             },
    //             "& .MuiDataGrid-row.greyed-out": {
    //                 backgroundColor: darken(theme.palette.background.paper, 0.1)
    //             }
    //         }}
    //     />
    // );
}