import IndexPage from "../../Common/CRUD/Pages/IndexPage";
import ResultsTable from "../../Common/CRUD/ResultsTable";
import FakeTable from "../../Components/FakeTable";
import TypeLabel from "../../Components/TypeLabel";
import ContainerIndexPage from "./ContainerIndexPage";
import ContainerTree from "./ContainerTree";

export const ContainerTable = [
    { field: 'type', headerName: 'Type', flex: 2, sortable: false, renderCell: params => <TypeLabel type={params.row?.type} /> },
    {
        field: 'name',
        headerName: 'Name',
        flex: 4,
        sortable: false
    }
];

export default function List() {
    return (
        <ContainerIndexPage
            resource={`/container/tree`}
            resourceName="Places"
            componentOverride={ContainerTree}
            forceLimit={1000}
            loadingComponent={<FakeTable cols={["Type", "Name"]} rows={10} />}
            componentProps={{
                openLink: row => `/container/${row.id}`,
                columns: ContainerTable
            }}
        />
    );
}