import { Button, Tab } from "@mui/material";
import ContainerTree from "./ContainerTree";
import { Add } from "@mui/icons-material";
import TabbedPage from "../../Common/Layout/TabbedPage";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useApi } from "../../Helpers/api";
import BadgedTab from "../../Components/BadgedTab";
import { useDebouncedFunction, useUrlParameter } from "../../Helpers/hooks";
import useStickyState from "../../Helpers/useStickyState";

export default function ContainerIndexPage({ resource, componentProps, noSearch }) {
    const [tab, setTab] = useState("tree");
    const navigate = useNavigate();
    const indexResponse = useApi(`${resource}`, { headers: { Accept: "application/json" } });

    let content;

    if (tab === "tree") {
        content = (
            <ContainerTree 
                {...{ resource, componentProps, indexResponse }}
            />
        );
    }

    return (
        <TabbedPage
            title={"Places"}
            titleButtons={<>
                <Button onClick={() => navigate(`/container/create`)} variant="outlined" label="Add" startIcon={<Add />}>Add</Button>
            </>}
            tabs={[
                <Tab key="tree" label="Tree" value="tree" />,
            ]}
            onTabChange={setTab}
            tabValue={tab}
            tabContent={content}
        />
    );
};