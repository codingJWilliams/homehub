import Text from "../../Common/CRUD/Fields/Text";
import ViewPage from "../../Common/CRUD/Pages/ViewPage";
import Relation from "../../Common/CRUD/Fields/Relation";
import Dropdown from "../../Common/CRUD/Fields/Dropdown";
import TextArea from "../../Common/CRUD/Fields/TextArea";
import { Edit } from "@mui/icons-material";
import { useParams } from "react-router-dom";
import { ContainerTable } from "./List";
import { AuditTabDefinition } from "../../Common/CRUD/Tabs/AuditsTab/AuditTabDefinition";

export const ContainerForm = [
    { key: "name", component: Text, label: "Name" },
    {
        key: "type", 
        component: Dropdown, 
        label: "Type",
        options: [
            { label: "Freezer", value: "freezer" },
            { label: "Fridge", value: "fridge" },
            { label: "Cupboard", value: "cupboard" },
            { label: "Storage Area", value: "storage" },
            { label: "Box", value: "box" },
        ]
    },
    { 
        key: "container_id", 
        component: Relation, 
        label: "Contained within", 
        pickerTitle: "Choose a location", 
        relationPath: "/container", 
        relationDisplayProperty: "name",
        columns: ContainerTable
    },
    { key: "description", component: TextArea, label: "Description" }
];

export default function View() {
    const { id } = useParams();

    return (
        <ViewPage
            resource={`/container/${id}`}
            resourceName="Place"
            tabs={[
                {
                    type: "form",
                    label: "Edit",
                    icon: <Edit fontSize="small" />,
                    fields: ContainerForm
                },
                {
                    type: "relation",
                    key: "children",
                    label: "Sub-places",
                    resource: "/container",
                    columns: [
                        { field: 'name', headerName: 'Name', flex: 5, sortable: false },
                    ]
                },
                {
                    type: "relation",
                    key: "assets",
                    label: "Assets",
                    resource: "/asset",
                    columns: [
                        { field: 'tag', headerName: 'Tag', flex: 5, sortable: false }
                    ]
                },
                AuditTabDefinition
            ]}
        />
    );
}