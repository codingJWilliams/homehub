import { ArrowDownward, ChevronLeft, ChevronRight, ExpandLess, ExpandMore, PhoneAndroid, Place, Smartphone } from "@mui/icons-material";
import { Badge, Box, Button, Chip, Typography, alpha, darken, getOverlayAlpha, lighten } from "@mui/material";
import { useState } from "react";
import { useApi } from "../../Helpers/api";
import FullwidthCircular from "../../Components/FullwidthCircular";
import { theme } from "../../Common/theme";
import { Link } from "react-router-dom";

function TreeContainerRow({ container, depth = 0 }) {
    const [open, setOpen] = useState(false);
    const { data, isLoading, error, refresh } = useApi(`/container/tree/?parent=${container.id}`, {}, open);

    let moreDetails;
    if (open && isLoading) {
        moreDetails = <FullwidthCircular />
    } else if (open && !isLoading) {
        moreDetails = <RenderTree depth={depth + 1} data={data} />
    }

    return (
        <>
            <Box sx={{ backgroundColor: alpha('#fff', getOverlayAlpha(0.5 * depth)), p: 0.7, border: `1px solid ${lighten(theme.palette.background.paper, 0.08)}`, borderBottom: 'none' }}>
                <Box display="flex" alignItems="center" onClick={() => setOpen(o => !o)} >
                    {open ? <ExpandLess /> : <ExpandMore />}
                    <Box display="flex" alignItems="center" gap="10px" width="100%">
                        {container.name}
                        {container.child_assets ? <Chip size="small" label={container.child_assets} icon={<Smartphone />} /> : null}
                        {container.child_containers ? <Chip size="small" label={container.child_containers} icon={<Place />} /> : null}
                        <Box flexGrow={1}></Box>
                        <Button size="small">Open</Button>
                    </Box>

                </Box>
                {moreDetails}
            </Box>
        </>
    );
}

function AssetRow({ asset, depth }) {
    return (
        <Box sx={{ backgroundColor: alpha('#fff', getOverlayAlpha(0.5 * depth)), p: 1, border: `1px solid ${lighten(theme.palette.background.paper, 0.08)}`, borderBottom: 'none' }} display="flex" alignItems="center" gap="5px">
            <Smartphone />
            <Link className="nostyle" to={`/asset/${asset.id}`}>
                {asset.model.display_name}{asset.tag ? ` (${asset.tag})` : ``}
            </Link>
        </Box>
    )
}

export default function RenderTree({ data, depth = 0 }) {
    return <Box sx={{ ml: depth ? 0.7 : 0 }}>
        <Box sx={{ pl: depth ? 1.5 : 0, borderLeft: depth ? `1px solid ${theme.palette.primary.main}` : '' }}>
            {/* <Typography variant="h6">Places</Typography> */}
            <Box sx={{ borderBottom: (data.containers.length && !data.assets.length) ? `1px solid ${lighten(theme.palette.background.paper, 0.08)}` : '' }}>
                {data.containers.map(ct => <TreeContainerRow depth={depth} key={ct.id} container={ct} />)}
            </Box>

            {/* <Typography variant="h6">Assets</Typography> */}
            <Box sx={{ borderBottom: data.assets.length ? `1px solid ${lighten(theme.palette.background.paper, 0.08)}` : '' }}>
                {data.assets.map(asset => <AssetRow depth={depth} key={asset.id} asset={asset} />)}
            </Box>
        </Box>
    </Box>;
}