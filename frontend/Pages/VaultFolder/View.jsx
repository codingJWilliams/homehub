import Text from "../../Common/CRUD/Fields/Text";
import Relation from "../../Common/CRUD/Fields/Relation";
import ViewPage from "../../Common/CRUD/Pages/ViewPage";
import { Edit } from "@mui/icons-material";
import { useParams } from "react-router-dom";
import { AuditTabDefinition } from "../../Common/CRUD/Tabs/AuditsTab/AuditTabDefinition";
import { VaultFolderTable } from "./List";
import { VaultTable } from "../Vault/List";

export const VaultFolderForm = [
    { key: "name", component: Text, label: "Name" },
    { key: "size_kb", component: Text, label: "Size" },
    { 
        key: "parent_folder_id", 
        component: Relation, 
        label: "Parent ID",
        pickerTitle: "Choose a folder",
        columns: VaultFolderTable,
        relationPath: "/vault/folder", 
        relationDisplayProperty: "name",
    },
    { 
        key: "vault_id", 
        component: Relation, 
        columns: VaultTable,
        label: "Vault ID",
        pickerTitle: "Choose a vault", 
        relationPath: "/vault", 
        relationDisplayProperty: "name",
    },
];

export default function View() {
    const { id } = useParams();

    return (
        <ViewPage
            resource={`/vault/folder/${id}`}
            resourceName="Folder"
            tabs={[
                {
                    type: "form",
                    label: "Edit",
                    icon: <Edit fontSize="small" />,
                    fields: VaultFolderForm
                },
                AuditTabDefinition
            ]}
        />
    );
}