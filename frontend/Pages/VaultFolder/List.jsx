import IndexPage from "../../Common/CRUD/Pages/IndexPage";

export const VaultFolderTable = [
    { field: 'name', headerName: 'Name', flex: 2, sortable: false }
];

export default function List() {
    return (
        <IndexPage
            resource={`/vault/folder/`}
            resourceName="Folders"
            componentProps={{
                openLink: row => `/vault/${row.id}`,
                columns: VaultFolderTable
            }}
        />
    );
}