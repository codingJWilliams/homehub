import IndexPage from "../../Common/CRUD/Pages/IndexPage";
import ResultsTable from "../../Common/CRUD/ResultsTable";
import FakeTable from "../../Components/FakeTable";

export default function List({ }) {
    return (
        <IndexPage
            resource={`/asset/role/`}
            resourceName="Asset Roles"
            component={ResultsTable}
            loadingComponent={<FakeTable cols={["Type", "Name"]} rows={10} />}
            componentProps={{
                openLink: row => `/asset/role/${row.id}`,
                columns: [
                    { field: 'name', headerName: 'Name', flex: 5, sortable: false }
                ]
            }}
        />
    );
}