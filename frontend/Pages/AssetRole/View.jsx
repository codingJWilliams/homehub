import { Link, useParams } from "react-router-dom";
import Text from "../../Common/CRUD/Fields/Text";
import ViewPage from "../../Common/CRUD/Pages/ViewPage";
import { Edit } from "@mui/icons-material";
import { AuditTabDefinition } from "../../Common/CRUD/Tabs/AuditsTab/AuditTabDefinition";
import { AssetTable } from "../Asset/List";

export const AssetRoleForm = [
    { key: "name", component: Text, label: "Name" }
];

export default function View() {
    const { id } = useParams();

    return (
        <ViewPage
            resource={`/asset/role/${id}`}
            resourceName="Asset Role"
            resourceNameKey="name"
            tabs={[
                {
                    type: "form",
                    label: "Edit",
                    icon: <Edit fontSize="small" />,
                    fields: AssetRoleForm
                },
                {
                    type: "many_relation",
                    key: "assets",
                    label: "Assets",
                    resource: "/asset",
                    attachResource: "/asset",
                    pickerTitle: "Choose an asset",
                    attachLabel: "Link",
                    detachLabel: "Unlink",
                    columns: AssetTable
                },
                AuditTabDefinition
            ]}
        />
    );
}