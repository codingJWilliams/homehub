import { Avatar, Box, Grid, List, ListItemAvatar, ListItemButton, ListItemText, Paper, Typography } from "@mui/material";
import { useApi } from "../../Helpers/api";
import LoadingWrapper from "../../Components/LoadingWrapper";
import { useNavigate } from "react-router-dom";
import logo from "../../Assets/logo.png";
import { mainList } from "../../Common/Layout/listItems";
import { useState } from "react";

function HomeContent({ modelCounts }) {
    const navigate = useNavigate();

    return (
        <>
            <Box textAlign="center">
                <img src={logo} alt="HomeHub Logo" style={{ maxWidth: "100%" }} />
            </Box>
            <Grid container gap="20px" sx={{ mt: 2 }}>
                {Object.keys(mainList).map(groupName =>
                    <Grid key={groupName} item flexGrow={1}>
                        <Paper elevation={1}>
                            <Box sx={{ p: 1, py: 0 }}>
                                <Typography variant="overline" sx={{ lineHeight: 1 }}>{groupName}</Typography>
                            </Box>
                            <nav aria-label="main mailbox folders">
                                <List sx={{ width: '100%' }}>
                                    {mainList[groupName].filter(c => !c.dashboardHidden).filter(c => modelCounts[c.model] !== undefined).map(c =>
                                        <ListItemButton key={c.page} onClick={() => navigate(c.url)}>
                                            <ListItemAvatar>
                                                <Avatar sx={{ bgcolor: "primary.main" }}>
                                                    <c.icon htmlColor="white" />
                                                </Avatar>
                                            </ListItemAvatar> 
                                            <ListItemText primary={c.page} secondary={`${modelCounts[c.model]} item${modelCounts[c.model] !== 1 ? "s" : ""}`} />
                                        </ListItemButton>
                                    )}
                                </List>
                            </nav>
                        </Paper>
                    </Grid>
                )}
            </Grid>
        </>
    );
}

export default function Home() {
    const { data, isLoading } = useApi(`/summary`);

    return (
        <LoadingWrapper isLoading={isLoading}>
            <HomeContent modelCounts={data} />
        </LoadingWrapper>
    );
}