import Text from "../../Common/CRUD/Fields/Text";
import ViewPage from "../../Common/CRUD/Pages/ViewPage";
import { Details } from "@mui/icons-material";
import { useParams } from "react-router-dom";
import { AuditTabDefinition } from "../../Common/CRUD/Tabs/AuditsTab/AuditTabDefinition";

export const PermissionForm = [
    { key: "action", component: Text, label: "Action" },
    { key: "resource", component: Text, label: "Resource" }
];

export default function View() {
    const { id } = useParams();

    return (
        <ViewPage
            resource={`/user/permission/${id}`}
            resourceName="Permission"
            readonly
            tabs={[
                {
                    type: "form",
                    label: "Details",
                    // icon: <Details fontSize="small" />,
                    fields: PermissionForm
                },
                AuditTabDefinition
            ]}
        />
    );
}