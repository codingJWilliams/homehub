import IndexPage from "../../Common/CRUD/Pages/IndexPage";
import ColorChip from "../../Components/ColorChip";
import ucfirst from "../../Helpers/ucfirst";

export const PermissionTable = [
    { field: 'action', headerName: 'Action', flex: 2, sortable: false, renderCell: params => <ColorChip label={ucfirst(params.row?.action)} /> },
    { field: 'resource', headerName: 'Resource', flex: 2, sortable: false },
];

export default function List() {
    return (
        <IndexPage
            resource={`/user/permission/`}
            resourceName="Permission"
            componentProps={{
                openLink: row => `/user/permission/${row.id}`,
                columns: PermissionTable
            }}
        />
    );
}