import { Box, Grid, Paper, Table, TableBody, TableCell, TableContainer, TableRow, Typography } from "@mui/material";
import { PaperCard, PaperCardBody, PaperCardHeader } from "../../Components/PaperCard";
import InfoTable from "../../Components/InfoTable";
import { mainList } from "../../Common/Layout/listItems";
import { Link } from "react-router-dom";
import ucfirst from "../../Helpers/ucfirst";
import ColorChip from "../../Components/ColorChip";

export function getRelatedLink(model, id) {
    let found;
    for (const key of Object.keys(mainList)) {
        for (const item of mainList[key]) {
            if (item.model === model) found = item;
        }
    }
    if (!found) return null;

    const link = `${found.url}/${id}`;
    return link.startsWith("//") ? link.substring(1) : link;
}

export default function AuditDetails({ data }) {
    const model = data.auditable_type.split("\\").splice(-1).join("\\");
    const link = getRelatedLink(model, data.auditable_id);
    const objectLink = link ? <Link to={link}>{model}({data.auditable_id})</Link> : <>{model}({data.auditable_id})</>;

    return (
        <Grid container spacing={2} sx={{ mt: -2 }}>
            <Grid item sm={12} flexGrow={1} sx={{ maxWidth: "100%" }}>
                <PaperCard>
                    <PaperCardHeader>
                        <Typography variant="h6">Details</Typography>
                    </PaperCardHeader>
                    <PaperCardBody>
                        <InfoTable data={{
                            Time: data.created_at,
                            "Object": objectLink,
                            "Type": <ColorChip label={ucfirst(data.event)} />,
                            "User IP Address": data.ip_address,
                            "User": data.user.email
                        }} />
                    </PaperCardBody>
                </PaperCard>
            </Grid>
            <Grid item sm={6} flexGrow={1}>
                <PaperCard>
                    <PaperCardHeader>
                        <Typography variant="h6">Old values</Typography>
                    </PaperCardHeader>
                    <PaperCardBody>
                        <InfoTable data={Array.isArray(data.old_values) ? {} : data.old_values} />
                    </PaperCardBody>
                </PaperCard>
            </Grid>
            <Grid item sm={6} flexGrow={1}>
                <PaperCard>
                    <PaperCardHeader>
                        <Typography variant="h6">New values</Typography>
                    </PaperCardHeader>
                    <PaperCardBody>
                        <InfoTable data={Array.isArray(data.new_values) ? {} : data.new_values} />
                    </PaperCardBody>
                </PaperCard>
            </Grid>
        </Grid>
    )
}