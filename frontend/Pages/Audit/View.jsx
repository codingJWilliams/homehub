import TextArea from "../../Common/CRUD/Fields/TextArea";
import ViewPage from "../../Common/CRUD/Pages/ViewPage";
import { Details, Info } from "@mui/icons-material";
import { useParams } from "react-router-dom";
import AuditDetails from "./AuditDetails";

export default function View() {
    const { id } = useParams();

    return (
        <ViewPage
            resource={`/audit/${id}`}
            resourceName="Audit"
            resourceNameKey="id"
            readonly
            tabs={[
                {
                    type: "custom",
                    label: "Details",
                    component: AuditDetails
                    // icon: <Info />
                }
            ]}
        />
    );
}