import IndexPage from "../../Common/CRUD/Pages/IndexPage";
import ResultsTable from "../../Common/CRUD/ResultsTable";
import formatDate from "../../Helpers/formatDate";
import ucfirst from "../../Helpers/ucfirst";
import ColorChip from "../../Components/ColorChip";
import { Link } from "react-router-dom";
import { getRelatedLink } from "./AuditDetails";
import UserAvatar from "../../Components/UserAvatar";

export const AuditLogTable = [
    { field: 'created_at', headerName: 'Date', flex: 3, sortable: false, valueGetter: params => formatDate(params.row?.created_at) },
    { field: 'user', headerName: 'User', flex: 3, sortable: false, renderCell: params => <UserAvatar user={params?.row?.user} />},
    { field: 'event', headerName: 'Action', flex: 3, sortable: false, renderCell: params => <ColorChip label={ucfirst(params.row?.event)} />},
    { 
        field: 'object', 
        headerName: 'Object', 
        flex: 3, 
        sortable: false,
        renderCell: params => {
            const clazz = params.row?.auditable_type.split("\\").splice(-1).join("\\");
            const content = <>{clazz} - {params.row?.auditable_id}</>;
            const link = getRelatedLink(clazz, params.row?.auditable_id);
            return link ? <Link to={link}>{content}</Link> : content;
        }
    }
];

export default function List() {
    return (
        <IndexPage
            resource={`/audit/`}
            resourceName="Audit Log"
            component={ResultsTable}
            noSearch
            componentProps={{
                openLink: row => `/audit/${row.id}`,
                columns: AuditLogTable
            }}
        />
    );
}