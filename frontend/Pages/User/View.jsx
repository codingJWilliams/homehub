import ViewPage from "../../Common/CRUD/Pages/ViewPage";
import { Link, useParams } from "react-router-dom";
import UserDetails from "./UserDetails";
import { RoleTable } from "../UserRole/List";
import { AuditTabDefinition } from "../../Common/CRUD/Tabs/AuditsTab/AuditTabDefinition";

export default function View() {
    const { id } = useParams();

    return (
        <ViewPage
            resource={`/user/${id}`}
            resourceName="User"
            resourceNameKey="name"
            readonly
            tabs={[
                {
                    type: "custom",
                    label: "Details",
                    component: UserDetails
                    // icon: <Info />
                },
                {
                    type: "many_relation",
                    key: "roles",
                    label: "Roles",
                    resource: "/user/role",
                    attachResource: "/user/role",
                    pickerTitle: "Choose a role",
                    attachLabel: "Assign",
                    detachLabel: "Unassign",
                    columns: RoleTable
                },
                AuditTabDefinition
            ]}
        />
    );
}