import IndexPage from "../../Common/CRUD/Pages/IndexPage";
import ResultsTable from "../../Common/CRUD/ResultsTable";

export const UserTable = [
    { field: 'name', headerName: 'Name', flex: 3, sortable: false },
    { field: 'email', headerName: 'Email', flex: 3, sortable: false },
];

export default function List() {
    return (
        <IndexPage
            resource={`/user/`}
            resourceName="Users"
            component={ResultsTable}
            noSearch
            componentProps={{
                openLink: row => `/user/${row.id}`,
                columns: UserTable
            }}
        />
    );
}