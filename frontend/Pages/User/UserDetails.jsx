import { Grid, Typography } from "@mui/material";
import { PaperCard, PaperCardBody, PaperCardHeader } from "../../Components/PaperCard";
import InfoTable from "../../Components/InfoTable";

export default function UserDetails({ data }) {
    return (
        <Grid container spacing={2} sx={{ mt: -2 }}>
            <Grid item sm={12} flexGrow={1} sx={{ maxWidth: "100%" }}>
                <PaperCard>
                    <PaperCardHeader>
                        <Typography variant="h6">Details</Typography>
                    </PaperCardHeader>
                    <PaperCardBody>
                        <InfoTable data={{
                            "Created At": data.created_at,
                            "Id": data.id,
                            "Name": data.name,
                            "Email": data.email
                        }} />
                    </PaperCardBody>
                </PaperCard>
            </Grid>
        </Grid>
    );
}