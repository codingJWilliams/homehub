import IndexPage from "../../Common/CRUD/Pages/IndexPage";
import ResultsTable from "../../Common/CRUD/ResultsTable";
import FakeTable from "../../Components/FakeTable";

export const AssetTable = [
    { field: 'tag', headerName: 'Tag', flex: 4, sortable: false },
    { field: 'model_id', headerName: 'Model', flex: 5, sortable: false, valueGetter: params => params.row?.model?.display_name }    
];

export default function List() {
    return (
        <IndexPage
            resource={`/asset/`}
            resourceName="Asset"
            component={ResultsTable}
            loadingComponent={<FakeTable cols={["Type", "Name"]} rows={10} />}
            componentProps={{
                openLink: row => `/asset/${row.id}`,
                columns: AssetTable
            }}
        />
    );
}