import Text from "../../Common/CRUD/Fields/Text";
import TextArea from "../../Common/CRUD/Fields/TextArea";
import ViewPage from "../../Common/CRUD/Pages/ViewPage";
import Relation from "../../Common/CRUD/Fields/Relation";
import Date from "../../Common/CRUD/Fields/Date";
import { Edit } from "@mui/icons-material";
import { AssetModelForm } from "../AssetModel/View";
import { ContainerForm } from "../Container/View";
import { AuditTabDefinition } from "../../Common/CRUD/Tabs/AuditsTab/AuditTabDefinition";
import { Link, useParams } from "react-router-dom";
import Barcode from "../../Common/CRUD/Fields/Barcode";
import { AssetModelTable } from "../AssetModel/List";
import { ContainerTable } from "../Container/List";
import Number from "../../Common/CRUD/Fields/Number";
import { AssetTable } from "./List";
import Dropdown from "../../Common/CRUD/Fields/Dropdown";

const AssetForm = [
    { key: "tag", component: Barcode, label: "Tag" },
    { 
        key: "model_id", 
        component: Relation, 
        label: "Model", 
        pickerTitle: "Choose a model", 
        relationPath: "/asset/model", 
        relationDisplayProperty: "name", 
        creationForm: AssetModelForm,
        columns: AssetModelTable
    },
    { 
        key: "container_id", 
        component: Relation, 
        label: "Stored in", 
        pickerTitle: "Choose a location", 
        relationPath: "/container", 
        relationDisplayProperty: "name", 
        creationForm: ContainerForm,
        columns: ContainerTable
    },
    {
        key: "status", 
        component: Dropdown, 
        label: "Status",
        options: [
            { label: "Active", value: "active" },
            { label: "Disposed of", value: "discarded" },
            { label: "Sold", value: "sold" }
        ]
    },
    { key: "serial_number", component: Text, label: "Serial Number" },
    { key: "purchased_at", component: Date, label: "Purchased at" },
    { key: "purchase_value", component: Number, currency: true, label: "Purchase Value" },
    { key: "notes", component: TextArea, label: "Notes" },
];

export default function View() {
    const { id } = useParams();

    return (
        <ViewPage
            resource={`/asset/${id}`}
            resourceName="Asset"
            resourceNameKey="tag"
            tabs={[
                {
                    type: "form",
                    label: "Edit",
                    icon: <Edit fontSize="small" />,
                    fields: AssetForm
                },
                {
                    type: "many_relation",
                    key: "roles",
                    label: "Roles",
                    resource: "/asset/role",
                    attachResource: "/asset/role",
                    pickerTitle: "Choose a role",
                    attachLabel: "Link",
                    detachLabel: "Unlink",
                    columns: [
                        { field: 'name', headerName: 'Name', flex: 5, sortable: false, renderCell: params => <Link to={`/asset/role/${params.row?.id}`}>{params.row?.name}</Link> }
                    ]
                },
                {
                    type: "many_relation",
                    key: "linked",
                    label: "Linked assets",
                    resource: "/asset",
                    attachResource: "/asset",
                    pickerTitle: "Choose an asset",
                    attachLabel: "Link",
                    detachLabel: "Unlink",
                    columns: AssetTable
                },
                {
                    type: "relation",
                    key: "sisters",
                    label: "Similar assets",
                    resource: "/asset",
                    columns: AssetTable
                },
                AuditTabDefinition
            ]}
        />
    );
}