import { Box, Typography } from "@mui/material";
import { getResource, updateResource, useApi } from "../../Helpers/api";
import { useRef, useState } from "react";
import { enqueueSnackbar } from "notistack";
import { ValidationError } from "../../Helpers/errors";
import BarcodeScanner from "./BarcodeScanner";
import Relation from "../../Common/CRUD/Fields/Relation";
import { ContainerTable } from "../Container/List";
import { ContainerForm } from "../Container/View";

const update_cache = {};

export default function AssetMassUpdate({ items }) {
    const [container_id, setContainerId] = useState(false);
    const ctRef = useRef();
    ctRef.current = container_id;
    var audio = new Audio('/ping.mp3');
    var error = new Audio('/error.mp3');

    const submitUpdateContainer = async (item) => {
        try {
            await updateResource(`/asset/${item.id}`, {
                ...item,
                container_id: ctRef.current
            });

            enqueueSnackbar(`Updated ${item.tag} successfully`, { "variant": "success", autoHideDuration: 800 });
            audio.currentTime = 0;
            audio.play();

        } catch (err) {
            error.currentTime = 0;
            error.play();

            if (err instanceof ValidationError) {
                enqueueSnackbar(`There were some problems with the form`, { variant: "error" })
            } else {
                enqueueSnackbar(err.message, { variant: "error" });
            }
        }
    }

    return (
        <Box sx={{ display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center", height: "60vh" }}>
            <Typography variant="h4">Mass update assets</Typography>
            <Relation
                field={{
                    key: "container_id",
                    label: "Stored in",
                    pickerTitle: "Choose a location",
                    relationPath: "/container",
                    relationDisplayProperty: "name",
                    creationForm: ContainerForm,
                    columns: ContainerTable
                }}
                data={{
                    container_id
                }}
                updateAttributeCb={(_, d) => {
                    setContainerId(d)
                    console.log(d);
                }}
            />
            <input type="text" onKeyUp={async evt => {
                if (evt.key !== "Enter") return;
                try {
                    const asset = await getResource(`/asset/tag/${evt.target.value}`);
                    await submitUpdateContainer(asset);
                } catch (e) {
                    enqueueSnackbar(e.message, { variant: "error" });
                    error.currentTime = 0;
                    error.play();        
                }
                    evt.target.value = "";
            }} placeholder="Barcode Input" />
        </Box>
    );
}