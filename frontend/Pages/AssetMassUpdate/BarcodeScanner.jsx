import { Add, Remove } from "@mui/icons-material";
import { IconButton, InputAdornment, TextField } from "@mui/material";
import { Html5QrcodeScanType, Html5QrcodeScanner, Html5QrcodeScannerState } from "html5-qrcode";
import { useEffect, useState } from "react";

export default function BarcodeScanner({ onScan }) {
    const [readerId] = useState(`barcode-${Math.floor(Math.random()*10000)}`);
    // const [rendered, setRendered] = useState(false);
    const [scanner, setScanner] = useState(null);



    useEffect(() => {
        let scanner_ = scanner;
        if (!scanner) {
            scanner_ = new Html5QrcodeScanner(readerId, { fps: 10, supportedScanTypes: [Html5QrcodeScanType.SCAN_TYPE_CAMERA], showTorchButtonIfSupported: true  });
            setScanner(scanner_);
        }

        if (document.getElementById(`${readerId}__scan_region`)) return
        var html5QrcodeScanner = scanner; // qrbox: 250
        scanner_.render(onScan);
    }, []);

    return (
        <div id={readerId} style={{ width: "80%", height: "50vh", overflow: "auto" }}>
        </div>
    );
}