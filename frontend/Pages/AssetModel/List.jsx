import IndexPage from "../../Common/CRUD/Pages/IndexPage";
import ResultsTable from "../../Common/CRUD/ResultsTable";
import FakeTable from "../../Components/FakeTable";

export const AssetModelTable = [
    { field: 'name', headerName: 'Name', flex: 5, sortable: false },
    { field: 'manufacturer', headerName: 'Manufacturer', flex: 5, sortable: false },
];

export default function List({ }) {
    return (
        <IndexPage
            resource={`/asset/model/`}
            resourceName="Asset Models"
            component={ResultsTable}
            loadingComponent={<FakeTable cols={["Type", "Name"]} rows={10} />}
            componentProps={{
                openLink: row => `/asset/model/${row.id}`,
                columns: AssetModelTable
            }}
        />
    );
}