import { useParams } from "react-router-dom";
import Text from "../../Common/CRUD/Fields/Text";
import TextArea from "../../Common/CRUD/Fields/TextArea";
import ViewPage from "../../Common/CRUD/Pages/ViewPage";
import { Edit } from "@mui/icons-material";
import { AuditTabDefinition } from "../../Common/CRUD/Tabs/AuditsTab/AuditTabDefinition";
import { AssetTable } from "../Asset/List";

export const AssetModelForm = [
    { key: "name", component: Text, label: "Name" },
    { key: "manufacturer", component: Text, label: "Manufacturer" },
    { key: "link", component: Text, label: "Link" },
    { key: "notes", component: TextArea, label: "Notes" },
];

export default function View() {
    const { id } = useParams();

    return (
        <ViewPage
            resource={`/asset/model/${id}`}
            resourceName="Asset Model"
            resourceNameKey="name"
            tabs={[
                {
                    type: "form",
                    label: "Edit",
                    icon: <Edit fontSize="small" />,
                    fields: AssetModelForm
                },
                {
                    type: "relation",
                    key: "assets",
                    label: "Assets",
                    resource: "/asset",
                    columns: AssetTable
                },
                AuditTabDefinition
            ]}
        />
    );
}