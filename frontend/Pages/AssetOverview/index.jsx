import { Box, Grid, Typography } from "@mui/material";
// import { useApi } from "../../Helpers/api";
// import LoadingWrapper from "../../Components/LoadingWrapper";
// import { PaperCard, PaperCardBody, PaperCardHeader } from "../../Components/PaperCard";
// import ResultsTable from "../../Common/CRUD/ResultsTable";
// import { DatePicker } from "@mui/x-date-pickers";
// import { useState } from "react";
// import dayjs from "dayjs";
import DateApiWrapper from "../../Components/DateApiWrapper";
import { PaperCard, PaperCardBody, PaperCardHeader } from "../../Components/PaperCard";
import { Chart } from "react-chartjs-2";


// const soldItemsColumns = [
//     { field: 'name', headerName: 'Name', flex: 5, sortable: false },
//     { field: 'sold', headerName: 'Number sold', flex: 3, sortable: false },
//     { field: 'revenue', headerName: 'Revenue generated', flex: 3, sortable: false, valueGetter: params => currency(parseFloat(params.row.sale_price) * params.row.sold) }
// ];

// const stockItemsColumns = [
//     { field: 'name', headerName: 'Name', flex: 5, sortable: false },
//     { field: 'stock_count', headerName: 'Tuck Stock', flex: 3, sortable: false },
//     { field: 'stored_stock', headerName: 'Home Stock', flex: 3, sortable: false },
//     { field: 'total_stock', headerName: 'Total Stock', flex: 4, sortable: false },
//     { field: 'sold_during_period', headerName: 'Amount sold', flex: 3, sortable: false }
// ];

function AssetOverviewContent({ data }) {
    console.log(data);
    return (
        <>
            {/* <Grid container gap="20px" sx={{ mt: 2 }}> */}
            <Typography variant="h5">Asset purchase value</Typography>
            <PaperCard sx={{ my: 1, width: "100%" }}>
                <PaperCardHeader>
                </PaperCardHeader>
                <PaperCardBody>
                {data.time_series.map(t => t.purchase_value).join(",")}
                    {/* <Chart data= /> */}
                </PaperCardBody>
            </PaperCard>
            {/* <PaperCard sx={{ my: 1, width: "100%" }}>
                <PaperCardHeader>
                </PaperCardHeader>
                <PaperCardBody>
                    <Grid container alignItems="stretch" justifyContent="space-evenly" flexWrap="wrap" gap="25px">
                        <Grid item textAlign="center" flexGrow={1}>
                            <Typography variant="h4">{currency(data.profits.total_value)}</Typography>
                            <Typography variant="body2">total tuckshop asset value</Typography>
                        </Grid>
                        <Grid item textAlign="center" flexGrow={1}>
                            <Typography variant="h4">{currency(data.profits.liquid_value)}</Typography>
                            <Typography variant="body2">total tuckshop liquid value</Typography>
                        </Grid>
                        <Grid item textAlign="center" flexGrow={1}>
                            <Typography variant="h4">{currency(data.profits.actual_revenue)}</Typography>
                            <Typography variant="body2">actual revenue</Typography>
                        </Grid>
                    </Grid>
                </PaperCardBody>
            </PaperCard>
            <Typography variant="h5" sx={{ mt: 4 }}>Sales overview</Typography>
            <PaperCard sx={{ my: 1, width: "100%" }}>
                <PaperCardHeader>
                </PaperCardHeader>
                <PaperCardBody>
                    <Grid container alignItems="stretch" justifyContent="space-evenly" flexWrap="wrap" gap="25px">
                        <Grid item textAlign="center" flexGrow={1}>
                            <Typography variant="h4">{currency(data.profits.expected_revenue_min)}</Typography>
                            <Typography variant="body2">expected min revenue</Typography>
                        </Grid>
                        <Grid item textAlign="center" flexGrow={1}>
                            <Typography variant="h4">{currency(data.profits.expected_revenue_max)}</Typography>
                            <Typography variant="body2">expected max revenue</Typography>
                        </Grid>
                        <Grid item textAlign="center" flexGrow={1}>
                            <Typography variant="h4">{currency(data.profits.items_cost)}</Typography>
                            <Typography variant="body2">cost of stock</Typography>
                        </Grid>
                        <Grid item textAlign="center" flexGrow={1}>
                            <Typography variant="h4">{currency(data.profits.charity_profit)}</Typography>
                            <Typography variant="body2">estimated profit</Typography>
                        </Grid>
                    </Grid>
                </PaperCardBody>
            </PaperCard> */}

            {/* </Grid> */}
        </>
    );
}

export default function AssetOverview() {
    return (
        <DateApiWrapper component={AssetOverviewContent} pathFn={({ from, to }) => `/asset/overview/?from=${from}&to=${to}`} />
    );
}