import Text from "../../Common/CRUD/Fields/Text";
import ViewPage from "../../Common/CRUD/Pages/ViewPage";
import TextArea from "../../Common/CRUD/Fields/TextArea";
import { Edit } from "@mui/icons-material";
import { useParams } from "react-router-dom";
import { UserTable } from "../User/List";
import { PermissionTable } from "../UserPermission/List";
import { AuditTabDefinition } from "../../Common/CRUD/Tabs/AuditsTab/AuditTabDefinition";

export const RoleForm = [
    { key: "name", component: Text, label: "Name" },
    { key: "description", component: TextArea, label: "Description" },
];

export default function View() {
    const { id } = useParams();

    return (
        <ViewPage
            resource={`/user/role/${id}`}
            resourceName="Role"
            tabs={[
                {
                    type: "form",
                    label: "Edit",
                    icon: <Edit fontSize="small" />,
                    fields: RoleForm
                },
                {
                    type: "many_relation",
                    key: "users",
                    label: "Users",
                    resource: "/user",
                    attachResource: "/user",
                    pickerTitle: "Choose a user",
                    attachLabel: "Assign",
                    detachLabel: "Unassign",
                    columns: UserTable
                },
                {
                    type: "many_relation",
                    key: "permissions",
                    label: "Permissions",
                    resource: "/user/permission",
                    attachResource: "/user/permission",
                    pickerTitle: "Choose a permission",
                    attachLabel: "Assign",
                    detachLabel: "Unassign",
                    columns: PermissionTable
                },
                AuditTabDefinition
            ]}
        />
    );
}