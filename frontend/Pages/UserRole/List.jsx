import IndexPage from "../../Common/CRUD/Pages/IndexPage";

export const RoleTable = [
    { field: 'name', headerName: 'Name', flex: 2, sortable: false }
];

export default function List() {
    return (
        <IndexPage
            resource={`/user/role/`}
            resourceName="Roles"
            componentProps={{
                openLink: row => `/user/role/${row.id}`,
                columns: RoleTable
            }}
        />
    );
}