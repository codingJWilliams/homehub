export class DetailedError extends Error {
    constructor(title, content, details) {
        super(content);
        this.title = title;
        this.details = details;
    }
}

export class ValidationError extends DetailedError {
    constructor(problems) {
        super("There was a problem processing your request", "The provided parameters were not valid", problems);
        this.problems = problems;
    }
}

/**
 * Checks and throws errors for error response codes
 * @param {Response} res The response returned from fetch()
 * @param {object} body The decoded JSON body
 */
export async function checkError(res, body) {
    if (res.status === 400 && body.errors) {
        throw new ValidationError(body.errors);
    }
    if (res.status === 500) {
        throw new DetailedError(
            "There was a problem processing your request",
            "The server threw an error.",
            {
                Path: res.url,
                Code: 500,
                Message: JSON.stringify(body).substring(0, 500) + (JSON.stringify(body).length > 500 ? "..." : "")
            }
        );
    }
    if (res.status === 404) {
        throw new DetailedError(
            "Resource not found",
            "The requested item does not exist, or could not be found. Please check it has not been deleted."
        );
    }

    if (res.status === 403) {
        throw new DetailedError(
            "Unauthorized request",
            body.message ?? "You do not have the required permission to perform that action."
        );
    }

    if (res.status === 401) {
        setTimeout(() => {
            localStorage.setItem("token", "{}");
            window.location.reload();
        }, 500);

        throw new DetailedError(
            "Unauthorized request",
            "You are not authenticated. Reloading the application now to fix this."
        );
    }

    if (res.status !== 200) {
        throw new DetailedError(
            "There was a problem processing your request",
            `The server responded with code ${res.status}`,
            body
        );
    }
}