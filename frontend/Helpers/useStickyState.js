import { useEffect, useState } from "react";

export default function useStickyState(key = "sticky", initialState = null) {
  const [state, setState] = useState(() => {
    const store = localStorage.getItem(key);
    try {
      const res = JSON.parse(store);

      if (res === null || res === undefined) return initialState;
      return res.state;
    } catch (e) {
      return null;
    }
  });

  useEffect(() => {
  }, [state]);

  const clearState = () => localStorage.removeItem(key);

  return [
    state,
    (newValue) => {
      localStorage.setItem(key, JSON.stringify({ state: newValue }));
      setState(newValue)
    },
    clearState];
};
