import * as c from "./key-derivation";
import { updateResource, uploadFile, getFileChunk } from "../api";
import { enqueueSnackbar } from "notistack";

export async function handleChunk(key: CryptoKey, chunk: ArrayBuffer) {
    return new Blob([await c.aesEncrypt(chunk, key)]);
}

export async function generateFileMetadata(fileKey: CryptoKey, vaultKey: CryptoKey) {
    // const fileKey = c.generateRandomAesKey();

    const fileKeyBuffer = await crypto.subtle.exportKey("raw", fileKey);
    const encryptedFileKey = await c.aesEncrypt(fileKeyBuffer, vaultKey);

    return {
        encryptedFileKey
    };
}

interface FileWriter {
    prepare(): Promise<void>;
    write(startIndex: number, data: ArrayBuffer): Promise<void>;
    finish(returnBlob?: boolean): Promise<void | Blob>;
}

class SaveFileWriter implements FileWriter {
    suggestedFilename: string;
    stream?: FileSystemWritableFileStream;

    constructor(suggestedFilename) {
        this.suggestedFilename = suggestedFilename;
    }

    async prepare() {
        try {
            const rawHandle: FileSystemFileHandle = await (<any>window).showSaveFilePicker({
                suggestedName: this.suggestedFilename
            });
            this.stream = await rawHandle.createWritable();
        } catch (e) {
            throw new Error("Unable to write to file on disk");
            // statusUpdateCb("Unable to write to file", 0);
            // enqueueSnackbar("Unable to write to file on disk", { variant: "error" });
            // return;
        }
    }

    async write(startIndex: number, data: ArrayBuffer) {
        await this.stream.seek(startIndex);
        await this.stream.write(data);
    }

    async finish(returnBlob?: boolean) {
        await this.stream.close();
    }
}

class DownloadFileWriter implements FileWriter {
    suggestedFilename: string;
    bufferParts?: ArrayBuffer[] = [];

    constructor(suggestedFilename) {
        this.suggestedFilename = suggestedFilename;
    }

    async prepare(): Promise<void> {
        
    }

    async write(startIndex: number, data: ArrayBuffer): Promise<void> {
        this.bufferParts.push(data);
    }

    async finish(returnBlob?: boolean): Promise<void | Blob> {
        const blobOptions = {
            type: "application/octet-stream"
        };
        if (this.suggestedFilename.endsWith(".pdf")) blobOptions.type = "application/pdf";
        if (this.suggestedFilename.endsWith(".png")) blobOptions.type = "image/png";
        if (this.suggestedFilename.endsWith(".jpg")) blobOptions.type = "image/jpeg";
        if (this.suggestedFilename.endsWith(".jpeg")) blobOptions.type = "image/jpeg";
        const blob = new Blob(this.bufferParts, blobOptions);
        if (returnBlob) return blob;
        
        saveBlob(this.suggestedFilename, blob);
    }
}

export function saveBlob(fileName: string, blob: Blob) {
    const url = URL.createObjectURL(blob);
    const anchor = document.createElement("a");
    anchor.href = url;
    anchor.download = fileName;
    anchor.click();
}

export async function processFileDownload(file: { id: number, filename: string, num_chunks: number, file_key: string }, vaultKey: CryptoKey, statusUpdateCb: Function, returnBlob?: boolean) {
    const supportsFileStream = 'showOpenFilePicker' in window;

    statusUpdateCb("Setting up destination file", 0);
    let writer: FileWriter;
    if (supportsFileStream && !returnBlob) {
        writer = new SaveFileWriter(file.filename);
    } else {
        writer = new DownloadFileWriter(file.filename);
    }
    await writer.prepare();

    statusUpdateCb("Preparing decryption keys", 0);
    const fileKey = await c.bufferToAesKey(await c.aesDecrypt(c.hexToBuffer(file.file_key), vaultKey));

    for (let chunkIndex = 0; chunkIndex < file.num_chunks; chunkIndex++) {
        try {
            statusUpdateCb(`Downloading chunk ${chunkIndex + 1}/${file.num_chunks}`, (chunkIndex / file.num_chunks) * 100);
            const chunkData = await getFileChunk(`/vault/file/${file.id}/chunk/${chunkIndex}`);
            statusUpdateCb(`Decrypting chunk ${chunkIndex + 1}/${file.num_chunks}`, (chunkIndex / file.num_chunks) * 100);
            const decryptedChunkData = await c.aesDecrypt(chunkData, fileKey);
            statusUpdateCb(`Writing chunk ${chunkIndex + 1}/${file.num_chunks}`, (chunkIndex / file.num_chunks) * 100);
            await writer.write(chunkIndex * 1024 * 1024 * 2, decryptedChunkData);
        } catch (e) {
            statusUpdateCb(`Failed at chunk ${chunkIndex + 1}`, 0);
            enqueueSnackbar(`Failed at chunk ${chunkIndex + 1}`, { variant: "error" });
            return;
        }
    }

    return await writer.finish(returnBlob);
}

export async function processFile(
    file: File,
    fileData: object,
    vaultKey: CryptoKey,
    statusUpdateCb: Function
) {
    statusUpdateCb("Generating encryption keys", 0);

    const fileKey = await c.generateRandomAesKey();
    const { encryptedFileKey } = await generateFileMetadata(fileKey, vaultKey);

    statusUpdateCb("Updating metadata on server", 0);

    const chunkSize = 1024 * 1024 * 2; // 2MB
    const numberOfChunks = Math.ceil(file.size / chunkSize);

    console.log(`Uploading file in ${numberOfChunks} chunks`)

    const modifiedDate = new Date();
    modifiedDate.setTime(file.lastModified);

    const fileModelResponse = await updateResource(`/vault/file/create`, {
        ...fileData,
        size_kb: Math.ceil(file.size / 1024),
        num_chunks: numberOfChunks,
        filename: await c.aesEncryptText(file.name, vaultKey),
        last_modified_at: modifiedDate.toISOString(),
        file_key: c.bufferToHex(encryptedFileKey)
    });

    statusUpdateCb("Uploading data", 0);
    let chunksCompleted = 0;
    for (let startByte = 0; startByte < file.size; startByte += chunkSize) {
        const chunk = file.slice(startByte, startByte + chunkSize);

        statusUpdateCb(`Encrypting chunk ${chunksCompleted + 1}/${numberOfChunks}`, ((chunksCompleted / numberOfChunks) * 100).toFixed(2));
        const encryptedChunk = await handleChunk(fileKey, await chunk.arrayBuffer());

        statusUpdateCb(`Uploading chunk ${chunksCompleted + 1}/${numberOfChunks}`, ((chunksCompleted / numberOfChunks) * 100).toFixed(2));
        await uploadFile(`/vault/file/${fileModelResponse.id}/chunk/${chunksCompleted}`, encryptedChunk);

        chunksCompleted++;
    }

    statusUpdateCb("Upload complete", 100);
}