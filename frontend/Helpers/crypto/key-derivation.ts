export async function getKeysFromPassword(password, salt) {
    const basePassword = await convertToKey(password);

    const masterKey = await pbkdf2(basePassword, salt, 256, 200000);
    const masterPasswordHash = await pbkdf2(await bufferToPbkdf2Key(masterKey), salt + "server hash", 256, 5000);

    return {
        masterKey,
        masterPasswordHash
    };
}

export async function generateRandomAesKey() {
    let key = new Uint8Array(32);
    crypto.getRandomValues(key);
    return bufferToAesKey(key);
}

export async function convertToKey(password) {
    let textEncoder = new TextEncoder();
    return await bufferToPbkdf2Key(textEncoder.encode(password));
}

export async function bufferToPbkdf2Key(buf) {
    return await window.crypto.subtle.importKey(
        'raw',
        buf,
        'PBKDF2',
        false,
        ['deriveBits', 'deriveKey']
    );
}

export async function bufferToAesKey(buf: BufferSource) {
    return await window.crypto.subtle.importKey(
        'raw',
        buf,
        { name: 'AES-CBC' },
        true,
        ['encrypt', 'decrypt']
    );
}

export async function checkMasterPassword(password, salt, masterPasswordHash) {
    try {
        const keys = await getKeysFromPassword(password, salt);
        console.log("Comparing ",bufferToHex(keys.masterPasswordHash), masterPasswordHash);
        return bufferToHex(keys.masterPasswordHash) === masterPasswordHash;
    } catch (e) {
        console.error(e);
        return false;
    }
}

export async function pbkdf2(keyMaterial, salt, bits = 256, iterations = 100000) {
    let textEncoder = new TextEncoder();
    const saltBuffer = textEncoder.encode(salt);
    const params = { name: "PBKDF2", hash: 'SHA-256', salt: saltBuffer, iterations };
    const derivation = await crypto.subtle.deriveBits(params, keyMaterial, bits);
    return derivation;
}


async function generateIv() {
    let initializationVector = new Uint8Array(16);
    crypto.getRandomValues(initializationVector);
    return initializationVector;
}

export function attachIv(initializationVector, dataBuffer) {
    let combinedBuffer = new Uint8Array(dataBuffer.byteLength + 16);
    let dataArr = new Uint8Array(dataBuffer);

    Array.prototype.forEach.call(initializationVector, function (byte, i) {
        combinedBuffer[i] = byte;
    });
    Array.prototype.forEach.call(dataArr, function (byte, i) {
        combinedBuffer[16 + i] = byte;
    });
    return combinedBuffer;
}

export function detachIv(dataBuffer) {
    let dataArr = new Uint8Array(dataBuffer);
    let iv = new Uint8Array(16);
    let data = new Uint8Array(dataArr.length - 16);
    Array.prototype.forEach.call(dataArr, function (byte, i) {
        if (i < 16) {
            iv[i] = byte;
        } else {
            data[i - 16] = byte;
        }
    });
    return { iv, data };
}

export async function aesEncryptText(data, keyBuffer: ArrayBuffer | CryptoKey) : Promise<String> {
    return bufferToHex(await aesEncrypt(stringToBuffer(data), keyBuffer));
}

export async function aesDecryptText(data, keyBuffer: ArrayBuffer | CryptoKey) : Promise<String> {
    return bufferToString(await aesDecrypt(hexToBuffer(data), keyBuffer));
}

export async function aesEncrypt(dataBuffer, keyBuffer: ArrayBuffer | CryptoKey): Promise<Uint8Array> {
    const key = (keyBuffer instanceof CryptoKey) ? keyBuffer : await bufferToAesKey(keyBuffer);
    const iv = await generateIv();
    const ciphertext = await crypto.subtle.encrypt({
        name: 'AES-CBC',
        iv
    }, key, dataBuffer);
    return attachIv(iv, ciphertext);
};

export async function aesDecrypt(dataBuffer, keyBuffer) {
    const key = (keyBuffer instanceof CryptoKey) ? keyBuffer : await bufferToAesKey(keyBuffer);
    const { iv, data } = detachIv(dataBuffer);

    return await crypto.subtle.decrypt(
        { name: 'AES-CBC', iv },
        key,
        data
    );
};

export function bufferToHex(buf) {
    const keyHex = [...new Uint8Array(buf)]
        .map(b => b.toString(16).padStart(2, "0"))
        .join("");
    return keyHex;
}

export function hexToBuffer(hex) {
    const typedArray = new Uint8Array(hex.match(/[\da-f]{2}/gi).map(function (h) {
        return parseInt(h, 16)
    }))
    return typedArray.buffer
}

export function bufferToString(buf) {
    return String.fromCharCode.apply(null, new Uint8Array(buf));
}

export function stringToBuffer(str) {
    var enc = new TextEncoder();
    return enc.encode(str);
}