import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";

dayjs.extend(relativeTime);


export default function formatDate(str) {
    if (!str) return null;
    const date = dayjs(str);
    return date.format("YYYY-MM-DD HH:mm");
}

export function formatDateRelative(str) {
    if (!str) return null;
    const date = dayjs(str);

    return date.fromNow();
}