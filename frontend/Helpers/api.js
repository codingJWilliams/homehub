import { useEffect, useState } from "react";
import { DetailedError, checkError } from "./errors";

const base = `${process.env.REACT_APP_API ? process.env.REACT_APP_API : ''}/api`;

function getToken() {
    const store = localStorage.getItem("token");
    if (!store || !JSON.parse(store) || !JSON.parse(store).state) throw new Error("Could not get authentication token");
    return JSON.parse(store).state;
}

/**
 * 
 * @param {string} url 
 * @param {RequestInit} fetchParams 
 */
export function useApi(url, fetchParams, makeRequest = true) {
    const [data, setData] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState(null);

    const refresh = async () => {
        setIsLoading(true);
        if (!makeRequest) return;

        const params = fetchParams ?? {};
        const headers = params.headers ?? {};
        headers.Authorization = `Bearer ${getToken()}`;
        headers.Accept = `application/json`;

        const response = await fetch(`${base}${url}`, { ...params, headers });
        try {
            const responseBody = await response.json();
            // await new Promise((res) => setTimeout(res, 1000));

            try {
                await checkError(response, responseBody);

                setError(null);
                setData(responseBody);
                setIsLoading(false);
            } catch (e) {
                setError(e);
                setData(responseBody);
                setIsLoading(false);
            }
        } catch (e) {
            setError(new DetailedError(
                "Unable to process server response", 
                "The server returned an invalid JSON payload. This could be due to a proxy inbetween the app and the server, or an issue with the application."
            ));
            setData(null);
            setIsLoading(false);
        }
    };

    /* eslint-disable react-hooks/exhaustive-deps */
    useEffect(() => { refresh() }, [url, makeRequest])

    return { data, isLoading, error, refresh };
}

export async function getResource(resource) {
    const response = await fetch(`${base}${resource}`, {
        method: "get",
        headers: {
            'Accept': 'application/json',
            'Authorization': `Bearer ${getToken()}`,
        }
    });
    const responseBody = await response.json();
    await checkError(response, responseBody);

    return responseBody;
}

export async function updateResource(resource, data) {
    const response = await fetch(`${base}${resource}`, {
        method: "put",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': `Bearer ${getToken()}`,
        }
    });
    const responseBody = await response.json();
    await checkError(response, responseBody);

    return responseBody;
}

export async function uploadFile(resource, file) {
    const data = new FormData();
    data.set("file", file);

    const response = await fetch(`${base}${resource}`, {
        method: "post",
        body: data,
        headers: {
            // 'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': `Bearer ${getToken()}`,
        }
    });
    const responseBody = await response.json();
    await checkError(response, responseBody);

    return responseBody;
}

export async function getFileChunk(resource) {
    const response = await fetch(`${base}${resource}`, {
        method: "get",
        headers: {
            'Authorization': `Bearer ${getToken()}`,
        }
    });
    const responseBody = await response.arrayBuffer();
    await checkError(response, {});

    return responseBody;
}

export async function deleteResource(resource, data) {
    const response = await fetch(`${base}${resource}`, {
        method: "delete",
        body: JSON.stringify(data),
        headers: {
            'Accept': 'application/json',
            'Authorization': `Bearer ${getToken()}`,
            'Content-Type': 'application/json',
        }
    });

    const responseBody = await response.json();
    await checkError(response, responseBody);

    return responseBody;
}