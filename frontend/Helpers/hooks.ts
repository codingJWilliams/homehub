import { useEffect, useRef, useState } from "react";
import { useSearchParams } from "react-router-dom";

export const useEffectAfterMount = (cb, dependencies) => {
    const mounted = useRef(true);

    useEffect(() => {
        if (!mounted.current) {
            return cb();
        }
        mounted.current = false;
    }, dependencies); // eslint-disable-line react-hooks/exhaustive-deps
};


export function useDebouncedFunction(func, debounceDelay) {
    const [intv, setIntv] = useState(null);

    return function () {
        if (intv) clearTimeout(intv);
        setIntv(setTimeout(func.bind(null, ...Array.from(arguments)), debounceDelay));
    }
}

export function useUrlParameter(key, defaultValue) {
    const params = new URLSearchParams(window.location.search);
    const [value, setValue] = useState(params.get(key));

    useEffect(() => {
        const params = new URLSearchParams(window.location.search);
        console.log("search update", params.toString());
        const val = params.get(key);
        setValue(val ? val : defaultValue);
    }, [ window.location.search ]);

    return [
        value, 
        (newValue) => {
            const params = new URLSearchParams(window.location.search);
            if (newValue.toString() === defaultValue) params.delete(key);
            else params.set(key, newValue);
            window.history.pushState({}, "", `?${params.toString()}`);
            setValue(newValue.toString());
        }
    ]
}