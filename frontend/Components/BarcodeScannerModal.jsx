import { Box, Button, Modal, Typography } from "@mui/material";
import { theme } from "../Common/theme";
// import BarcodeScannerComponent from "react-qr-barcode-scanner";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    bgcolor: 'background.paper',
    border: '2px solid white',
    boxShadow: 24,
    minWidth: '50%',
    p: 4,
    [theme.breakpoints.down('sm')]: {
        minWidth: '90%'
    }
};

export default function BarcodeScannerModal({ isOpen, selectionCb }) {
    return (
        <Modal
            open={isOpen}
            onClose={() => selectionCb(null)}
        >
            <Box sx={style}>
                <Typography variant="h5" sx={{ pb: 2 }}>
                    Scan a barcode
                </Typography>
                {/* <BarcodeScannerComponent
                    onUpdate={(err, result) => {
                        if (result) selectionCb(result.text);
                    }}
                    torch={true}
                /> */}
                <Button
                    color="primary"
                    sx={{ mt: 1, float: 'right' }}
                    variant="contained"
                    onClick={() => selectionCb(null)}
                >
                    Cancel
                </Button>
            </Box>
        </Modal >
    );
}