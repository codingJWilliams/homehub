export default function StyleResetLink(props) {
    return (
        <a
            {...props}
            ref={(el) => el && el.style.setProperty("color", props.color, "important")}
        />
    );
}