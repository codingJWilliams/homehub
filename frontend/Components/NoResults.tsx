import { Box, Paper, Typography } from "@mui/material";
import { HeartBroken } from "@mui/icons-material";
import { PropsWithChildren } from "react";
import { PaperCard, PaperCardBody, PaperCardHeader } from "./PaperCard";

export default function NoResults({ children }: PropsWithChildren) {
    return (
        <PaperCard>
            <PaperCardHeader>
                <Typography variant="h5" mt={0} style={{ display: "flex", alignItems: "center", gap: "10px" }}>
                    <HeartBroken fontSize="inherit" /> No records found
                </Typography>
            </PaperCardHeader>
            <PaperCardBody>
                You could try refining your search term and filters, or create a new record.
            </PaperCardBody>
        </PaperCard>
    );
}