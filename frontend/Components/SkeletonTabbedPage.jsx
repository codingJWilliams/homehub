import { Skeleton, Tab } from "@mui/material";
import TabbedPage from "../Common/Layout/TabbedPage";
import FullwidthCircular from "./FullwidthCircular";

export default function SkeletonTabbedPage({ }) {
    return (
        <TabbedPage
            title={<Skeleton width={400} />}
            subtitle={<Skeleton width={200} />}
            tabValue={"sel"}
            onTabChange={() => {}}
            tabs={[
                <Tab label={<Skeleton width={60} />} value="sel" key={0} />,
                <Tab label={<Skeleton width={50} />} value="notsel1" key={1} />,
                <Tab label={<Skeleton width={80} />} value="notsel1" key={2} />
            ]}
            tabContent={<FullwidthCircular sx={{ height: "20vh" }} />}
        />
    )
}