import { Box, Button, Modal, Typography } from "@mui/material";
import { useApi } from "../../Helpers/api";
import { useState } from "react";
import { useDebouncedFunction, useEffectAfterMount, useUrlParameter } from "../../Helpers/hooks";
import TreePicker from "./TreePicker";
import CreateModal from "../../Common/CRUD/Pages/CreateModal";
import ModalBox from "../ModalBox";
import PaginatedIndex from "../../Common/CRUD/PaginatedIndex";
import ResultsTable from "../../Common/CRUD/ResultsTable";
import useStickyState from "../../Helpers/useStickyState";


export default function AjaxPickerModal({ isOpen, selectionCb, resource, title, creationForm, columns, initialValue, clearable = true, excludeIds = [], multiple = false }) {
    const [selectionModel, setSelectionModel] = useState(initialValue ? [initialValue] : []);
    const [isCreating, setIsCreating] = useState(false);
    const [offset, setOffset] = useState(0);
    const [limit, setLimit] = useStickyState("modal_pagination_limit", 10);
    const [gridCols] = useState(columns);
    const [query, setQuery] = useState("");
    const [search, setSearch] = useState("");
    const debouncedSetSearch = useDebouncedFunction(setSearch, 800);
    const paginationParams = new URLSearchParams();
    paginationParams.append("pagination[offset]", offset);
    paginationParams.append("pagination[limit]", limit);
    paginationParams.append("query", search);
    if (excludeIds.length) paginationParams.append("exclude", excludeIds.join(","));
    const indexResponse = useApi(`${resource}?${paginationParams}`, { headers: { Accept: "application/json" } });
    useEffectAfterMount(() => {
        if (isOpen) indexResponse.refresh();
    }, [isOpen]);

    let modal = null;

    if (isCreating)
        modal = (
            <CreateModal
                open={isOpen}
                onSave={(createdId) => {
                    setIsCreating(false);
                    if (createdId) selectionCb(createdId, "");
                }}
                form={creationForm}
                resource={resource}
                title={title}
            />
        );
    else if (indexResponse)
        modal = (
            <ModalBox
                isOpen={isOpen}
                onClose={() => selectionCb()}
            >
                <Typography variant="h5" sx={{ pb: 2 }}>
                    {title}
                </Typography>

                <PaginatedIndex
                    {...{ resource, indexResponse, limit, setLimit, query, setOffset }}
                    limitedOptions={true}
                    componentProps={{
                        width: "100%",
                        rowSelectionModel: selectionModel,
                        columns: gridCols,
                        compact: true,
                        onRowSelectionModelChange: model => {
                            // Only allow one selection
                            if (model.length > 1 && !multiple) setSelectionModel([model[1]]);
                            else setSelectionModel(model);
                        }
                    }}
                    forceFocus={true}
                    setQuery={q => {
                        setQuery(q);
                        debouncedSetSearch(q);
                    }}
                />

                <Button
                    color="primary"
                    sx={{ mt: 1, float: 'right' }}
                    variant="contained"
                    onClick={() => {
                        if (multiple) selectionCb(selectionModel)
                        else selectionCb(selectionModel.length ? selectionModel[0] : null)
                    }}
                    // onClick={() => selectionCb(selectedId, selectedDisplayName)}
                    disabled={selectionModel.length === 0}
                >
                    Select
                </Button>
                {clearable ?
                    <Button
                        color="warning"
                        sx={{ mt: 1, mr: 1, float: 'right' }}
                        variant="outlined"
                        onClick={() => {
                            selectionCb(null);
                            setSelectionModel([]);
                        }}
                    >
                        Clear
                    </Button>
                    : null}
                {creationForm ?
                    <Button
                        color="info"
                        sx={{ mt: 1, mr: 1, float: 'right' }}
                        variant="outlined"
                        onClick={() => setIsCreating(true)}
                    >
                        Create
                    </Button>
                    : null}
            </ModalBox >
        );

    return modal;
}