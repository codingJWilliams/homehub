import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import { TreeItem, TreeView } from "@mui/lab";
import { Box } from "@mui/material";
import { useEffect } from "react";

export default function TreePicker({ selectionUpdatedCb, items, initialValue }) {
    const renderTree = nodes => (
        <TreeItem key={nodes.id} nodeId={`${nodes.id}`} label={nodes.name || nodes.tag}>
            {nodes.children ? nodes.children.map(n => renderTree(n)) : null}
        </TreeItem>
    );

    const findInTree = (id, items) => {
        for (const item of items) {
            if (item.id === id) return item;
            if (item.children) {
                const childResult = findInTree(id, item.children);
                if (childResult) return childResult;
            }
        }
        return null;
    }

    useEffect(() => {
        const found = findInTree(parseInt(initialValue), items);
        if (found) {
            selectionUpdatedCb(initialValue, found.name);
        }
    }, [initialValue]);

    return (
        <Box sx={{ bgcolor: 'background.offset' }}>
            <TreeView
                defaultCollapseIcon={<ExpandMoreIcon />}
                defaultExpandIcon={<ChevronRightIcon />}
                defaultSelected={[`${initialValue}`]}
                sx={{ height: 240, flexGrow: 1, overflowY: 'auto' }}
                onNodeSelect={(event, nodeId) => {
                    selectionUpdatedCb(nodeId, findInTree(parseInt(nodeId), items).name);
                }}
            >
                {items.map(item => renderTree(item))}
            </TreeView>
        </Box>
    )
}