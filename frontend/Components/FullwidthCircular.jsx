import { Box, CircularProgress } from "@mui/material";

export default function FullwidthCircular({ sx }) {
    return (
        <Box sx={{ display: 'flex', justifyContent: "center", alignItems: "center", width: "100%", ...sx }}>
            <CircularProgress />
        </Box>
    );
}