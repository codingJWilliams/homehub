import { Box, Paper, lighten, styled } from "@mui/material";

export const PaperCard = styled(Paper)(({theme}) => ({
    padding: theme.spacing(0),
    border: `1px solid ${lighten(theme.palette.background.paper, 0.15)}`,
    boxShadow: "none",
    borderRadius: theme.spacing(1)
}));

export const PaperCardHeader = styled(Box)(({theme}) => ({
    padding: theme.spacing(2),
    paddingBottom: "0px"
}));

export const PaperCardBody = styled(Box)(({theme}) => ({
    padding: theme.spacing(2)
}));