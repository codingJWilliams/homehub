import { LoadingButton } from "@mui/lab";
import { useState } from "react";

export default function ButtonAsync(props) {
    const [loading, setLoading] = useState(false);
    const { ajax } = props;
    let copyProps = {...props};
    delete copyProps.ajax;

    const run = (evt) => {
        setLoading(true);
        ajax(evt).then(() => setLoading(false));
    };

    return (
        <LoadingButton
            {...copyProps}
            loading={loading}
            onClick={run}
        >
            {props.children}
        </LoadingButton>
    );
}