import { Box, Typography } from "@mui/material";
import { PaperCard, PaperCardBody, PaperCardHeader } from "./PaperCard";
import { Error } from "@mui/icons-material";
import InfoTable from "./InfoTable";
import { DetailedError } from "../Helpers/errors";

export default function ErrorMessage({ title = "There's been a problem", children, details, exception }) {

    let displayTitle = title;
    let displayChildren = children;
    let displayDetails = details;

    if (exception) {
        if (exception instanceof DetailedError) {
            displayTitle = exception.title;
            displayChildren = exception.message;
            displayDetails = exception.details;
        } else if (exception instanceof Error) {
            displayTitle = "There was an unknown error";
            displayChildren = exception.message;
            displayDetails = {
                Filename: exception.fileName,
                Line: exception.lineNumber,
                Details: exception.toString()
            }
        }
    }

    return (
        <Box sx={{ display: "flex", justifyContent: "center", flexDirection: "column", gap: "15px" }}>
            <PaperCard sx={{ maxWidth: "100%", minWidth: "500px" }}>
                <PaperCardHeader>
                    <Typography variant="h5" color="error" sx={{ display: "flex", alignItems: "center" }}>
                        <Error fontSize="inherit" sx={{ mr: 1 }} />
                        {displayTitle}
                    </Typography>
                </PaperCardHeader>
                <PaperCardBody>
                    {displayChildren}
                </PaperCardBody>
            </PaperCard>
            {displayDetails ?
                <PaperCard sx={{ maxWidth: "100%", minWidth: "500px" }}>
                    <PaperCardHeader>
                        <Typography variant="h5">More Details</Typography>
                    </PaperCardHeader>
                    <PaperCardBody>
                        <InfoTable data={displayDetails} />
                    </PaperCardBody>
                </PaperCard> : null}
        </Box>
    )
}