import { Chip } from "@mui/material";
import SHA256 from 'sha256-es';

export const getColor = text => `hsl(${parseInt(SHA256.hash(text).substr(0, 2), 16)}deg 21.5% 62.5%)`;

export default function ColorChip({ label, icon }) {
    return (
        <Chip icon={icon} size="small" label={label} color="primary" sx={{ bgcolor: getColor(label) }} />
    );
}