import { useEffect, useState } from "react";
import * as c from "../Helpers/crypto/key-derivation";
import { Skeleton } from "@mui/material";

export default function DecryptingText({ hex, vaultKey }) {
    const [isDecrypted, setIsDecrypted] = useState(false);
    const [data, setData] = useState(null);
    const [skeletonWidth] = useState(Math.floor(30 + (Math.random() * 25)));

    useEffect(() => {
        c.aesDecryptText(hex, vaultKey).then(plaintext => {
            setData(plaintext);
            setIsDecrypted(true);
        }).catch(err => {
            setIsDecrypted(false);
            console.log("Problem is", err);
        });
    }, [hex, vaultKey]);

    if (!isDecrypted) return <Skeleton width={skeletonWidth} />;

    return data;
}