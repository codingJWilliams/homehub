import { InputBase, lighten, styled } from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import { theme } from "../Common/theme.js";
import { useEffect, useRef, useState } from "react";
import { useDebouncedFunction } from "../Helpers/hooks";

const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    borderWidth: "1px",
    borderColor: lighten(theme.palette.grey[900], 0.09),
    borderStyle: "solid",
    backgroundColor: theme.palette.grey[900],
    '&:hover': {
        backgroundColor: lighten(theme.palette.grey[900], 0.03),
    },
    marginLeft: 0,
    width: '100%',
    maxWidth: '500px'
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 1.3),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    width: '100%',
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        paddingLeft: `calc(1em + ${theme.spacing(3)})`,
        transition: theme.transitions.create('width'),
    },
}));

export default function ({ value, onSearch, forceFocus }) {
    const ref = useRef();
    // const [ query, setQuery ] = useState("");
    // const onChange: any = useDebouncedFunction(onSearch, 500);

    useEffect(() => {
        if (forceFocus) {
            const r: any = ref;
            if (ref.current) r.current.focus();
        }
    }, []);

    const active = value && value.length > 0;

    return (
        <Search sx={{ mb: 1.5, borderColor: active ? theme.palette.primary.main : undefined }}>
            <SearchIconWrapper>
                <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase
                placeholder="Search…"
                inputProps={{ 'aria-label': 'search' }}
                inputRef={ref}
                value={value ?? ""}
                onChange={(evt: any) => {
                    onSearch(evt.target.value);
                    // setQuery(evt.target.value);
                }}
            />
        </Search>
    );
}