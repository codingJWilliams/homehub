import { Box, CircularProgress, ThemeProvider, Typography } from "@mui/material";
import { theme } from "../Common/theme";

export default function AppFullpageMessage({ message }) {
    return (
        <ThemeProvider theme={theme}>
            <Box sx={{
                width: "100vw",
                height: "100vh",
                position: "absolute",
                top: "0",
                left: "0",
                // background: "black",
                bgcolor: "background.paper",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "column"
            }} >
                <Typography variant="h4" color="primary">{message}</Typography>
                <CircularProgress size="3.5rem" sx={{ mt: 4 }} />
            </Box>
        </ThemeProvider>

    )
}