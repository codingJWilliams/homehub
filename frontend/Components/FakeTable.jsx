import { Box, Skeleton, darken } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import { theme } from "../Common/theme";

const LoadingSkeleton = () => (
    <Box
        sx={{
            height: "max-content"
        }}
    >
        {[...Array(10)].map((_) => (
            <Skeleton variant="rectangular" sx={{ my: 4, mx: 1 }} />
        ))}
    </Box>
);

export default function FakeTable({ cols = 3, rows = 10 }) {
    return (
        <DataGrid
            rows={Array(rows).fill({}).map(r => ({ id: Math.random() }))}
            columns={
                cols
                    .map(c => ({
                        field: c,
                        // renderHeader: params => <Skeleton variant="rounded" width="100%" />, 
                        flex: 10,
                        renderCell: params => <Skeleton variant="rounded" height={30} width={`${Math.floor(Math.random() * 50) + 50}%`} />
                    }))}
            pageSize={rows}
            // autoHeight
            // sx={{minHeight: 600}}  if you want to use autoHeight you will need add some height
            rowsPerPageOptions={[10]}
            disableSelectionOnClick
            disableColumnMenu
            hideFooter
            disableColumnSelector
            loading={true}
            sx={{
                bgcolor: "background.offset",
                borderColor: darken(theme.palette.divider, 0.45),
                "& .MuiDataGrid-row .MuiDataGrid-cell": {
                    borderColor: darken(theme.palette.divider, 0.45)
                },
                "& .MuiDataGrid-columnHeaders": {
                    borderColor: darken(theme.palette.divider, 0.45)
                }
            }}
        />
    )
}