import { Chip, Tab } from "@mui/material";
import { theme } from "../Common/theme";

export default function BadgedTab(props) {
    const { badge, label } = props;

    if (!badge) return <Tab {...props} />;

    return (
        <Tab
            {...props}
            sx={{ flexDirection: "row" }}
            label={
                <>
                    {label}
                    <Chip color="primary" size="small" sx={{
                        "& .MuiChip-label": {
                            px: 0.4,
                        },
                        '&.MuiChip-root': {
                            height: theme.spacing(2),
                            ml: 0.6,
                            borderRadius: "20%"
                        }
                    }} label={badge} />
                </>
            }
        />
    );
}