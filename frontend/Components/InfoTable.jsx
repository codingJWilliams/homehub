import { QrCodeScannerOutlined } from "@mui/icons-material";
import { Table, TableBody, TableCell, TableContainer, TableRow } from "@mui/material";
import { isValidElement } from "react";

export default function InfoTable({ data }) {
    return (
        <TableContainer>
            <Table size="small" sx={{ maxWidth: "100%" }}>
                <TableBody>
                    {Object.keys(data).map(key => (
                        <TableRow hover key={key}>
                            <TableCell>{key}</TableCell>
                            <TableCell>{(typeof data[key] === "string" || isValidElement(data[key])) ? data[key] : JSON.stringify(data[key])}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}