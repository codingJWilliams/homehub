import { Button, MenuItem } from "@mui/material";
import { useRef, useState } from "react";

export default function ButtonFile(props) {
    const inputRef = useRef();
    const { onFileSelect } = props;
    let copyProps = { ...props };
    delete copyProps.onFileSelect;
    delete copyProps.onClick;

    const run = (evt) => {
        if (inputRef.current) inputRef.current.click();
        if (props.onClick) props.onClick();
    };

    return (
        <>
            <input
                type="file"
                style={{ display: "none" }}
                ref={inputRef}
                onChange={(evt) => {
                    console.log("got an chaneg");
                    if (evt.target.files) onFileSelect(evt.target.files[0]);
                }}
            ></input>
            <MenuItem
                {...copyProps}
                onClick={run}
            >
                {props.children}
            </MenuItem>
        </>
    );
}