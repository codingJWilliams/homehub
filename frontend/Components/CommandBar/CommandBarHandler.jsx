import { useEffect, useState } from "react";
import CommandBar from ".";

export function CommandBarHandler({ }) {
    const [isOpen, setIsOpen] = useState(false);
    window.openCommandBar = () => setIsOpen(true);
    
    useEffect(() => {
        const handler = (ev) => {
            if ((ev.ctrlKey || ev.metaKey) && ev.key === "k") setIsOpen(!isOpen);
        }
        window.addEventListener("keydown", handler)

        return () => window.removeEventListener("keydown", handler);
    });

    return (
        <CommandBar open={isOpen} onClose={() => setIsOpen(false)}></CommandBar>
    )
}