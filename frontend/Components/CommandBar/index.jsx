import { Box, lighten, styled } from '@mui/material';
import Modal from '@mui/material/Modal';
import Search from './Search';
import ResultsContainer from './ResultsContainer';
import { useEffect, useRef, useState } from 'react';
import { useNavigate } from "react-router-dom";
import { useApi } from '../../Helpers/api';

const ModalBox = styled(Box)(({ theme }) => ({
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: "50%",
    [theme.breakpoints.down("md")]: {
        width: "80%"
    },
    [theme.breakpoints.down("sm")]: {
        width: `calc(100% - ${theme.spacing(4)})`,
    },
    backgroundColor: theme.palette.background.paper,
    border: `1px solid ${lighten(theme.palette.background.paper, 0.15)}`,
    borderRadius: theme.spacing(2),
    boxShadow: 24,
    "&:focus": {
        outline: "none"
    }
}));

export default function CommandBar({ open, onClose }) {
    const keyboardActionRef = useRef(null);
    const [searchTerm, setSearchTerm] = useState("");
    const [searchHasStarted, setSearchHasStarted] = useState(false);
    const { data, isLoading, error, refresh } = useApi(`/search/?query=${encodeURIComponent(searchTerm)}`, { headers: { Accept: "application/json" } });

    useEffect(() => {
        setSearchHasStarted(false);
        setSearchTerm("");
    }, [open]);

    return (
        <Modal open={open} onClose={onClose}>
            <ModalBox>
                <Search 
                    keyboardActionRef={keyboardActionRef} 
                    open={open} 
                    setSearchTerm={(t) => {
                        setSearchTerm(t);
                        setSearchHasStarted(true);
                    }}
                />
                <ResultsContainer 
                    keyboardActionRef={keyboardActionRef} 
                    items={data ? data.items : []} 
                    isLoading={isLoading} 
                    searchHasStarted={searchHasStarted}
                    onResultSelected={onClose} 
                    error={error}
                />
            </ModalBox>
        </Modal>
    );
}