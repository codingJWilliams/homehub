import { Box, InputBase, lighten, styled } from "@mui/material";
import { theme } from "../../Common/theme";
import ResultItem from "./ResultItem";
import { useEffect, useRef, useState } from "react";
import { useDebouncedFunction } from "../../Helpers/hooks";

const SearchInput = styled(InputBase)(({ theme }) => ({
    width: "100%",
}));

const boxStyle = {
    padding: `${theme.spacing(1)} ${theme.spacing(2)}`,
    width: "100%",
    borderBottom: `1px solid ${lighten(theme.palette.background.paper, 0.15)}`
};

export default function Search({ keyboardActionRef, setSearchTerm, open }) {
    const [searchValue, setSearchValue] = useState("");
    const debouncedSet = useDebouncedFunction(setSearchTerm, 400);
    const input = useRef();

    useEffect(() => {
        setSearchValue("");

        if (input.current) input.current.focus();
    }, [open]);

    return (
        <Box sx={boxStyle}>
            <SearchInput
                value={searchValue}
                placeholder="Search for entries..."
                inputRef={input}
                onKeyUp={evt => {
                    if (evt.code === "ArrowUp") {
                        if (keyboardActionRef.current) keyboardActionRef.current("up");
                    } else if (evt.code === "ArrowDown") {
                        if (keyboardActionRef.current) keyboardActionRef.current("down");
                    } else if (evt.code === "Enter") {
                        if (keyboardActionRef.current) keyboardActionRef.current("click");
                    }
                }}
                onChange={evt => {
                    setSearchValue(evt.target.value)
                    debouncedSet(evt.target.value);
                }}
            />
        </Box>
    );
}