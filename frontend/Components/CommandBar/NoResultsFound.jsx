import { Search } from "@mui/icons-material";
import { Box, Typography } from "@mui/material";

export default function NoResultsFound({ searchHasStarted }) {
    if (!searchHasStarted) return (
        <Box py={5} textAlign="center">
            <Typography variant="h2"><Search fontSize="inherit" /></Typography>
            <Typography variant="h5">Enter a search term</Typography>
            <Typography variant="subtitle1">Search globally across HomeHub</Typography>
        </Box>
    );

    return (
        <Box py={5} textAlign="center">
            <Typography variant="h2"><Search fontSize="inherit" /></Typography>
            <Typography variant="h5">No results found</Typography>
            <Typography variant="subtitle1">Try broadening your search term</Typography>
        </Box>
    )
}