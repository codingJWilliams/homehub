import { Assignment } from "@mui/icons-material";
import { Avatar, Box, Skeleton, Typography, lighten, styled } from "@mui/material";
import { green } from "@mui/material/colors";
import { getColor } from "../ColorChip";

const ItemContainer = styled(Box)(({ theme }) => ({
    background: lighten(theme.palette.background.paper, 0.04),
    "&:hover": {
        background: lighten(theme.palette.background.paper, 0.08),
    },
    cursor: "pointer",
    transition: "ease 0.1s background",
    borderRadius: theme.spacing(0.75),
    padding: theme.spacing(1),
    "&:not(:last-child)": {
        marginBottom: theme.spacing(1)
    },
    width: "100%",
    display: "flex",
    alignItems: "center",
    gap: 10
}));

export default function MockResultItem({ icon, title, description, link }) {
    return (
        <ItemContainer>
            <Skeleton variant="rectangular" width={40} height={40} />
            <Box sx={{ display: "flex", flexDirection: "column" }}>
                <Typography variant="subtitle1" sx={{ lineHeight: 1.25 }}>
                    <Skeleton width={80} />
                </Typography>
                <Typography variant="subtitle2" sx={{ lineHeight: 1.25 }}>
                    <Skeleton width={130} />
                </Typography>
            </Box>
        </ItemContainer>
    );
}