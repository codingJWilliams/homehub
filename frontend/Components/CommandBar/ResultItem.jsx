import { Assignment, KeyboardReturn } from "@mui/icons-material";
import { Avatar, Box, Typography, lighten, styled } from "@mui/material";
import { green } from "@mui/material/colors";
import { getColor } from "../ColorChip";

const ItemContainer = styled(Box)(({ theme }) => ({
    background: lighten(theme.palette.background.paper, 0.04),
    "&.selected": {
        background: lighten(theme.palette.background.paper, 0.08),
    },
    cursor: "pointer",
    transition: "ease 0.1s background",
    borderRadius: theme.spacing(0.75),
    padding: theme.spacing(1),
    "&:not(:last-child)": {
        marginBottom: theme.spacing(1)
    },
    width: "100%",
    display: "flex",
    alignItems: "center",
    gap: 10
}));

export default function ResultItem({ icon, title, description, link, selected, setSelected, onClick }) {
    return (
        <ItemContainer className={selected ? "selected" : null} onMouseEnter={() => setSelected(true)} onClick={onClick}>
            <Avatar sx={{ bgcolor: getColor(title) }} variant="rounded">
                {icon}
            </Avatar>
            <Box sx={{ display: "flex", flexDirection: "column" }}>
                <Typography variant="subtitle1" sx={{ lineHeight: 1.25 }}>
                    {title}
                </Typography>
                <Typography variant="subtitle2" sx={{ lineHeight: 1.25 }}>
                    {description}
                </Typography>
            </Box>
            {selected && (
                <>
                    <Box sx={{ flexGrow: 1 }} />
                    <KeyboardReturn />
                </>
            )}
        </ItemContainer>
    );
}