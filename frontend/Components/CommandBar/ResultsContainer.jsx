import { Box, CircularProgress, Typography } from "@mui/material";
import { theme } from "../../Common/theme";
import ResultItem from "./ResultItem";
import { Place, Smartphone } from "@mui/icons-material";
import MockResultItem from "./MockResultItem";
import { Fragment, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { mainList } from "../../Common/Layout/listItems";
import ErrorMessage from "../ErrorMessage";
import NoResultsFound from "./NoResultsFound";

const boxStyle = {
    padding: theme.spacing(2),
    maxHeight: "80vh",
    overflowY: "scroll"
};

const ResultHeader = ({ text }) => (
    <Typography variant="subtitle2" sx={{ "&:not(:first-child)": { mt: 2 }, mb: 0.4 }}>{text}</Typography>
);

export default function ResultsContainer({ items, isLoading, keyboardActionRef, searchHasStarted, onResultSelected, error }) {
    const [selectedId, setSelectedId] = useState(null);
    const navigate = useNavigate();

    const findItem = id => {
        if (!id || !items) return null;
        const [model, searchId] = id.split(".");

        if (items[model]) {
            const found = items[model].find(it => it.id === parseInt(searchId));
            return found;
        }
    };

    const getRouterEntry = item => {
        for (const category of Object.keys(mainList)) {
            const found = mainList[category].find(entry => entry.model === item.model);
            if (found) return found;
        }
    };

    const moveDown = () => {
        const [model, searchId] = selectedId.split(".");

        const currentModelIndex = Object.keys(items).indexOf(model);
        const currentIndex = items[model].findIndex(it => it.id === parseInt(searchId));

        if (items[model].length > (currentIndex + 1)) {
            setSelectedId(items[model][currentIndex + 1].compositeId);
        } else if (Object.keys(items).length > (currentModelIndex + 1)) {
            setSelectedId(items[Object.keys(items)[currentModelIndex + 1]][0].compositeId);
        }
    }

    const moveUp = () => {
        const [model, searchId] = selectedId.split(".");

        const currentModelIndex = Object.keys(items).indexOf(model);
        const currentIndex = items[model].findIndex(it => it.id === parseInt(searchId));

        if (currentIndex > 0) {
            setSelectedId(items[model][currentIndex - 1].compositeId);
        } else if (currentModelIndex > 0) {
            const newModelKey = Object.keys(items)[currentModelIndex - 1];
            setSelectedId(items[newModelKey][items[newModelKey].length - 1].compositeId);
        }
    }

    const clickItem = it => {
        const rt = getRouterEntry(it);
        navigate(`${rt.url}/${it.id}`);
        console.log("navigate")
    }

    const keyboardAction = (action) => {
        if (isLoading) return null;

        if (action === "up") {
            moveUp();
        } else if (action === "down") {
            moveDown();
        } else if (action === "click" && selectedId) {
            clickItem(findItem(selectedId));
            (onResultSelected || (() => {}))(selectedId)
        }
    };

    keyboardActionRef.current = keyboardAction;

    useEffect(() => {
        if (!isLoading && !error && !findItem(selectedId)) {
            const firstKey = Object.keys(items)[0];
            if (firstKey) {
                setSelectedId(items[firstKey][0].compositeId);
                return;
            }
            setSelectedId(null);
        }
    }, [items]);

    if (error) return (
        <ErrorMessage exception={error} />
    );

    //, display: "flex", justifyContent: "center", py: 10
    if (isLoading) return (
        <Box sx={{ ...boxStyle }}>
            <MockResultItem />
            <MockResultItem />
            <MockResultItem />
            <MockResultItem />
            <MockResultItem />
        </Box>
    );

    if (!Object.keys(items).some(k => items[k].length)) return (
        <NoResultsFound searchHasStarted={searchHasStarted} />
    );

    return (
        <Box sx={boxStyle}>
            {
                Object.keys(items).map(group => (
                    <Fragment key={group}>
                        <ResultHeader text={group} />
                        {items[group].map(item => {
                            const ItemIcon = getRouterEntry(item).icon;

                            return (
                                <ResultItem
                                    key={item.compositeId}
                                    title={item.title}
                                    description={item.description ?? ''}
                                    icon={<ItemIcon />}
                                    selected={selectedId === item.compositeId}
                                    setSelected={() => setSelectedId(item.compositeId)}
                                    onClick={() => clickItem(item)}
                                />
                            )
                        }
                        )}
                    </Fragment>
                ))
            }
        </Box>
    )
}