import { useState } from "react";
import { useApi } from "../Helpers/api";
import { PaperCard, PaperCardBody, PaperCardHeader } from "./PaperCard";
import { Box, Grid, Typography } from "@mui/material";
import LoadingWrapper from "./LoadingWrapper";
import dayjs from "dayjs";
import { DatePicker } from "@mui/x-date-pickers";

export default function DateApiWrapper({ component, pathFn }) {
    const fromBase = new Date();
    fromBase.setTime(fromBase.getTime() - (1000 * 60 * 60 * 24 * 7));
    const [from, setFrom] = useState(fromBase.toISOString().split("T")[0]);
    const [to, setTo] = useState(new Date().toISOString().split("T")[0]);
    const { data, isLoading, error } = useApi(pathFn({ from, to }));

    const DisplayComponent = component;

    return (
        <>
            <PaperCard sx={{ mb: 2 }}>
                <PaperCardHeader>
                    <Typography variant="h5">Date filters</Typography>
                </PaperCardHeader>
                <PaperCardBody>
                    <Grid container gap="10px">
                        <Grid item>
                            <Typography variant="subtitle2">From</Typography>
                            <DatePicker
                                defaultValue={dayjs(from)}
                                onChange={(newValue) => setFrom(newValue.format("YYYY-MM-DD"))}
                            />
                        </Grid>
                        <Grid item>
                            <Typography variant="subtitle2">To</Typography>
                            <DatePicker
                                defaultValue={dayjs(to)}
                                onChange={(newValue) => setTo(newValue.format("YYYY-MM-DD"))}
                            />
                        </Grid>
                    </Grid>
                </PaperCardBody>
            </PaperCard>
            <LoadingWrapper isLoading={isLoading} error={error}>
                <DisplayComponent data={data} />
            </LoadingWrapper>
        </>
    );
}