import { InputBase, darken, lighten, styled } from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import { theme } from "../Common/theme.js";
import { useEffect, useRef, useState } from "react";
import { useDebouncedFunction } from "../Helpers/hooks.js";

const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    borderWidth: "1px",
    borderColor: darken(theme.palette.primary.main, 0.3),
    borderStyle: "solid",
    backgroundColor: darken(theme.palette.primary.main, 0.2),
    '&:hover': {
        backgroundColor: darken(theme.palette.primary.main, 0.3),
    },
    marginLeft: 0,
    width: '100%',
    maxWidth: '500px'
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 1.3),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    width: '100%',
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        paddingLeft: `calc(1em + ${theme.spacing(3)})`,
        transition: theme.transitions.create('width'),
    },
}));

export default function NavbarSearchInput({ onClick }) {
    return (
        <Search>
            <SearchIconWrapper>
                <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase
                placeholder="Search globally... (CMD+K)"
                inputProps={{ 'aria-label': 'search' }}
                value={""}
                onClick={onClick}
            />
        </Search>
    );
}