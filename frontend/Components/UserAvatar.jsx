import styled from "@emotion/styled";
import { Avatar, Box, darken, lighten } from "@mui/material";
import { deepOrange } from "@mui/material/colors";
import { getColor } from "./ColorChip";
import { theme } from "../Common/theme";
import { Link } from "react-router-dom";

const SmallAvatar = styled(Avatar)(({ theme }) => ({
    width: 28,
    height: 28,
    marginRight: 10
    // border: `2px solid ${theme.palette.background.paper}`,
}));

export default function UserAvatar({ user }) {
    return (
        <Box display="flex" alignItems="center" sx={{
            borderRadius: theme.spacing(1),
            border: `1px solid ${lighten(theme.palette.background.paper, 0.15)}`,
            padding: theme.spacing(0.55),
            "&:hover": {
                bgcolor: `${lighten(theme.palette.background.paper, 0.1)}`
            }
        }}>
            <SmallAvatar sx={{ bgcolor: getColor(user.name) }}>
                <span style={{ textDecoration: "none" }}>
                    {user.name[0].toUpperCase()}
                </span>
            </SmallAvatar>
            <Link to="/user/1">
                {user.name}
            </Link>
        </Box>
    )
}