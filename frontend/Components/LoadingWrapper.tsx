import { Box } from "@mui/material";
import { PropsWithChildren, ReactNode } from "react";
import FullwidthCircular from "./FullwidthCircular";
import ErrorMessage from "./ErrorMessage";

export default function LoadingWrapper({ isLoading, children, error, fakeComponent}: { isLoading: boolean, error: Error, fakeComponent?: ReactNode } & PropsWithChildren) {
    const displayComponent = fakeComponent ?? <FullwidthCircular sx={{ height: "50vh" }} />;

    if (error) return (
        <ErrorMessage exception={error} />
    );

    if (isLoading) return (
        <Box>
            {displayComponent}
        </Box>
    );

    return children;
}