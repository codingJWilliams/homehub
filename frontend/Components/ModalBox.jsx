import { Box, Modal, lighten } from "@mui/material";
import { theme } from "../Common/theme";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    bgcolor: 'background.paper',
    border: `1px solid ${lighten(theme.palette.background.paper, 0.15)}`,
    borderRadius: theme.spacing(1),
    boxShadow: 24,
    minWidth: '70%',
    maxHeight: "95vh",
    overflowY: "auto",
    p: 2,
    [theme.breakpoints.down('sm')]: {
        minWidth: '95%'
    }
};

export default function ModalBox({ isOpen, onClose, children }) {
    return (
        <Modal
            open={isOpen}
            onClose={onClose}
        >
            <Box sx={style}>
                {children}
            </Box>
        </Modal>
    )
}