import * as React from 'react';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ClickAwayListener from '@mui/material/ClickAwayListener';
import Grow from '@mui/material/Grow';
import Paper from '@mui/material/Paper';
import Popper from '@mui/material/Popper';
import MenuItem from '@mui/material/MenuItem';
import MenuList from '@mui/material/MenuList';
import ButtonFile from './ButtonFile';

export default function ButtonDropdown({ color, labelText, onClick, options, sx, onFileSelect }) {
    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef(null);

    const handleMenuItemClick = (event, index) => {
        setOpen(false);
        onClick(index);
    };

    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event) => {
        if (
            anchorRef.current &&
            anchorRef.current.contains(event.target)
        ) {
            return;
        }

        setOpen(false);
    };

    return (
        <React.Fragment>
            <ButtonGroup sx={sx} variant="contained" ref={anchorRef} color={color}>
                <Button
                    onClick={handleToggle}
                    size="small"
                >{labelText}</Button>
                <Button
                    size="small"
                    onClick={handleToggle}
                    sx={{ p: 0 }}
                >
                    <ArrowDropDownIcon fontSize="small" />
                </Button>
            </ButtonGroup>
            <Popper
                sx={{
                    zIndex: 1,
                }}
                open={open}
                anchorEl={anchorRef.current}
                role={undefined}
                transition
                keepMounted
                disablePortal
            >
                {({ TransitionProps, placement }) => (
                    <Grow
                        {...TransitionProps}
                        style={{
                            transformOrigin:
                                placement === 'bottom' ? 'center top' : 'center bottom',
                        }}
                    >
                        <Paper elevation={4}>
                            <ClickAwayListener onClickAway={handleClose}>
                                <MenuList id="split-button-menu" autoFocusItem>
                                    {options.map((option, index) => (
                                        option.type === "button" ?
                                            <MenuItem
                                                key={option.name}
                                                onClick={(event) => handleMenuItemClick(event, index)}
                                            >
                                                {option.name}
                                            </MenuItem> :
                                            <ButtonFile
                                                key={option.name}
                                                onClick={(event) => handleMenuItemClick(event, index)}
                                                onFileSelect={onFileSelect}
                                            >
                                                {option.name}
                                            </ButtonFile>
                                    ))}
                                </MenuList>
                            </ClickAwayListener>
                        </Paper>
                    </Grow>
                )}
            </Popper>
        </React.Fragment>
    );
}