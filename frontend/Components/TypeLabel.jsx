import AcUnitIcon from "@mui/icons-material/AcUnit";
import WarehouseIcon from "@mui/icons-material/Warehouse";
import KitchenIcon from "@mui/icons-material/Kitchen";
import InventoryIcon from "@mui/icons-material/Inventory";
import { Chip } from "@mui/material";


export default function TypeLabel({ type, sx }) {
    const Component = {
        freezer: AcUnitIcon,
        fridge: KitchenIcon,
        cupboard: WarehouseIcon,
        storage: WarehouseIcon,
        box: InventoryIcon
    }[type];

    if (!Component) return null;

    return (
        <Chip variant="outlined" icon={<Component sx={sx} />} label={type[0].toUpperCase() + type.substr(1).toLowerCase()} />
    );
}